# About

Hyrax Middleware: a middleware for mobile edge clouds.

Copyright (C) 2019 INESC TEC. 

This software is authored by João Filipe Rodrigues. Its development was financially supported by HYRAX project (Hyrax: Crowd-Sourcing Mobile Devices to Develop Edge Clouds - Ref: CMUP-ERI/FIA/0048/2013) and was part of João's PhD work at the Computer Science Department, Faculty of Sciences, University of Porto,  supervised by Luís Lopes and Fernando Silva, and with contributions by Eduardo Marques, Rolando Martins, and Joaquim Silva. 

If you use the Hyrax middleware in a work that leads to a scientific publication, we would appreciate if you would kindly cite the following document:

João Rodrigues, A Middleware for Mobile Edge-Cloud Applications, PhD Thesis, Computer Science Department, Faculty of Sciences, University of Porto, 2019

The document can be found at https://hdl.handle.net/10216/118307

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

This work makes use and is distributed with the following libraries:
- Jdeferred (http://jdeferred.org)
- Little Proxy (https://github.com/adamfisk/LittleProxy)
- Google Protocol Buffers (https://developers.google.com/protocol-buffers/)
- Google Protocol Buffers - Java Format (https://github.com/bivas/protobuf-java-format)

You can reach INESC TEC at info@inesctec.pt, or

Campus da Faculdade de Engenharia da Universidade do Porto
Rua Dr. Roberto Frias
4200-465 Porto
Portugal

A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.


# Hyrax Link Layer
The Hyrax link layer is an Android API that allows to control distinct wireless
technologies in a normalized and robust way. Android devices have different
wireless technologies available, e.g. Bluetooth, Bluetooth LE, Wifi, Wifi Direct
and 3G/4G and each of them has its own library. With these different technologies
the Hyrax link layer provides just one interface to control all of them.

The project [HyraxLinkLayerApp](https://bitbucket.org/hyrax_dcc/hyrax-link-layer-app),
hosts an Android App used to test the link layer and also may serve as a developer
guide because it contains a few examples of use.

## How to install
The code in this project is an Android studio library, thus we assume that the
reader knows what it is. If not check [Android Library](https://developer.android.com/studio/projects/android-library.html).
As this project is a library it cannot be installed direct on the Android devices, thus
it must be first added to an Android project.

### Add as git submodule
The advantages of submodules is essentially the ability to add other projects
to your own project. These submodules projects are independent which means that
can be have it's own commits/updates. See [Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
for more information. Assuming that developer has already a git project, then inside the project
folder (root folder) do:
```git
git submodule add git@bitbucket.org:hyrax_dcc/android-link-layer-library.git
git status
cat .gitmodules
```
* The command on the first line means to clone a git project and adding it as
a current project submodule.
* In the second line the developer must see at least two changes on the project
**.gitmodules** and **android-link-layer-library**. Means that a submodule was
successfully added.
* The last command the developer can see the references to the submodule added.

*These commands are just needed for git older versions*. For newer versions the
above commands are enough.
```git
git submodule init
git submodule update
```

In case the developer does not have the current project under git, then clone
is just enough.
```git
git clone git@bitbucket.org:hyrax_dcc/android-link-layer-library.git
```

From now on the developer should have the Android library added to the project. To
finalize the process two more things must be done, the library compile instruction
and add the library to gradle settings. Regarding the first process, on app folder gradle
(*build.gradle*) add in the dependencies section the compile instruction
*compile project(':android-link-layer-library')*. It should look like this:
```
dependencies {
    ...
    compile project(':android-link-layer-library')
}
```
Now the developer must open the *settings.gladle* and add ':android-link-layer-library'.
The file must look like:
```
include ':app', ':android-link-layer-library'
```
If the line is not added the project won't know that the library exists. From now on
the link layer tools are at developers disposal. **So Have Fun**

## Overview
The link layer has abstract objects that are common in all technologies, which
represents the same. Next the most important objects.

* **Link**: The interface *Link* allows to manage all the technologies
  in a normalized way.
* **LinkService**: The *LinkService* is a factory that constructs and access the technology
  objects and also enables and disables the whole link layer system.
* **Listeners**: There are just three listeners types (*ActionListener*, *ActionProgressListener*, *ActionEventListener*).
  Those listeners the allows to hear action events and also to keep track of
  current system state.
* **Events**: We managed to normalize the events, thus those means exactly the same
  thing in every technology.
* **Properties**: Every action category has its own properties, e.g. Discovery, Visibility,
  Connection. These properties permits us to tune and specialize the behavior of the methods
  for a more advanced control.
* **Outcome**: The *Outcome* object allows to have a more trustable flow control
  of the program. The whole objects are non-null and have always the information
  about the success, the result of an action and, if the case, a categorized
  the exception error object.

In this section will be depicted a few examples of use code in order to have
a more comprehensive view about the link layer. To use the link layer, it must be
initiated first. The following snippet shows how to initialize the link layer
and how they create/destroy links to control a specific technology.
```java
//Boots the link service with default properties
LinkService.boot(new ServiceProperties.Builder().build());

//From here the developer may instantiate all the link that desires
//Bluetooth example
Link bluetooth = LinkService.create(Technology.BLUETOOTH, context);
//Wifi example
Link wifi = LinkService.create(Technology.WIFI, context);
//Wifi P2p example
Link wifip2p = LinkService.create(Technology.WIFI_P2P, context);
//All the remaining technologies are initiated exactly in the same way


<code_here>

//The developer may disable each link individually
bluetooth.detach();//Disables Bluetooth link
wifi.detach(); //Disables Wifi link
wifip2p.detach();//Disables Wifi P2p link

//Or the developer may decide just to disable all the active links
LinkService.shutdown();
```
### Using asynchronous calls
The following snippet shows how to start an asynchronous server where's a device
is able to receive connections from peers clients. The technology used is Bluetooth LE.
```java
//initializes the link layer
LinkService.boot(new ServiceProperties.Builder().build());

new ExampleBthLeServer(getContext());
...
/**
  Asynchronous Example
  There are dozens of ways to represents this code, we choose like this because
  it's easier to read and to understand the flow, instead of having callbacks
  inside callbacks (The asynchronous code gets messy very easy).
  All the listeners inside methods are just called once (garantied by link layer).
  This allow us to have a certain level of trust in or program flow.
**/
class ExampleBthLeServer {
  private Link bth_le;
  public ExampleBthLeServer(Context context) {
    //Creates a Bluetooth le link
    bth_le = LinkService.create(Technology.BLUETOOTH_LE, context);
    enable();
  }

  public void enable() {
    //checks if hardware is enabled
    if (!bth_le.isEnabled()) { //if not
      //enables the hardware
      bth_le.enable(new ActionListener<Boolean>() {
        @Override
        public void onActionResult(@NotNull Outcome<Boolean> outcome) {
          if (outcome.isSuccessful())
            accept();
        }
      });
    }
    accept();
  }

  public void accept() {
    //accept connection from peers clients
    bth_le.acceptConnections(new ServerProperties.Builder().build(), new ActionListener<String>() {
      @Override
      public void onActionResult(@NotNull Outcome<String> outcome) {
        if (outcome.isSuccessful())
          visible();
      }
    });
  }

  public void visible() {
    //becomes visible in order to other devices seeing it while discovering
    bth_le.visible(new ActionListener<String>() {
      @Override
      public void onActionResult(@NotNull Outcome<String> outcome) {
        if (outcome.isSuccessful()) {
          //Device is visible to others
          String id = outcome.getOutcome(); //visibility id
        } else {//if the outcome is not success
          //the developer may get the error cause
          outcome.getError();
        }
      }
    });
  }
}
...
LinkService.shutdown();
```
The following snippet shows how to start an asynchronous client where's a device
is able to discover devices in the neighborhood and connected to them.
The technology used is Bluetooth LE.
```java
LinkService.boot(new ServiceProperties.Builder().build());
new ExampleBthLeClient(getContext());
...    
/**
  Asynchronous Example
  There are dozens of ways to represents this code, we choose like this because
  it's easier to read and to understand the flow, instead of having callbacks
  inside callbacks (The asynchronous code gets messy very easy).
  All the listeners inside methods are just called once (garantied by link layer),
  excepting in the discovery that in be called several times until a inProgress
  is done (after this the listener will not be called anymore).
  This allow us to have a certain level of trust in or program flow.
**/
class ExampleBthLeClient {
  private Link bth_le;
  public ExampleBthLeClient(Context context) {
    //Creates a Bluetooth le link
    bth_le = LinkService.create(Technology.BLUETOOTH_LE, context);
    enable();
  }


  public void enable() {
    //checks if hardware is enabled
    if (!bth_le.isEnabled()) { //if not
      //enables the hardware
      bth_le.enable(new ActionListener<Boolean>() {
        @Override
        public void onActionResult(@NotNull Outcome<Boolean> outcome) {
          if (outcome.isSuccessful())
          discover();
        }
      });
    }
    discover();
  }

  public void discover() {
    //starts the discovery process (discover nearby devices that are visible)
    bth_le.discover(new ActionProgressListener<Collection<Device>>() {
      @Override
      public void onEventDone(@NotNull Outcome<Collection<Device>> outcome) {
        //Receives the result of discovery (all devices found).
        Collection<Device> devices = outcome.getOutcome();
        Device[] dev = devices.toArray(new Device[devices.size()]);

        connect(dev[0]);
      }

      @Override
      public void onActionResult(@NotNull Outcome<Collection<Device>> outcome) {
        /**
        * It's receives the discovery inProgress and it's called several times.
        * e.g. collection of devices that are being found
        */
      }
    });
  }

  public void connect(Device device) {
    //connects to a remote device
    bth_le.connect(device, new ActionListener<Device>() {
      @Override
      public void onActionResult(@NotNull Outcome<Device> outcome) {
        //Receives the connection event
        if (outcome.isSuccessful()) {
          //Device is connected
          Device device = outcome.getOutcome(); //connected device
        } else {
          //Connection error
        }
      }
    });
  }
}
...
LinkService.shutdown();
```
### Using synchronous calls
The following snippet shows how to start an synchronous server where’s a device is
able to receive connections from peers clients. The technology used is Bluetooth LE.
This example do exactly the same thing as the previous server.
```java
//initializes the link layer
LinkService.boot(new ServiceProperties.Builder().build());
...
//Creates a Bluetooth le link
Link bth_le = LinkService.create(Technology.BLUETOOTH_LE, context);

//Checks is the hardware is enabled or not
if (!bth_le.isEnabled()) { // if not
  //Starts the hardware
  Outcome<Boolean> enabled = bth_le.enable();
  if (enabled.isSuccessful()) {

    //accept connection from peers clients
    Outcome<String> accept = bth_le.acceptConnections(new ServerProperties.Builder().build());
    if (accept.isSuccessful()) {

      ////becomes visible in order to other devices seeing it while discovering
      Outcome<String> visible = bth_le.visible();
      if (visible.isSuccessful()) {
        //Device is visible to others
        String id = visible.getOutcome();//visibility id
      } else {
        //the developer may get the error cause
        outcome.getError();
      }
    }
  }
}
...
LinkService.shutdown();
```
The following snippet shows how to start an synchronous client where’s a device is
able to discover devices in the neighborhood and connected to them.
The technology used is Bluetooth LE legal.
```java
LinkService.boot(new ServiceProperties.Builder().build());
...
Link bth_le = LinkService.create(Technology.BLUETOOTH_LE, context);
//Checks is the hardware is enabled or not
if (!bth_le.isEnabled()) { //if not
  //Creates a Bluetooth le link
  Outcome<Boolean> enabled = bth_le.enable();
  if (enabled.isSuccessful()) {
    /**
      Starts the discovery process (discover nearby devices that are visible).
      This action blocks the current thread during a default discovery time,
      that can be changed if the developer created the DiscoveryProperties object.
    **/
    Outcome<Collection<Device>> discover = bth_le.discover();
    if (discover.isSuccessful()) {
      Collection<Device> devices = discover.getOutcome();
      Device[] dev = devices.toArray(new Device[devices.size()]);

      //Connects to the first device in the list
      Outcome<Device> connect = bth_le.connect(dev[0]);
      if (connect.isSuccessful()) {
        //Device is connected
        Device device = outcome.getOutcome(); //connected device
      } else {
        //Connection error
      }
    }
  }
}
...
LinkService.shutdown();
```
### Other Features
Either on the asynchronous or synchronous version the action result are available
when it is called. However there are other means to catch those events results. Next
in the following code snippet we will show how to catch those events passively.
```java
Link bth_le = LinkService.create(Technology.BLUETOOTH_LE, context);

<some_code>

/**
This method allows to catch literally all the events produced by the link layer.
**/
bth_le.listenAll(new ActionEventListener() {
  @Override
  public void onEvent(@NotNull LinkEvent eventType, @NotNull Outcome outcome) {
    switch (eventType) {
      case LINK_HARDWARE_ON:
      break;
      case LINK_HARDWARE_OFF:
      break;
      ...
    }
  }
});

/**
The link layer allows to listen events by category which helps the developer
to filter the group of events needed.
**/
bth_le.listenByCategory(EventCategory.LINK_DISCOVERY, new ActionEventListener() {
  @Override
  public void onEvent(@NotNull LinkEvent eventType, @NotNull Outcome outcome) {
    switch (eventType) {
      case LINK_DISCOVERY_ON:
      break;
      case LINK_DISCOVERY_OFF:
      break;
      case LINK_DISCOVERY_FOUND:
      break;
    }
  }
});

/**
The link layer permits another refinement level which is filter the events by type of
event. For instance listen all the new connections that the device is establishing
or receiving.
**/
bth_le.listenByEventType(LinkEvent.LINK_CONNECTION_NEW, new ActionEventListener<Device>() {
  @Override
  public void onEvent(@NotNull LinkEvent eventType, @NotNull Outcome<Device> outcome) {
    Device device = outcome.getOutcome();
  }
});

/**
Although we can go further which is listen events of a certain type filtered by
an object. For instance listen client connections for a specific server. Note that
some technologies allows to start several servers to receive connections.
**/
//Accept connections
Outcome<String> server = bth_le.acceptConnections(new ServerProperties.Builder().build());
//Get server identifier
String serverId = server.getOutcome();
//Listen all new clients events filtered by a specific server identifier.
bth_le.listenByEventType(LinkEvent.LINK_CONNECTION_SERVER_NEW_CLIENT, serverId,
  new ActionEventListener<Device>() {
    @Override
    public void onEvent(@NotNull LinkEvent eventType, @NotNull Outcome<Device> outcome) {
      Device clientDevice = outcome.getOutcome();
    }
});
```

## Known issues
Some of the known issues are related with bugs on Android Api and others
are related with the link layer (1st release).

- Wifi
  - NsdManager: Does not allow to send/receive service attributes. It returns
  always an empty list.
  - The WifiScanFilter just allows to filter the advertiser by
  service type(Bonjour protocol). The remaining options do not work yet.
  The filter must be done at higher level domain.

- Wifi Direct
  - The WifiScanFilter simply does not work. We did not try much, but most probably
  the filter must be done at higher level domain.
  - The discovery and visibility processes are exactly the same and cannot be
  done separately. Or the Wifi Direct evolves or to achieve this we must to have
  control at a logic level that gives a illusion of separated processes.

- Bluetooth Low Energy
  * The ScanFilter does not work. The Google's api provides this object but we
  weren't able to put this to work.
