package org.hyrax.link.misc;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class EventObservableTest {
    private EventObservable<Observer<String>> observable;

    @Before
    public void setUp() {
        observable = new EventObservable<>();
        System.out.println("SetUp...");
    }

    @After
    public void tearDown() {
        observable.removeAll();
        System.out.println("TearDown...");
        assertEquals(0, observable.length());
    }

    @Test
    public void addAndNotify() {
        observable.addListener(object -> System.out.println("Notified Append " + object));
        assertEquals(1, observable.length());
        observable.addListenerOnTop(object -> System.out.println("Notified Top " + object));
        assertEquals(2, observable.length());

        observable.notifyListeners(listener -> {
            listener.onEvent("Hello ...");
            return false;
        });
    }

    @Test
    public void addAndNotifyOnce() {
        observable.addListener(object -> System.out.println("Notified Append " + object));
        assertEquals(1, observable.length());
        observable.addListenerOnTop(object -> System.out.println("Notified Top " + object));
        assertEquals(2, observable.length());

        observable.notifyListeners(listener -> {
            listener.onEvent("Hello ...");
            observable.addListener((object -> System.out.println("Notified Inside " + object)));
            return false;
        });
        assertEquals(4, observable.length());
        observable.notifyListeners(listener -> {
            listener.onEvent("Hi ...");
            return true;
        });
        assertEquals(0, observable.length());
    }

    @Test
    public void addAndNotifyRemove() {
        observable.addListener(object -> System.out.println("Notified Append " + object));
        observable.addListenerOnTop(object -> System.out.println("Notified Top " + object));
        Observer<String> l = (object -> System.out.println("Yet Another One " + object));
        observable.addListener(l);
        observable.notifyListeners(listener -> {
            listener.onEvent("Another ...");
            return false;
        });
        observable.removeListener(l);
        observable.notifyListeners(listener -> {
            listener.onEvent("Another ...");
            return false;
        });
        observable.removeAll();
        assertEquals(0, observable.length());
        observable.notifyListeners(listener -> {
            listener.onEvent("Another ...");
            return true;
        });
    }
}