package org.hyrax.link.misc;

import org.junit.Test;
import org.mockito.ArgumentMatcher;

import java.util.LinkedList;

import static org.mockito.Mockito.*;
/**
 * Created on 10/19/18.
 */
public class MockTest {

    @Test
    public void mockTest() {
        //You can mock concrete classes, not just interfaces
        LinkedList mockedList = mock(LinkedList.class);

        //stubbing
        when(mockedList.get(0)).thenReturn("first");
        when(mockedList.get(1)).thenThrow(new RuntimeException());

        //following prints "first"
        System.out.println(mockedList.get(0));

        //following throws runtime exception
        //System.out.println(mockedList.get(1));

        //following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999));

        //Although it is possible to verify a stubbed invocation, usually it's just redundant
        //If your code cares what get(0) returns, then something else breaks (often even before verify() gets executed).
        //If your code doesn't care what get(0) returns, then it should not be stubbed. Not convinced? See here.
        verify(mockedList).get(0);

        //stubbing using built-in anyInt() argument matcher
        when(mockedList.get(anyInt())).thenReturn("element");

        //stubbing using custom matcher (let's say isValid() returns your own matcher implementation):
        when(mockedList.contains(argThat((ArgumentMatcher<String>) argument -> true))).thenReturn(true);

        //following prints "element"
        System.out.println(mockedList.get(999));

        //you can also verify using an argument matcher
        verify(mockedList, times(3)).get(anyInt());

        doThrow(new RuntimeException("Blá")).when(mockedList).clear();
        //following throws RuntimeException:
        mockedList.clear();

        //argument matchers can also be written as Java 8 Lambdas
        //verify(mockedList).add(argThat((ArgumentMatcher<String>) someString -> someString.length() > 5));
    }
}
