package org.hyrax.link.misc;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * Created on 10/18/18.
 */
public class EventObservableThreadTest {
    private EventObservable<Observer<String>> observable;

    @Before
    public void setUp() {
        observable = new EventObservable<>();
        System.out.println("SetUp...");
    }

    @After
    public void tearDown() {
        observable.removeAll();
        System.out.println("TearDown...");
    }

    @Test
    public void parallel() {
        ExecutorService service = Executors.newFixedThreadPool(10);
        final CountDownLatch latch = new CountDownLatch(30);
        final Random rand = new Random();
        service.submit(() -> {
            try {
                int k = 30, c = 0;
                while (k > 0) {
                    if (k % 5 == 0) {
                        observable.notifyListeners(listener -> {
                            listener.onEvent("Hi");
                            return false;
                        });
                    } else {
                        c++;
                        service.submit(() -> {
                            System.out.println("Registered " + Thread.currentThread().getName());
                            observable.addListener((object -> System.out.println("Listen " + object)));
                        });

                    }
                    Thread.sleep(rand.nextInt(50));
                    latch.countDown();
                    k--;
                }
            } catch (InterruptedException | AssertionError e) {
                e.printStackTrace();
                System.err.println(e.getMessage());
            }
        });

        try {
            latch.await(15, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
