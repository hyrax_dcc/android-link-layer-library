/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.Aggregator;

/**
 * Enumeration of event types, triggered by the link layer.
 * <br/>
 * These events are the normalization to the android libraries actions.
 */
public enum LinkEvent {
    LINK_DETACH(EventCategory.LINK_DETACH, true),

    LINK_HARDWARE_ON(EventCategory.LINK_HARDWARE, true),
    LINK_HARDWARE_OFF(EventCategory.LINK_HARDWARE, true),

    LINK_DISCOVERY_ON(EventCategory.LINK_DISCOVERY, true),
    LINK_DISCOVERY_FOUND(EventCategory.LINK_DISCOVERY, false),
    LINK_DISCOVERY_DONE(EventCategory.LINK_DISCOVERY, true),
    LINK_DISCOVERY_OFF(EventCategory.LINK_DISCOVERY, true),

    LINK_VISIBILITY_ON(EventCategory.LINK_VISIBILITY, true),
    LINK_VISIBILITY_DONE(EventCategory.LINK_VISIBILITY, true),
    LINK_VISIBILITY_OFF(EventCategory.LINK_VISIBILITY, true),

    LINK_CONNECTION_NEW(EventCategory.LINK_CONNECTION, true),
    LINK_CONNECTION_LOST(EventCategory.LINK_CONNECTION, true),

    LINK_CONNECTION_SERVER_ON(EventCategory.LINK_CONNECTION_SERVER, true),
    LINK_CONNECTION_SERVER_INFO(EventCategory.LINK_CONNECTION_SERVER, true),
    LINK_CONNECTION_SERVER_NEW_CLIENT(EventCategory.LINK_CONNECTION_SERVER, true),
    LINK_CONNECTION_SERVER_OFF(EventCategory.LINK_CONNECTION_SERVER, true);

    @NonNull
    private final EventCategory category;
    private final boolean toRemove;

    /**
     * Event constructor.
     *
     * @param category event category
     * @param toRemove <tt>true</tt> to remove after triggering (temporary event),
     *                 <tt>false</tt> otherwise
     */
    LinkEvent(@NonNull EventCategory category, boolean toRemove) {
        this.category = category;
        this.toRemove = toRemove;
    }

    /**
     * Returns the {@link EventCategory} of event
     *
     * @return category of event
     */
    @NonNull
    public EventCategory getCategory() {
        return category;
    }

    /**
     * Check if the action is done and is to be removed by the
     * {@link Aggregator}
     *
     * @return <tt>true</tt> if done, <tt>false</tt> otherwise
     */
    public boolean isActionDone() {
        return toRemove;
    }
}
