/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.Executable;
import org.hyrax.link.misc.exception.LinkException;

/**
 * The interface that returns the result of a specific action in the link layer,
 * aiming to ensure that a result is not a NULL.
 * <br/>
 * The NULL cause some troubles to the developers then this interface ensures that when a result
 * is available it has always a non-null object.
 */
public interface Outcome<D> {
    /**
     * Checks the success of the result.
     *
     * @return <tt>true</tt> if success, <tt>false</tt> otherwise
     */
    boolean isSuccessful();

    /**
     * Returns the result of an action.
     *
     * @return the action result
     */
    @NonNull
    D getOutcome();

    /**
     * Returns the result error of an action.
     * <br/>
     * Even in case of action success this method does not return a null, but an
     * {@link LinkException} with an empty reason.
     *
     * @return the action error
     */
    @NonNull
    LinkException getError();

    /**
     * Executes the executable interface in case the outcome is success.
     *
     * @param executable object executor
     */
    default void ifSuccess(@NonNull Executable<D> executable) {
        if (isSuccessful())
            executable.execute(getOutcome());
    }

    /**
     * Executes the executable interface in case the outcome is success.
     *
     * @param executable object executor
     */
    default void ifError(@NonNull Executable<D> executable) {
        if (!isSuccessful())
            executable.execute(getOutcome());
    }
}
