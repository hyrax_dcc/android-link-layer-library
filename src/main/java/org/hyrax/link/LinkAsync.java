/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.util.Collection;

/**
 * This interface is responsible for controlling the link layer operations, which are
 * the following:
 * <br/>
 * <ul>
 * <li>Enable/Disable Hardware</li>
 * <li>Devices Discovery</li>
 * <li>Devices Visibility</li>
 * <li>Devices Connection</li>
 * <li>Device Connection Acceptance</li>
 * <li>Services publication</li>
 * </ul>
 * <br/>
 * This actions are executed on asynchronous manner, meaning that the developer must pass an
 * {@link LinkListener} as argument in order to receiver the result later.
 * <br/>
 * The listener callback returns an {@link Outcome} which allows to check if the action was
 * performed with success or not. The developer <b>must</b> check always the validity of result.
 * Furthermore with an {@link Outcome} comes the result of the operations as well a
 * {@link Throwable} object indicating the action error.
 *
 * //@param <F> type of link feature
 */
public interface LinkAsync<F extends LinkFeatures> extends Link<F> {

    /**
     * Enables the hardware (Async).
     * <br/>
     * The result is received through a listener, which will be removed once triggered.
     *
     * @param listener listen for hardware enable event
     */
    void enable(@NonNull LinkListener<Boolean> listener);

    /**
     * Disables the hardware (Async).
     * <br/>
     * The result is received through a listener, which will be removed once triggered.
     *
     * @param listener listen for hardware disable event
     */
    void disable(@NonNull LinkListener<Boolean> listener);

    /**
     * Starts by searching for reachable devices in the neighborhood by giving a specific
     * {@link DiscoveryProperties} (Async).
     * <br/>
     * The {@link DiscoveryProperties} are a set of properties that allows to tune the discovery
     * progress to the developer needs.
     * <br/>
     * The result is received through a listener, which will be removed once the progress
     * is done.
     *
     * @param properties   discovery properties
     * @param doneListener listener that receives a notification when the discovery is done
     */

    void discover(@NonNull DiscoveryProperties properties,
                  @NonNull LinkListener<Collection<Device>> doneListener);

    /**
     * Starts by searching for reachable devices in the neighborhood by giving a specific
     * {@link DiscoveryProperties} (Async).
     * <br/>
     * The {@link DiscoveryProperties} are a set of properties that allows to tune the discovery
     * progress to the developer needs.
     * <br/>
     * The result is received through a listener, which will be removed once the progress
     * is done.
     *
     * @param properties   discovery properties
     * @param doneListener listener that receives a notification when the discovery is done
     */

    void discover(@NonNull DiscoveryProperties properties,
                  @NonNull LinkListener<Collection<Device>> doneListener,
                  @NonNull LinkListener<Collection<Device>> progressListener);

    /**
     * Cancels the discovery process given an scanId (Async).
     * <br/>
     * The result is received through a listener, which will be removed once triggered.
     *
     * @param scannerId    scanner identifier
     * @param listenerStop listen for the discovery cancel event
     */
    void cancelDiscover(@NonNull String scannerId, @NonNull LinkListener<String> listenerStop);

    /**
     * Turns the device visibility on with defined properties (ASYNC).
     * <br/>
     * Devices may support more than one visibility process, thus it generates an advertiseId
     * in order to identify the advertiser.
     *
     * @param properties    visibility properties
     * @param startListener listener that receives the visible on event
     */
    void setVisible(@NonNull VisibilityProperties properties,
                    @NonNull LinkListener<String> startListener);

    /**
     * Turns the device visibility on with defined properties (ASYNC).
     * <br/>
     * Devices may support more than one visibility process, thus it generates an advertiseId
     * in order to identify the advertiser.
     *
     * @param properties    visibility properties
     * @param startListener listener that receives the visible on event
     * @param doneListener  listener that receives the visible done event
     */
    void setVisible(@NonNull VisibilityProperties properties,
                    @NonNull LinkListener<String> startListener,
                    @NonNull LinkListener<String> doneListener);

    /**
     * Turns the device visibility off given a advertiseId (ASYNC).
     * <br/>
     * Devices may support more than one visibility process, thus if an advertiser is disabled does
     * not means that the device is completely invisible. Then to become completely invisible all
     * the advertises must be removed.
     *
     * @param advertiserId advertiser identifier
     * @param listener     listener that receives the visible event
     */
    void cancelVisible(@NonNull String advertiserId, @NonNull LinkListener<String> listener);

    /**
     * Connects to a remote device by specifying a timeout (Async).
     * <br/>
     * The result is received through a listener, which will be removed once triggered.
     *
     * @param device     device to connect
     * @param properties connection properties
     * @param listener   listener that receives the result of connection
     */
    void connect(@NonNull Device device, @NonNull ConnectionProperties properties,
                 @NonNull LinkListener<Device> listener);

    /**
     * Disconnects from a remote device (Async).
     * <br/>
     * The result is received through a listener, which will be removed once triggered.
     *
     * @param device   device to disconnect
     * @param listener listener that receives the result of disconnection
     */
    void disconnect(@NonNull Device device, @NonNull LinkListener<Device> listener);

    /**
     * Starts to accept connections (server/access point) from clients with a specific
     * identifier (Async).
     * <br/>
     * The result is received through a listener, which will be removed once triggered.
     *
     * @param properties server properties
     * @param listener   listener that receives server identifier
     */
    void accept(@NonNull ServerProperties properties, @NonNull LinkListener<String> listener);

    /**
     * Denies more connections from clients (Async).
     * <br/>
     * Depending of technology the clients can be kept or not, e.g Bluetooth will keep the
     * connections while the Wi-Fi P2p not.
     * <br/>
     * The result is received through a listener, which will be removed once triggered.
     *
     * @param serverId connection server object
     * @param listener listener that receives the result
     */
    void deny(@NonNull String serverId, @NonNull LinkListener<String> listener);
}
