/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link;


import android.support.annotation.NonNull;

import org.hyrax.link.misc.ScanData;

import java.util.Collection;

/**
 * The {@link Device} object represents a remote device object.
 * <br/>
 * Remote device object is any reachable device, in the neighbourhood.
 * This interface wraps the original object device into an abstraction representation of remove device.
 */
public interface Device {
    /**
     * Returns the name of the remote device.
     * During the execution process the name may very, thus do not stick on this name to identify
     * uniquely the remote device.
     *
     * @return the name of remote device
     */

    @NonNull
    String getName();

    /**
     * Returns an unique address identifier of the remote device.
     * Usually the unique device identifier comes in a form of mac address, however it may be not
     * the only form.
     *
     * @return the unique identifier of the device
     */
    @NonNull
    String getUniqueAddress();

    /**
     * Returns a RSSI value (signal strength) of remote device.
     *
     * @return rssi integer
     */
    int getRssi();

    /**
     * Returns the original technology object.
     *
     * @return original object
     */
    @NonNull
    Object getOriginalObject();

    /**
     * Returns a collection of scan data.
     * <br/>
     * If not data exists, return an empty collection.
     *
     * @return collection of scan data.
     */
    @NonNull
    Collection<ScanData> getScanData();

    /**
     * Merges scanData with current scanData.
     *
     * @param scanData data to be merged
     */
    void mergeScanData(Collection<ScanData> scanData);

    /**
     * Returns the device technology.
     *
     * @return device technology
     */
    @NonNull
    Technology getTechnology();
}
