/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.bluetooth.BluetoothLeLink;
import org.hyrax.link.bluetooth.BluetoothLink;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.link.AbstractLink;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.link.LinkContext;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.exception.LinkLayerRuntimeException;
import org.hyrax.link.mobile.MobileLink;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.WifiLink;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This class is a gateway between the developer and the communication technologies.
 */
public class LinkService {
    private static final AtomicBoolean enabled = new AtomicBoolean(false);
    @Nullable
    private static LinkService instance;
    @NonNull
    private final LinkContext linkContext;
    @NonNull
    private final Handler workerHandler;

    @NonNull
    private final ConcurrentHashMap<Technology, AbstractLink<?>> links;

    /**
     * Enumeration that represents the internet protocol version.
     */
    public enum ProtocolVersion {
        IPV4, // version 4
        IPV6  // version 6
    }

    /**
     * Constructor.
     *
     * @param context application context
     * @see <a href="https://developer.android.com/reference/android/content/Context.html" target="_blank">Context</a>
     */
    private LinkService(@NonNull Context context) {
        this.linkContext = new LinkContext(context);
        this.workerHandler = new Handler(
                new HandlerThread("LinkWorkerThread") {{
                    start();
                }}.getLooper());

        links = new ConcurrentHashMap<>();
    }

    /**
     * Boots the {@link LinkService}.
     * <br/>
     * This method will essentially initialize the handler and the thread pool. in order to be
     * ready for the links implementation.
     * <br/>
     * The {@link Handler} and the {@link HandlerThread} will be responsible for listen the
     * android communication events freeing the main thread of extra work.
     * <br/>
     * The executor service is where the logic actions will be performed, in the link layer, as
     * the synchronous methods need extras threads to be executed.
     *
     * @param context application context
     * @see <a href="https://developer.android.com/reference/android/os/Handler.html" target="_blank">Handler</a>
     * @see <a href="https://developer.android.com/reference/android/os/HandlerThread.html" target="_blank">HandlerThread</a>
     * @see <a href="https://developer.android.com/reference/java/util/concurrent/ExecutorService.html" target="_blank">ExecutorService</a>
     */
    public static void boot(@NonNull Context context) {
        if (enabled.compareAndSet(false, true)) {
            instance = new LinkService(context);
            return;
        }
        throw new LinkLayerRuntimeException("Link Service already booted");
    }

    /**
     * Returns the link context object.
     *
     * @return link context object
     */
    @NonNull
    public static LinkContext getLinkContext() {
        Objects.requireNonNull(instance);
        return instance.linkContext;
    }

    /**
     * Verifies if the {@link LinkService} is enabled.
     *
     * @return <tt>true</tt> if enabled, <tt>false</tt> otherwise
     */
    public static boolean isEnabled() {
        return enabled.get();
    }

    /**
     * Shutdowns the {@link LinkService} and cleans whatever needs to be cleansed
     */
    public static void shutdown() {
        if (enabled.compareAndSet(true, false) && instance != null) {
            for (Technology tech : instance.links.keySet()) {
                AbstractLink<?> link = instance.getLink(tech);
                if (link.isAttached())
                    link.detach();
            }

            instance.links.clear();
            instance.workerHandler.getLooper().quitSafely();
            instance = null;
        }
    }

    /**
     * Shutdowns a specific link technology.
     *
     * @param technology link technology to be disabled/detached
     */
    public static void shutdownLink(@NonNull Technology technology) {
        if(instance != null) {
            AbstractLink<?> link = instance.links.remove(technology);
            if (link != null && link.isAttached())
                link.detach();
        }
    }

    /**
     * Returns the link controller synchronous api.
     *
     * @param technology link technology
     * @param <T>        type of link features
     * @return a link sync api instance
     */
    public static <T extends LinkFeatures> LinkSync<T> getLinkSync(@NonNull Technology technology) {
        Objects.requireNonNull(instance);
        return instance.<T>getLink(technology).getSyncApi();
    }

    /**
     * Returns the link controller asynchronous api.
     *
     * @param technology link technology
     * @param <T>        type of link features
     * @return a link async api instance
     */
    public static <T extends LinkFeatures> LinkAsync<T> getLinkAsync(@NonNull Technology technology) {
        Objects.requireNonNull(instance);
        return instance.<T>getLink(technology).getAsyncApi();
    }

    /**
     * Returns the link controller asynchronous promise api.
     *
     * @param technology link technology
     * @param <T>        type of link features
     * @return a link async promise api instance
     */
    public static <T extends LinkFeatures> LinkPromise<T> getLinkPromise(@NonNull Technology technology) {
        Objects.requireNonNull(instance);
        return instance.<T>getLink(technology).getPromiseApi();
    }

    /**
     * Verifies if a link with specific {@link Technology} exists.
     *
     * @param technology technology to check
     * @return <tt>true</tt> if exists, <tt>false</tt> otherwise
     */
    public static boolean hasLink(@NonNull final Technology technology) {
        Objects.requireNonNull(instance);
        return instance.links.containsKey(technology);
    }

    /**
     * Returns a new link given a technology as argument.
     * <br/>
     * In case the link already exists it returns a previously  instantiated link object,
     * otherwise it will instantiate a new link object
     *
     * @param technology link technology
     * @return a link object
     */
    @NonNull
    private synchronized <T extends LinkFeatures> AbstractLink<T> getLink(@NonNull final Technology technology) {
        AbstractLink<?> link = links.get(technology);

        if (link == null) {
            switch (technology) {
                case BLUETOOTH:
                    link = BluetoothLink.getInstance();
                    break;
                case BLUETOOTH_LE:
                    link = BluetoothLeLink.getInstance();
                    break;
                case WIFI:
                    link = WifiLink.getInstance();
                    break;
                case WIFI_DIRECT:
                    link = WifiDirectLink.getInstance();
                    break;
                case MOBILE:
                    link = MobileLink.getInstance();
                    break;

                default:
                    throw new LinkLayerRuntimeException("Undefined technology " + technology);
            }
            links.put(technology, link);

            link.setLinkContext(linkContext);
            link.setWorkerHandler(workerHandler);
            link.attach();
            link.getAsyncApi().listenOnce(
                    LinkEvent.LINK_DETACH,
                    Aggregator.NO_FILTER,
                    (LinkListener<Boolean>) (eventType, outcome) -> links.remove(technology));
        }

        return (AbstractLink<T>) link;
    }


    /**
     * Returns the Wifi ip address for a specific interface. Either legacy or p2p.
     *
     * @param interfacePrefix interface prefix. e.g. p2p-wlan
     * @param pVersion        internet protocol version. e.g. ipv4
     * @return returns a string format ip address or {@value Constants#UNDEFINED}
     */
    public static String getWifiIpAddress(@NonNull String interfacePrefix,
                                          @NonNull ProtocolVersion pVersion) {
        try {
            Enumeration<NetworkInterface> netIfs = NetworkInterface.getNetworkInterfaces();
            Class<? extends InetAddress> classType;
            switch (pVersion) {
                case IPV4:
                    classType = Inet4Address.class;
                    break;
                case IPV6:
                    classType = Inet6Address.class;
                    break;
                default:
                    throw new LinkLayerRuntimeException("Undefined protocol version " + pVersion);
            }

            while (netIfs.hasMoreElements()) {
                NetworkInterface netIf = netIfs.nextElement();
                if (netIf.getName().startsWith(interfacePrefix)) {
                    Enumeration<InetAddress> addresses = netIf.getInetAddresses();
                    while (addresses.hasMoreElements()) {
                        InetAddress address = addresses.nextElement();
                        if (classType.isAssignableFrom(address.getClass())) {
                            return address.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return Constants.UNDEFINED;
    }
}
