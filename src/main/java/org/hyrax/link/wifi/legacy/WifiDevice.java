/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.legacy;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.wifi.types.WifiScanRecord;
import org.hyrax.link.wifi.direct.WifiDirectNode;

import java.util.Objects;

/**
 * Wifi device representation given distinct objects sources.
 */
public class WifiDevice {
    private final String name;
    private final String address;
    private final int rssi;

    @NonNull
    private final Object originalObject;
    @NonNull
    private final WifiSource source;
    /* Depending of the constructor source the device hash may differ.
     * Sources from wifi scan, wifi connect or from WifiDirect will by hashed by ssid.
     * The remaining sources i.e. wifi services discovery will by hashed by address.
     */
    private final String stringToHash;

    /**
     * Constructs the {@link WifiDevice} class given a {@link ScanResult} object.
     * <br/>
     * Constructor used by the wifi scan
     *
     * @param scanResult wifi scan wifi result
     * @see <a href="https://developer.android.com/reference/android/net/wifi/ScanResult.html" target="_blank">ScanResult</a>
     */
    public WifiDevice(@NonNull ScanResult scanResult) {
        this.name = scanResult.SSID;
        this.address = scanResult.BSSID;
        this.rssi = scanResult.level;
        this.originalObject = scanResult;
        this.source = WifiSource.SCAN_RESULT;
        this.stringToHash = name;
    }

    /**
     * Constructs the {@link WifiDevice} class given a {@link WifiInfo} and bssid object.
     * <br/>
     * Constructor used by wifi connection
     *
     * @param wifiInfo wifi connection information
     * @param bssid    bssid of access point
     * @see <a href="https://developer.android.com/reference/android/net/wifi/WifiInfo.html" target="_blank">WifiInfo</a>
     */
    public WifiDevice(@NonNull WifiInfo wifiInfo, @NonNull String bssid) {
        this.name = wifiInfo.getSSID().replace("\"", "");
        this.address = bssid;
        this.rssi = wifiInfo.getRssi();
        this.originalObject = wifiInfo;
        this.source = WifiSource.WIFI_INFO;
        this.stringToHash = name;
    }

    /**
     * Constructs the {@link WifiDevice} class given a {@link WifiScanRecord}.
     * <br/>
     * Constructor used by wifi discovery services services
     *
     * @param scanRecord wifi service data
     */
    public WifiDevice(@NonNull WifiScanRecord scanRecord) {
        this.name = scanRecord.getServiceName();
        this.address = scanRecord.getHost();
        this.rssi = Constants.DEFAULT_RSSI;
        this.originalObject = scanRecord;
        this.source = WifiSource.WIFI_SCAN_RECORD;
        this.stringToHash = address;
    }

    /**
     * Constructs the {@link WifiDevice} class given a {@link WifiDirectNode} and the ssid.
     *
     * @param wifiDirectNode wifi p2p object
     * @param ssid           network ssid
     */
    public WifiDevice(@NonNull WifiDirectNode wifiDirectNode, @NonNull String ssid) {
        this.name = ssid;
        this.address = wifiDirectNode.getUniqueAddress();
        this.rssi = wifiDirectNode.getRssi();
        this.originalObject = wifiDirectNode;
        this.source = WifiSource.WIFI_P2P_SCAN_RECORD;
        this.stringToHash = name;
    }

    /**
     * Returns the device name.
     *
     * @return string name
     */
    @NonNull
    public String getName() {
        return name;
    }

    /**
     * Returns the device address.
     *
     * @return string address
     */
    @NonNull
    public String getAddress() {
        return address;
    }

    /**
     * Returns the device signal strength.
     *
     * @return signal strength
     */
    public int getRssi() {
        return rssi;
    }

    /**
     * Returns the source of this object.
     *
     * @return object source
     */
    @NonNull
    public WifiSource getSource() {
        return source;
    }

    /**
     * Returns the original source object.
     *
     * @return original source object
     */
    @NonNull
    public Object getOriginalObject() {
        return originalObject;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof WifiDevice) &&
                ((WifiDevice) o).stringToHash.equals(stringToHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stringToHash);
    }

    /**
     * Wifi scan source.
     */
    public enum WifiSource {
        SCAN_RESULT,
        WIFI_INFO,
        WIFI_SCAN_RECORD,
        WIFI_P2P_SCAN_RECORD,
    }
}


