/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.legacy.translator;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.event.VisibilityOffEvent;
import org.hyrax.link.misc.event.VisibilityOnEvent;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.link.LinkContext;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.VisibilityLogicTranslator;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiAdvertiseSettings;
import org.hyrax.link.wifi.WifiLink;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Wifi does not implement the visibility option.
 */
public class WifiVisibilityTranslator implements VisibilityLogicTranslator<WifiAdvertiseSettings, WifiAdvertiseData> {
    @NonNull
    private final NsdManager nsdManager;
    @NonNull
    private final Aggregator aggregator;
    @NonNull
    private final Handler workerHandler;

    @NonNull
    private final ConcurrentHashMap<String, NsdManager.RegistrationListener> advertisers;

    /**
     * Constructor of {@link WifiVisibilityTranslator} class.
     *
     * @param linkContext   linkContext application context
     * @param workerHandler worker handler thread
     * @param aggregator    listeners aggregator
     */
    public WifiVisibilityTranslator(@NonNull LinkContext linkContext, @NonNull Aggregator aggregator,
                                    @NonNull Handler workerHandler) {
        this.nsdManager = linkContext.getNsdManager();
        this.aggregator = aggregator;
        this.workerHandler = workerHandler;
        this.advertisers = new ConcurrentHashMap<>();
    }

    @Override
    public void turnVisibilityOn(@NonNull VisibilityProperties<WifiAdvertiseSettings, WifiAdvertiseData> visibilityProperties) {
        String advertiserId = visibilityProperties.getIdentifier();

        LinkException exception = null;

        if (Constants.DEFAULT_ADVERTISER_UUID.equals(advertiserId)) {
            exception = LinkException.build(Reason.UNSUPPORTED, "Wifi does not implement default visibility feature");

        } else if (visibilityProperties.getAdvertiserSettings() == null) {
            exception = LinkException.build(Reason.NULL_POINTER, "Empty advertiser settings please defined one");

        } else {
            workerHandler.post(new RegisterService(visibilityProperties));
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new VisibilityOnEvent(
                    visibilityProperties.getIdentifier(), exception));
    }

    @Override
    public void turnVisibilityOff(@NonNull String advertiseId) {
        if (Constants.DEFAULT_SCANNER_UUID.equals(advertiseId)) {
            Receiver.notifyEvent(aggregator, new VisibilityOffEvent(advertiseId,
                    LinkException.build(Reason.UNSUPPORTED, "Wifi does not implement default visibility feature")
            ));
        } else {
            NsdManager.RegistrationListener listener = advertisers.remove(advertiseId);
            if (null != listener) {
                nsdManager.unregisterService(listener);

            } else {
                Receiver.notifyEvent(aggregator, new VisibilityOffEvent(
                        advertiseId, LinkException.build(Reason.NULL_POINTER,
                        "No active advertiser " + advertiseId)
                ));
            }
        }
    }

    @Override
    public boolean isVisibilityOn(@NonNull String advertiseId) {
        return advertisers.containsKey(advertiseId);
    }

    @NonNull
    @Override
    public Collection<String> getAdvertisers() {
        return Collections.unmodifiableList(new ArrayList<>(advertisers.keySet()));
    }

    @Override
    public void detach() {
        for (NsdManager.RegistrationListener listener : advertisers.values())
            nsdManager.unregisterService(listener);

        advertisers.clear();
    }

    /**
     * Register and Publish an Wifi Service.
     */
    private class RegisterService implements Runnable {
        @NonNull
        private final VisibilityProperties<WifiAdvertiseSettings, WifiAdvertiseData> properties;

        /**
         * Constructor.
         *
         * @param properties visibility properties
         */
        RegisterService(@NonNull VisibilityProperties<WifiAdvertiseSettings, WifiAdvertiseData> properties) {
            this.properties = properties;
        }

        @Override
        public void run() {
            NsdManager.RegistrationListener listener = new NsdManager.RegistrationListener() {
                @Override
                public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                    advertisers.remove(properties.getIdentifier());
                    Receiver.notifyEvent(aggregator, new VisibilityOnEvent(
                            properties.getIdentifier(), LinkException.build(
                            WifiLink.mapServiceErrorToReason(errorCode),
                            "Register error " + properties.getIdentifier())));
                }

                @Override
                public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                    advertisers.remove(properties.getIdentifier());
                    Receiver.notifyEvent(aggregator, new VisibilityOffEvent(
                            properties.getIdentifier(), LinkException.build(
                            WifiLink.mapServiceErrorToReason(errorCode),
                            "Unregister error " + properties.getIdentifier())));
                }

                @Override
                public void onServiceRegistered(NsdServiceInfo serviceInfo) {
                    Receiver.notifyEvent(aggregator, new VisibilityOnEvent(
                            properties.getIdentifier()));
                }

                @Override
                public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
                    advertisers.remove(properties.getIdentifier());
                    Receiver.notifyEvent(aggregator, new VisibilityOffEvent(
                            properties.getIdentifier()));
                }
            };
            try {

                WifiAdvertiseSettings settings = properties.getAdvertiserSettings();
                Map<String, String> data = (properties.getAdvertiserData() == null)
                        ? new HashMap<>()
                        : properties.getAdvertiserData().getData();

                if (settings == null)
                    throw new Exception("Advertiser settings NULL");

                NsdServiceInfo serviceInfo = settings.getNsdServiceInfo();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    for (Map.Entry<String, String> entry : data.entrySet()) {
                        serviceInfo.setAttribute(entry.getKey(), entry.getValue());
                    }
                }

                Object prop = advertisers.putIfAbsent(properties.getIdentifier(), listener);
                if (null == prop) {
                    nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, listener);

                } else {
                    Receiver.notifyEvent(aggregator, new VisibilityOnEvent(properties.getIdentifier(),
                            LinkException.build(
                                    Reason.ALREADY_STARTED,
                                    "Visibility service already started: " + properties.getIdentifier())));
                }

            } catch (Exception e) {
                e.printStackTrace();
                Receiver.notifyEvent(aggregator, new VisibilityOnEvent(
                        properties.getIdentifier(), LinkException.build(Reason.INTERNAL_ERROR, e.getMessage())
                ));
            }
        }
    }
}
