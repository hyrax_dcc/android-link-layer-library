/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.legacy.translator;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.event.ConnectionServerOffEvent;
import org.hyrax.link.misc.event.ConnectionServerOnEvent;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.ConnectionServerLogicTranslator;
import org.hyrax.link.wifi.direct.translator.WifiP2pConnectionServerTranslator;
import org.hyrax.link.wifi.types.WifiServerSettings;

import java.util.ArrayList;
import java.util.Collection;

/**
 * The Wifi does not implement the acceptance of connections, however it is possible.
 * <br/>
 * Note that a device only allows one state at same time, or is an access point it is connected
 * to Wifi.
 * <br/>
 * Although this problem can be overcome by using the {@link WifiP2pConnectionServerTranslator},
 * where the device can either connected to Wifi and act as access point at same time.
 */
public class WifiConnectionServerTranslator implements ConnectionServerLogicTranslator<WifiServerSettings> {
    @NonNull
    private final Aggregator aggregator;

    /**
     * Constructor of {@link WifiConnectionServerTranslator} class.
     *
     * @param aggregator listeners aggregator
     */
    public WifiConnectionServerTranslator(@NonNull Aggregator aggregator) {
        this.aggregator = aggregator;
    }

    @Override
    public void turnServerOn(@NonNull ServerProperties<WifiServerSettings> properties) {
        Receiver.notifyEvent(aggregator, new ConnectionServerOnEvent(properties.getIdentifier(),
                LinkException.build(Reason.UNSUPPORTED, "Wifi does not implement connection server")
        ));
    }

    @Override
    public void turnServerOff(@NonNull String serverId) {
        Receiver.notifyEvent(aggregator, new ConnectionServerOffEvent(serverId,
                LinkException.build(Reason.UNSUPPORTED, "Wifi does not implement connection server")
        ));
    }

    @Override
    public boolean isServerOn(@NonNull String serverId) {
        return false;
    }

    @NonNull
    @Override
    public Collection<String> getActiveServers() {
        return new ArrayList<>();
    }

    @Override
    public void detach() {
    }
}
