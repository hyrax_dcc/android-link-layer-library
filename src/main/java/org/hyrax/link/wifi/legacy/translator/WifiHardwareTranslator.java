/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.legacy.translator;

import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.HardwareOffEvent;
import org.hyrax.link.misc.event.HardwareOnEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.HardwareLogicTranslator;

/**
 * Class {@link WifiHardwareTranslator} manages the wifi state. ENABLE/DISABLE
 */
public class WifiHardwareTranslator implements HardwareLogicTranslator {
    @NonNull
    private final WifiManager wifiManager;
    @NonNull
    private final Aggregator aggregator;

    /**
     * Constructor of {@link WifiHardwareTranslator} class.
     *
     * @param wifiManager wifi object manager
     * @param aggregator  listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/net/wifi/WifiManager.html" target="_blank">WifiManager</a>
     */
    public WifiHardwareTranslator(@NonNull WifiManager wifiManager, @NonNull Aggregator aggregator) {
        this.wifiManager = wifiManager;
        this.aggregator = aggregator;
    }

    @Override
    public void turnHardwareOn() {
        if (!wifiManager.setWifiEnabled(true)) {
            Receiver.notifyEvent(aggregator, new HardwareOnEvent(
                    LinkException.build(Reason.ERROR_START, "Error turning hardware on"))
            );
        }
    }

    @Override
    public void turnHardwareOff() {
        if (!wifiManager.setWifiEnabled(false)) {
            Receiver.notifyEvent(aggregator, new HardwareOffEvent(
                    LinkException.build(Reason.ERROR_STOP, "Error turning hardware off")
            ));
        }
    }

    @Override
    public boolean isHardwareOn() {
        return wifiManager.isWifiEnabled();
    }

    @Override
    public void detach() {

    }
}
