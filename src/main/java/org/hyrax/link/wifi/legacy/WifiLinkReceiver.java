/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.legacy;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.Device;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.ConnectionLostEvent;
import org.hyrax.link.misc.event.ConnectionNewEvent;
import org.hyrax.link.misc.event.DiscoveryFoundEvent;
import org.hyrax.link.misc.event.DiscoveryOffEvent;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.event.HardwareOffEvent;
import org.hyrax.link.misc.event.HardwareOnEvent;
import org.hyrax.link.wifi.legacy.translator.WifiDiscoveryTranslator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class {@link WifiLinkReceiver} receives all the Wifi events.
 */
public class WifiLinkReceiver extends Receiver {
    @NonNull
    private final WifiManager wifiManager;
    @Nullable
    private WifiDevice prevConnectedDevice;

    /**
     * Creates a {@link Receiver} object.
     * <br/>
     * It receives an object {@link Aggregator}, in order to access the registered listeners.
     *
     * @param aggregator  listener aggregator
     * @param wifiManager wifi object manager
     */
    public WifiLinkReceiver(@NonNull Aggregator aggregator, @NonNull WifiManager wifiManager) {
        super(aggregator);
        this.wifiManager = wifiManager;
    }

    @NonNull
    @Override
    protected List<Event> actionToTechEvent(@NonNull String action, @NonNull Intent intent) {
        List<Event> events = new ArrayList<>();

        switch (action) {
            case WifiManager.WIFI_STATE_CHANGED_ACTION:
                int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
                switch (state) {
                    case WifiManager.WIFI_STATE_ENABLED:
                        events.add(new HardwareOnEvent());
                        break;

                    case WifiManager.WIFI_STATE_DISABLED:
                        events.add(new HardwareOffEvent());
                        break;
                }
                break;

            case WifiManager.SCAN_RESULTS_AVAILABLE_ACTION:
                Set<Device> nodes = new HashSet<>();
                List<ScanResult> results = wifiManager.getScanResults();
                for (ScanResult scanResult : results) {
                    nodes.add(new WifiNode(new WifiDevice(scanResult)));
                }

                events.add(new DiscoveryFoundEvent(Constants.DEFAULT_SCANNER_UUID, nodes));
                if (WifiDiscoveryTranslator.isScanning())
                    events.add(new DiscoveryOffEvent(Constants.DEFAULT_SCANNER_UUID));
                break;

            case WifiManager.NETWORK_STATE_CHANGED_ACTION:
                String bssid = intent.getStringExtra(WifiManager.EXTRA_BSSID);
                NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                WifiInfo wifiInfo = intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);

                if (networkInfo.isConnected() && null != bssid && null != wifiInfo) {
                    prevConnectedDevice = new WifiDevice(wifiInfo, bssid);
                    events.add(new ConnectionNewEvent(new WifiNode(new WifiDevice(wifiInfo, bssid))));

                } else if (null != prevConnectedDevice) {
                    events.add(new ConnectionLostEvent(new WifiNode(
                            new WifiDevice((WifiInfo) prevConnectedDevice.getOriginalObject(),
                                    prevConnectedDevice.getAddress()))));
                    prevConnectedDevice = null;
                }

                break;
        }

        return events;
    }

    @NonNull
    @Override
    public IntentFilter getIntentFilters() {
        return new IntentFilter() {{
            addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
            addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        }};
    }
}
