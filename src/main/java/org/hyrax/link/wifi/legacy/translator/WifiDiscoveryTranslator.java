/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.legacy.translator;

import android.location.LocationManager;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.hyrax.link.misc.Bonjour;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.event.DiscoveryFoundEvent;
import org.hyrax.link.misc.event.DiscoveryOffEvent;
import org.hyrax.link.misc.event.DiscoveryOnEvent;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkListener;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.link.LinkContext;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.DiscoveryLogicTranslator;
import org.hyrax.link.wifi.WifiLink;
import org.hyrax.link.wifi.types.WifiScanFilter;
import org.hyrax.link.wifi.types.WifiScanRecord;
import org.hyrax.link.wifi.legacy.WifiDevice;
import org.hyrax.link.wifi.legacy.WifiNode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class {@link WifiDiscoveryTranslator} manages the wifi discovery state.
 * START_DISCOVERY/STOP_DISCOVERY
 */
public class WifiDiscoveryTranslator implements DiscoveryLogicTranslator<Void, WifiScanFilter> {
    @NonNull
    private static final ConcurrentHashMap<String, ServiceDiscovery> scanners = new ConcurrentHashMap<>();
    /**
     * Default service discovery.
     */
    private final ServiceDiscovery NO_SERVICE = new ServiceDiscovery(Constants.DEFAULT_SCANNER_UUID, null);

    @NonNull
    private final LinkContext linkContext;
    @NonNull
    private final WifiManager wifiManager;
    @NonNull
    private final NsdManager nsdManager;
    @NonNull
    private final Aggregator aggregator;
    /**
     * Listens for discovery events.
     */
    private final LinkListener eventListener = (LinkListener<String>) (eventType, outcome) -> {
        switch (eventType) {
            case LINK_DISCOVERY_ON:
                outcome.ifError(scanners::remove);
                break;
            case LINK_DISCOVERY_OFF:
                scanners.remove(outcome.getOutcome());
                break;
        }
    };

    /**
     * Constructor of {@link WifiDiscoveryTranslator} class.
     *
     * @param linkContext application context
     * @param aggregator  listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/net/wifi/WifiManager.html" target="_blank">WifiManager</a>
     */
    public WifiDiscoveryTranslator(@NonNull LinkContext linkContext, @NonNull Aggregator aggregator) {
        this.linkContext = linkContext;
        this.wifiManager = linkContext.getWifiManager();
        this.nsdManager = linkContext.getNsdManager();
        this.aggregator = aggregator;

        aggregator.listen(eventListener, EventCategory.LINK_DISCOVERY, true);
    }

    /**
     * Checks if Wifi is performing a default discovery.
     *
     * @return <tt>true</tt> if discovering, <tt>false</tt> otherwise
     */
    public static boolean isScanning() {
        return scanners.containsKey(Constants.DEFAULT_SCANNER_UUID);
    }

    @Override
    public void turnDiscoveryOn(@NonNull DiscoveryProperties<Void, WifiScanFilter> discoveryProperties) {
        LinkException exception = null;
        String scanId = discoveryProperties.getIdentifier();

        if (!wifiManager.isWifiEnabled()) {
            exception = LinkException.build(Reason.INTERNAL_ERROR, "Enable the Wifi");

        } else if (Constants.DEFAULT_SCANNER_UUID.equals(scanId)) {
            if (linkContext.getLocationManager().isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                Object prop = scanners.putIfAbsent(scanId, NO_SERVICE);
                if (null == prop) {
                    if (!wifiManager.startScan()) {
                        scanners.remove(scanId);
                        exception = LinkException.build(Reason.ERROR_START, "Error starting discovery");
                    } else {
                        Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(scanId));
                    }
                } else {
                    exception = LinkException.build(Reason.ALREADY_STARTED, "Discovery already active");
                }
            } else {
                exception = LinkException.build(Reason.LOCATION_DISABLED, "Location is disabled, please enabled it");
            }


        } else {
            startServiceDiscovery(discoveryProperties);
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(scanId, exception));
    }

    /**
     * Start the discovery of services.
     *
     * @param properties discovery properties object
     */
    private void startServiceDiscovery(@NonNull DiscoveryProperties<Void, WifiScanFilter> properties) {
        ServiceDiscovery sDiscovery = new ServiceDiscovery(properties.getIdentifier(), properties.getScannerFilter());
        Object prop = scanners.putIfAbsent(properties.getIdentifier(), sDiscovery);

        if (prop == null) {
            Bonjour bonjour = (properties.getScannerFilter() == null) ? Bonjour.SERVICE_TCP :
                    properties.getScannerFilter().getProtocol();

            nsdManager.discoverServices(bonjour.getProtocolName(), NsdManager.PROTOCOL_DNS_SD, sDiscovery);
        } else {
            LinkException exception = LinkException.build(Reason.ALREADY_STARTED,
                    "Discovery service already started: " + properties.getIdentifier());
            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(properties.getIdentifier(), exception));
        }
    }

    @Override
    public void turnDiscoveryOff(@NonNull String scanId) {
        NsdManager.DiscoveryListener service = scanners.remove(scanId);
        LinkException exception = null;

        if (null == service) {
            exception = LinkException.build(Reason.NULL_POINTER, "No active discovery");

        } else if (NO_SERVICE.equals(service)) {
            exception = LinkException.build(Reason.WARNING, "The discovery process cannot be " +
                    "interrupted Please wait for discovery off signal.");
        } else {
            nsdManager.stopServiceDiscovery(service);
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(scanId, exception));
    }

    @Override
    public boolean isDiscoveryOn(@NonNull String scanId) {
        return scanners.containsKey(scanId);
    }

    @NonNull
    @Override
    public Collection<String> getScanners() {
        return Collections.unmodifiableList(new ArrayList<>(scanners.keySet()));
    }

    @Override
    public void detach() {
        aggregator.removeListener(eventListener);
        for (NsdManager.DiscoveryListener service : scanners.values())
            if (!NO_SERVICE.equals(service))
                nsdManager.stopServiceDiscovery(service);

        scanners.clear();
    }

    /**
     * Service discovery class.
     */
    private class ServiceDiscovery implements NsdManager.DiscoveryListener {
        private final ReentrantLock LOCK = new ReentrantLock();
        private final String identifier;
        @Nullable
        private final WifiScanFilter scanFilter;

        private final Set<String> servicesName;
        private final LinkedList<NsdServiceInfo> unresolvedServices;

        /**
         * Constructor.
         *
         * @param identifier scanner identifier
         * @param scanFilter scan filter object
         */
        private ServiceDiscovery(@NonNull String identifier, @Nullable WifiScanFilter scanFilter) {
            this.identifier = identifier;
            this.scanFilter = scanFilter;
            this.servicesName = new HashSet<>();
            this.unresolvedServices = new LinkedList<>();
        }

        @Override
        public void onStartDiscoveryFailed(String serviceType, int errorCode) {
            scanners.remove(identifier);
            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(identifier, LinkException
                    .build(WifiLink.mapServiceErrorToReason(errorCode),
                            "Error start service discovery")));
        }

        @Override
        public void onStopDiscoveryFailed(String serviceType, int errorCode) {
            Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(identifier, LinkException
                    .build(WifiLink.mapServiceErrorToReason(errorCode),
                            "Error stopping service discovery")));
        }

        @Override
        public void onDiscoveryStarted(String serviceType) {
            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(identifier));
        }

        @Override
        public void onDiscoveryStopped(String serviceType) {
            Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(identifier));
        }

        @Override
        public void onServiceFound(NsdServiceInfo serviceInfo) {
            LOCK.lock();
            try {
                if (servicesName.add(serviceInfo.getServiceName())) {
                    unresolvedServices.add(serviceInfo);
                }
            } finally {
                LOCK.unlock();
            }
            resolveNext(null);
        }

        /**
         * Resolves the next service.
         *
         * @param serviceNameToRemove service name to be remove, or NULL
         */
        private void resolveNext(@Nullable String serviceNameToRemove) {
            // tries to get the lock
            if (LOCK.tryLock()) {
                try {
                    if (serviceNameToRemove != null)
                        servicesName.remove(serviceNameToRemove);

                    if (!unresolvedServices.isEmpty()) {
                        NsdServiceInfo info = unresolvedServices.removeFirst();
                        nsdManager.resolveService(info, new ServiceResolver(info.getServiceName()));
                    }
                } finally {
                    LOCK.unlock();
                }
            }
        }

        @Override
        public void onServiceLost(NsdServiceInfo serviceInfo) {
            Log.e("Wifi Discovery", "Service lost");
            //TODO - notify service lost
        }

        /**
         * Service resolver class.
         */
        private class ServiceResolver implements NsdManager.ResolveListener {
            private final String serviceName;

            private ServiceResolver(@NonNull String serviceName) {
                this.serviceName = serviceName;
            }

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                resolveNext(serviceName);
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                WifiScanRecord scanRecord = new WifiScanRecord(serviceInfo);
                //if service meets the requirements
                if (scanRecord.validOnFilter(scanFilter)) {
                    Receiver.notifyEvent(aggregator,
                            new DiscoveryFoundEvent(
                                    identifier,
                                    new WifiNode(new WifiDevice(scanRecord))));

                }
                resolveNext(serviceName);
            }
        }
    }
}
