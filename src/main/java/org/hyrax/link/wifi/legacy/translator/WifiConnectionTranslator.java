/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.legacy.translator;

import android.annotation.TargetApi;
import android.net.ProxyInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.hyrax.link.misc.event.ConnectionLostEvent;
import org.hyrax.link.misc.event.ConnectionNewEvent;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.Device;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.ConnectionLogicTranslator;
import org.hyrax.link.wifi.legacy.WifiDevice;
import org.hyrax.link.wifi.legacy.WifiNode;
import org.hyrax.link.wifi.types.WifiConnectionSettings;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link WifiConnectionTranslator} manages the wifi connections.
 * CONNECT/DISCONNECT
 */
public class WifiConnectionTranslator implements ConnectionLogicTranslator<WifiConnectionSettings> {
    @NonNull
    private final WifiManager wifiManager;
    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<Device, ConnectionProperties> devicesTracked;

    /**
     * Listen of connections events
     */
    private final LinkListener eventListener = new LinkListener<Device>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<Device> outcome) {
            switch (eventType) {
                case LINK_CONNECTION_NEW:
                    outcome.ifError(devicesTracked::remove);
                    outcome.ifSuccess(argument -> addDevice(argument));
                    break;

                case LINK_CONNECTION_LOST:
                    Device device = outcome.getOutcome();
                    ConnectionProperties properties = devicesTracked.remove(device);
                    if (null != properties && properties.toRemoveWhenDisconnect())
                        removeNetwork(getNetworkId(device.getName()));
                    break;
            }
        }
    };

    /**
     * Constructor of {@link WifiConnectionTranslator} class
     *
     * @param wifiManager wifi object manager
     * @param aggregator  listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/net/wifi/WifiManager.html" target="_blank">WifiManager</a>
     */
    public WifiConnectionTranslator(@NonNull WifiManager wifiManager, @NonNull Aggregator aggregator) {
        this.wifiManager = wifiManager;
        this.aggregator = aggregator;

        this.devicesTracked = new ConcurrentHashMap<>();

        aggregator.listen(eventListener, EventCategory.LINK_CONNECTION, true);

        if (isConnectedToWifi()) {
            WifiInfo info = wifiManager.getConnectionInfo();
            Device device = new WifiNode(new WifiDevice(info, info.getBSSID()));
            addDevice(device);
        }
    }

    /**
     * Adds a device to the connected devices list
     *
     * @param device connected device
     */
    private void addDevice(@NonNull Device device) {
        boolean rm = device.getName().startsWith("DIRECT");
        devicesTracked.putIfAbsent(device,
                ConnectionProperties.newBuilder(device.getName())
                        .removeWhenDisconnect(rm)
                        .build());
    }

    @Override
    public void turnConnectionOn(@NonNull Device remoteDevice, @NonNull ConnectionProperties<WifiConnectionSettings> properties) {
        LinkException exception = null;
        Object prop = devicesTracked.putIfAbsent(remoteDevice, properties);
        if (null == prop) {
            if (isConnectedToWifi())
                wifiManager.disconnect();

            boolean success;

            String ssid = properties.getIdentifier();
            String password = properties.getPassword();
            WifiConnectionSettings.Proxy proxy = (properties.getSettings() == null) ? null : properties.getSettings().getProxy();

            if (Constants.UNDEFINED.equals(password))
                success = connectNetwork(ssid);

            else if (proxy != null) {
                success = connectNetwork(ssid, password, proxy.host, proxy.port);

            } else {
                aggregator.listenOnce((LinkListener<Device>) (eventType, outcome) -> {
                    outcome.ifError(devicesTracked::remove);
                    outcome.ifSuccess(this::addDevice);
                }, LinkEvent.LINK_CONNECTION_NEW, remoteDevice, true);

                success = connectNetwork(ssid, password);
            }

            if (!success) {
                devicesTracked.remove(remoteDevice);
                exception = LinkException.build(Reason.ERROR_START, String.format("Connection to %s has failed", ssid));
            }

        } else {
            exception = LinkException.build(Reason.ALREADY_STARTED, "Already connected to " + properties.getIdentifier());
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new ConnectionNewEvent(remoteDevice, exception));
    }

    @Override
    public void turnConnectionOff(@NonNull Device remoteDevice) {
        LinkException exception = null;
        ConnectionProperties properties = devicesTracked.remove(remoteDevice);
        if (null == properties) {
            exception = LinkException.build(Reason.NULL_POINTER,
                    "No connected network " + remoteDevice.getName());
        } else {
            if (!wifiManager.disconnect())
                exception = LinkException.build(Reason.ERROR_STOP, "Error disconnecting network " +
                        remoteDevice.getName());
            if (properties.toRemoveWhenDisconnect()) {
                removeNetwork(getNetworkId(remoteDevice.getName()));
            }
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new ConnectionLostEvent(remoteDevice, exception));
    }

    @Override
    public boolean isConnectionOn(@NonNull Device remoteDevice) {
        return devicesTracked.containsKey(remoteDevice);
    }

    @NonNull
    @Override
    public Collection<Device> getActiveConnections() {
        return Collections.unmodifiableCollection(new ArrayList<>(devicesTracked.keySet()));
    }

    @Override
    public void detach() {
        aggregator.removeListener(eventListener);

        for (Map.Entry<Device, ConnectionProperties> entry : devicesTracked.entrySet()) {
            if (entry.getValue().toRemoveWhenDisconnect()) {
                turnConnectionOff(entry.getKey());
            }
        }
        devicesTracked.clear();
    }

    /**
     * Checks if the device is connected to wifi network.
     *
     * @return <tt>true</tt> if connected, <tt>false</tt> otherwise
     */
    private boolean isConnectedToWifi() {
        return wifiManager.getConnectionInfo().getNetworkId() != -1;
    }

    /**
     * Return a network id given a ssid.
     *
     * @param ssid network ssid
     * @return network id
     */
    private int getNetworkId(@NonNull String ssid) {
        String configSSID = "\"" + ssid + "\"";
        List<WifiConfiguration> confNetworks = wifiManager.getConfiguredNetworks();
        if (confNetworks != null) {
            for (WifiConfiguration network : confNetworks) {
                if (network.SSID.equals(configSSID)) {
                    return network.networkId;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the network ssid given a network id.
     *
     * @param networkId network id
     * @return network ssid
     */
    private String getNetworkSsid(int networkId) {
        List<WifiConfiguration> confNetworks = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration network : confNetworks) {
            if (network.networkId == networkId)
                return network.SSID;
        }
        return Constants.UNDEFINED;
    }

    /**
     * Connects to a wifi network without password.
     *
     * @param ssid network ssid
     * @return <tt>true</tt> if action success, <tt>false</tt> otherwise
     */
    private boolean connectNetwork(@NonNull String ssid) {
        int netId = getNetworkId(ssid);
        if (netId == -1) {
            WifiConfiguration conf = new WifiConfiguration();
            conf.SSID = "\"" + ssid + "\"";
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            netId = wifiManager.addNetwork(conf);
        }

        return netId != -1 && connectNetwork(netId);
    }

    /**
     * Connects to a wifi network with password.
     *
     * @param ssid     network ssid
     * @param password network passphrase
     * @return <tt>true</tt> if action success, <tt>false</tt> otherwise
     */
    private boolean connectNetwork(@NonNull String ssid, @NonNull String password) {
        return connectNetwork(ssid, password, null, 0);
    }

    /**
     * Connects to a wifi network with password and proxy settings.
     *
     * @param ssid      network ssid
     * @param password  network passphrase
     * @param proxyHost proxy host address
     * @param proxyPort proxy host port
     * @return <tt>true</tt> if action success, <tt>false</tt> otherwise
     */
    @TargetApi(value = 21)
    private boolean connectNetwork(@NonNull String ssid, @NonNull String password,
                                   @Nullable String proxyHost, int proxyPort) {
        int netId = getNetworkId(ssid);
        if (netId == -1) {
            WifiConfiguration conf = new WifiConfiguration();
            conf.SSID = "\"" + ssid + "\"";
            conf.preSharedKey = "\"" + password + "\"";

            if (null != proxyHost) {
                /*
                 * How to enable proxy
                 * //http://stackoverflow.com/questions/12486441/how-can-i-set-proxysettings-and-proxyproperties-on-android-wi-fi-connection-using
                 */
                try {
                    Class proxySettings = Class.forName("android.net.IpConfiguration$ProxySettings");
                    Class[] setProxyParams = new Class[2];
                    setProxyParams[0] = proxySettings;
                    setProxyParams[1] = ProxyInfo.class;

                    Method setProxy = conf.getClass().getDeclaredMethod("setProxy", setProxyParams);
                    setProxy.setAccessible(true);

                    ProxyInfo desiredProxy = ProxyInfo.buildDirectProxy(proxyHost, proxyPort);
                    //ProxyInfo desiredProxy = null;
                /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    desiredProxy = ProxyInfo.buildDirectProxy(proxyHost, proxyPort);
                }*/
                    Object[] methodParams = new Object[2];
                    methodParams[0] = Enum.valueOf(proxySettings, "STATIC");
                    methodParams[1] = desiredProxy;

                    setProxy.invoke(conf, methodParams);

                } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                    Log.e("Wifi Proxy", "Error setting proxy");
                    return false;
                }
            }
            netId = wifiManager.addNetwork(conf);
            wifiManager.saveConfiguration();
        }
        return netId != -1 && connectNetwork(netId);
    }


    /**
     * Connects to a wifi network given a network id.
     *
     * @param networkId network id
     * @return @return <tt>true</tt> if action success, <tt>false</tt> otherwise
     */
    private boolean connectNetwork(int networkId) {
        if (!wifiManager.enableNetwork(networkId, true)) {
            disableNetwork(networkId);
            Log.e("Wifi", "Unable to enable network through wifi manager -> "
                    + getNetworkSsid(networkId));
            return false;
        }
        return true;
    }

    /**
     * Disables (disconnects) a network given a network id.
     *
     * @param networkId network id
     * @return @return <tt>true</tt> if action success, <tt>false</tt> otherwise
     */
    private boolean disableNetwork(int networkId) {
        return wifiManager.disableNetwork(networkId) && wifiManager.saveConfiguration();
    }

    /**
     * Disables and removes a network from configuration, given a network id.
     *
     * @param networkId network id
     * @return @return <tt>true</tt> if action success, <tt>false</tt> otherwise
     */
    private boolean removeNetwork(int networkId) {
        return wifiManager.disableNetwork(networkId) && wifiManager.removeNetwork(networkId)
                && wifiManager.saveConfiguration();
    }
}
