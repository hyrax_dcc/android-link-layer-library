/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.legacy;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.ScanData;
import org.hyrax.link.wifi.types.ScanDataWifi;
import org.hyrax.link.wifi.direct.WifiDirectNode;
import org.hyrax.link.wifi.types.WifiScanRecord;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Wifi device representation.
 */
public class WifiNode implements Device {
    @NonNull
    private final WifiDevice wifiDevice;
    @NonNull
    private final Set<ScanData> scanData;

    /**
     * Constructor of {@link WifiNode} class.
     *
     * @param wifiDevice wifi device object
     */
    public WifiNode(final @NonNull WifiDevice wifiDevice) {
        this.wifiDevice = wifiDevice;
        switch (wifiDevice.getSource()) {
            case WIFI_SCAN_RECORD:
                this.scanData = new HashSet<ScanData>() {{
                    add(new ScanDataWifi((WifiScanRecord) wifiDevice.getOriginalObject()));
                }};
                break;

            case WIFI_P2P_SCAN_RECORD:
                this.scanData = new HashSet<ScanData>() {{
                    addAll(((WifiDirectNode) wifiDevice.getOriginalObject()).getScanData());
                }};

                break;

            default:
                this.scanData = new HashSet<>();
        }
    }

    @NonNull
    @Override
    public String getName() {
        return wifiDevice.getName();
    }

    @NonNull
    @Override
    public String getUniqueAddress() {
        return wifiDevice.getAddress();
    }

    @Override
    public int getRssi() {
        return wifiDevice.getRssi();
    }

    @NonNull
    @Override
    public WifiDevice getOriginalObject() {
        return wifiDevice;
    }

    @NonNull
    @Override
    public Collection<ScanData> getScanData() {
        return scanData;
    }

    @Override
    public void mergeScanData(@NonNull Collection<ScanData> scanData) {
        this.scanData.addAll(scanData);
    }

    @NonNull
    @Override
    public Technology getTechnology() {
        return Technology.WIFI;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof WifiNode && ((WifiNode) o).getOriginalObject().equals(wifiDevice);
    }

    @Override
    public int hashCode() {
        return wifiDevice.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s: %s (%s): Data %s", getTechnology().name(), getName(), getUniqueAddress(), getScanData());
    }
}
