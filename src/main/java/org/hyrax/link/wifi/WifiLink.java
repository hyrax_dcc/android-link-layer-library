/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi;

import android.net.nsd.NsdManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Bonjour;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.exception.LinkLayerRuntimeException;
import org.hyrax.link.misc.link.Translators;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.Device;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.misc.link.AbstractLink;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.wifi.direct.WifiDirectNode;
import org.hyrax.link.wifi.legacy.WifiDevice;
import org.hyrax.link.wifi.legacy.WifiLinkReceiver;
import org.hyrax.link.wifi.legacy.WifiNode;
import org.hyrax.link.wifi.legacy.translator.WifiConnectionServerTranslator;
import org.hyrax.link.wifi.legacy.translator.WifiConnectionTranslator;
import org.hyrax.link.wifi.legacy.translator.WifiDiscoveryTranslator;
import org.hyrax.link.wifi.legacy.translator.WifiHardwareTranslator;
import org.hyrax.link.wifi.legacy.translator.WifiVisibilityTranslator;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiAdvertiseSettings;
import org.hyrax.link.wifi.types.WifiConnectionSettings;
import org.hyrax.link.wifi.types.WifiScanFilter;
import org.hyrax.link.wifi.types.WifiServerSettings;

import java.util.Locale;
import java.util.UUID;

/**
 * Singleton class responsible for managing the Wifi operations.
 */
public class WifiLink extends AbstractLink<WifiLink> implements LinkFeatures {
    private static final String SERVER_NAME = "Wifi Direct Server";
    private static WifiLink instance;

    /**
     * Constructor.
     */
    private WifiLink() {
        super(Technology.WIFI);
    }

    /**
     * Returns the wifi link instance.
     *
     * @return wifi link instance
     */
    @NonNull
    public static WifiLink getInstance() {
        if (instance == null)
            instance = new WifiLink();
        return instance;
    }

    /**
     * Maps Nsd manager reason to enum Reason
     *
     * @param reason integer reason
     * @return enum reason
     */
    @NonNull
    public static Reason mapServiceErrorToReason(int reason) {
        switch (reason) {
            case NsdManager.FAILURE_ALREADY_ACTIVE:
                return Reason.ALREADY_STARTED;

            case NsdManager.FAILURE_MAX_LIMIT:
                return Reason.TOO_MANY_ACTIVE;

            default:
                return Reason.INTERNAL_ERROR;
        }
    }

    @NonNull
    @Override
    protected WifiLink getFeatures() {
        return this;
    }

    @NonNull
    @Override
    protected Translators initTranslators() {
        WifiManager wifiManager = linkContext.getWifiManager();

        return new Translators(new WifiLinkReceiver(aggregator, wifiManager),
                new WifiHardwareTranslator(wifiManager, aggregator),
                new WifiDiscoveryTranslator(linkContext, aggregator),
                new WifiVisibilityTranslator(linkContext, aggregator, workerHandler),
                new WifiConnectionServerTranslator(aggregator),
                new WifiConnectionTranslator(wifiManager, aggregator));
    }

    @Override
    protected void onAttach() {

    }

    @Override
    protected void onDetach() {
        instance = null;
    }

    @Override
    public DiscoveryProperties.Builder<Void, WifiScanFilter> newDiscoveryBuilder() {
        return DiscoveryProperties.<Void, WifiScanFilter>newBuilder()
                .stopAfterTimeoutExpiration(false);
    }

    /**
     * Returns a service discovery builder, filtered by service name
     *
     * @param serviceName name of the service to filter
     * @return discovery object builder
     */
    @NonNull
    public DiscoveryProperties.Builder<Void, WifiScanFilter> newDiscoveryBuilder(@NonNull String serviceName) {
        return DiscoveryProperties.<Void, WifiScanFilter>newBuilder()
                .setScanId(UUID.randomUUID().toString())
                .setScannerFilter(WifiScanFilter.newBuilder()
                        .setServiceName(serviceName)
                        .setProtocol(Bonjour.SERVICE_TCP).build())
                .stopAfterTimeoutExpiration(true);
    }

    /**
     * This method will intentionally fail. Use {@link WifiLink#newVisibilityBuilder(String, int)} instead.
     *
     * @return visibility properties builder
     */
    @Override
    public VisibilityProperties.Builder<WifiAdvertiseSettings, WifiAdvertiseData> newVisibilityBuilder() {
        return VisibilityProperties.newBuilder();
    }

    /**
     * Returns the default service visibility builder
     *
     * @param serviceName name of the service
     * @param port        listen service port
     * @return visibility properties builder
     */
    @NonNull
    public VisibilityProperties.Builder<WifiAdvertiseSettings, WifiAdvertiseData>
    newVisibilityBuilder(@NonNull String serviceName, int port) {
        return VisibilityProperties.<WifiAdvertiseSettings, WifiAdvertiseData>newBuilder()
                .setAdvertiseId(UUID.randomUUID().toString())
                .setAdvertiserSettings(
                        WifiAdvertiseSettings.newBuilder(serviceName)
                                .setProtocol(Bonjour.SERVICE_TCP)
                                .setPort(port)
                                .build());
    }

    @Override
    public ConnectionProperties.Builder<WifiConnectionSettings> newConnectionBuilder(@NonNull Device device) {
        return ConnectionProperties.newBuilder(device.getName());
    }

    @Override
    public ServerProperties.Builder<WifiServerSettings> newServerBuilder() {
        return ServerProperties.<WifiServerSettings>newBuilder()
                .setName(SERVER_NAME);
    }

    /**
     * Transforms Wifi direct nodes object to Wifi legacy object.
     *
     * @param device wifi direct node object
     * @param ssid   wifi network ssid
     * @return a device object
     */
    @NonNull
    public Device toWifiNode(@NonNull Device device, @NonNull String ssid) {
        if (device instanceof WifiDirectNode)
            return new WifiNode(new WifiDevice((WifiDirectNode) device, ssid));
        throw new LinkLayerRuntimeException("Only WifiDirectNode objects supported");
    }

    /**
     * Returns the ip address of wifi interface or {@value Constants#UNDEFINED}
     * in case the ip is not available.
     *
     * @return string ip representation or {@value Constants#UNDEFINED}
     */
    @NonNull
    public String getIpAddress() {
        WifiManager manager = linkContext.getWifiManager();
        WifiInfo info = manager.getConnectionInfo();
        int ipAddress = info.getIpAddress();
        return String.format(Locale.ENGLISH, "%d.%d.%d.%d",
                (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
                (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
    }
}
