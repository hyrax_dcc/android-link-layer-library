/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi;

import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Bonjour;
import org.hyrax.link.misc.link.Translators;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.Device;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.misc.link.AbstractLink;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.wifi.direct.WifiDirectCredentials;
import org.hyrax.link.wifi.legacy.translator.WifiHardwareTranslator;
import org.hyrax.link.wifi.direct.WifiDirectLinkReceiver;
import org.hyrax.link.wifi.direct.translator.WifiP2pConnectionServerTranslator;
import org.hyrax.link.wifi.direct.translator.WifiP2pConnectionTranslator;
import org.hyrax.link.wifi.direct.translator.WifiP2pDiscoveryTranslator;
import org.hyrax.link.wifi.direct.translator.WifiP2pVisibilityTranslator;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiAdvertiseSettings;
import org.hyrax.link.wifi.types.WifiConnectionSettings;
import org.hyrax.link.wifi.types.WifiScanFilter;
import org.hyrax.link.wifi.types.WifiServerSettings;

import java.util.UUID;

/**
 * This class is responsible for managing the Wifi P2p operations.
 */
public class WifiDirectLink extends AbstractLink<WifiDirectLink> implements LinkFeatures {
    private static final String SERVER_NAME = "Wifi Direct Server";
    private static WifiDirectLink instance;

    /**
     * Constructor.
     */
    private WifiDirectLink() {
        super(Technology.WIFI_DIRECT);
    }

    /**
     * Returns the wifi direct link instance.
     *
     * @return wifi direct link instance
     */
    @NonNull
    public static WifiDirectLink getInstance() {
        if (instance == null)
            instance = new WifiDirectLink();
        return instance;
    }

    /**
     * Maps the Integer error into string errors representation.
     *
     * @param reason integer reason
     * @return string error representation
     */
    @NonNull
    public static Reason mapReasonToReason(int reason) {
        switch (reason) {
            case WifiP2pManager.BUSY:
                return Reason.BUSY;
            case WifiP2pManager.P2P_UNSUPPORTED:
                return Reason.UNSUPPORTED;
            default:
                return Reason.INTERNAL_ERROR;
        }
    }

    @NonNull
    @Override
    protected Translators initTranslators() {
        //wifi p2p object
        WifiP2pManager wifiP2pManager = linkContext.getWifiP2pManager();
        WifiP2pManager.Channel wifiP2pChannel = wifiP2pManager.initialize(linkContext, workerHandler.getLooper(), null);
        //legacy wifi object
        WifiManager wifiManager = linkContext.getWifiManager();

        return new Translators(new WifiDirectLinkReceiver(aggregator),
                new WifiHardwareTranslator(wifiManager, aggregator),
                new WifiP2pDiscoveryTranslator(wifiP2pManager, wifiP2pChannel, aggregator),
                new WifiP2pVisibilityTranslator(wifiP2pManager, wifiP2pChannel, aggregator),
                new WifiP2pConnectionServerTranslator(wifiP2pManager, wifiP2pChannel, aggregator),
                new WifiP2pConnectionTranslator(wifiP2pManager, wifiP2pChannel, aggregator)
        );
    }

    @Override
    protected void onAttach() {

    }

    @Override
    protected void onDetach() {
        instance = null;
    }

    @NonNull
    @Override
    protected WifiDirectLink getFeatures() {
        return this;
    }

    @Override
    public DiscoveryProperties.Builder<Void, WifiScanFilter> newDiscoveryBuilder() {
        return DiscoveryProperties.<Void, WifiScanFilter>newBuilder()
                .stopAfterTimeoutExpiration(false);
    }

    /**
     * Returns a service discovery builder, filtered by service name
     *
     * @param serviceName name of the service to filter
     * @return discovery object builder
     */
    @NonNull
    public DiscoveryProperties.Builder<Void, WifiScanFilter> newDiscoveryBuilder(@NonNull String serviceName) {
        return DiscoveryProperties.<Void, WifiScanFilter>newBuilder()
                .setScanId(UUID.randomUUID().toString())
                .setScannerFilter(WifiScanFilter.newBuilder()
                        .setServiceName(serviceName)
                        .setProtocol(Bonjour.PRESENCE_TCP).build())
                .stopAfterTimeoutExpiration(true);
    }

    @Override
    public VisibilityProperties.Builder<WifiAdvertiseSettings, WifiAdvertiseData> newVisibilityBuilder() {
        return VisibilityProperties.newBuilder();
    }

    /**
     * Returns a service visibility builder
     *
     * @param serviceName   name of the service
     * @param port          listen service port
     * @param advertiseData extra data to advertise
     * @return visibility properties builder
     */
    @NonNull
    public VisibilityProperties.Builder<WifiAdvertiseSettings, WifiAdvertiseData>
    newVisibilityBuilder(@NonNull String serviceName, int port, @NonNull WifiAdvertiseData advertiseData) {
        return newVisibilityBuilder(serviceName, port, false, advertiseData);
    }

    /**
     * Returns a service visibility builder
     *
     * @param serviceName   name of the service
     * @param port          listen service port
     * @param forceDiscover force to go discover
     * @param advertiseData extra data to advertise
     * @return visibility properties builder
     */
    @NonNull
    public VisibilityProperties.Builder<WifiAdvertiseSettings, WifiAdvertiseData>
    newVisibilityBuilder(@NonNull String serviceName, int port, boolean forceDiscover,
                         @NonNull WifiAdvertiseData advertiseData) {
        return newVisibilityBuilder(WifiAdvertiseSettings
                .newBuilder(serviceName)
                .setProtocol(Bonjour.PRESENCE_TCP)
                .setPort(port)
                .forceToGoDiscovery(forceDiscover)
                .build(), advertiseData);
    }

    /**
     * Returns a new service visibility builder.
     *
     * @param settings service settings
     * @param data     service data
     * @return visibility properties object
     */
    @NonNull
    public VisibilityProperties.Builder<WifiAdvertiseSettings, WifiAdvertiseData>
    newVisibilityBuilder(@NonNull WifiAdvertiseSettings settings, @NonNull WifiAdvertiseData data) {
        return VisibilityProperties.<WifiAdvertiseSettings, WifiAdvertiseData>newBuilder()
                .setAdvertiseId(UUID.randomUUID().toString())
                .stopAfterTimeoutExpiration(true)
                .setAdvertiserSettings(settings)
                .setAdvertiserData(data);
    }


    @Override
    public ConnectionProperties.Builder<WifiConnectionSettings> newConnectionBuilder(@NonNull Device device) {
        return ConnectionProperties.newBuilder(device.getUniqueAddress());
    }

    @Override
    public ServerProperties.Builder<WifiServerSettings> newServerBuilder() {
        return ServerProperties.<WifiServerSettings>newBuilder()
                .setName(SERVER_NAME);
    }

    /**
     * Returns the wifi p2p authentication credentials.
     * NOTE: this only works if the device is the group owner.
     *
     * @param serverId server identifier
     * @return proto buf object with credentials
     */
    @NonNull
    public WifiDirectCredentials getWifiP2pCredentials(@NonNull String serverId) {
        return ((WifiP2pConnectionServerTranslator) translators.connectionServerLogicTranslator)
                .getWifiP2pCredentials(serverId);
    }

    /**
     * Returns the default advertise settings for Wifi Direct.
     *
     * @param serviceName name if the service
     * @return a advertise settings builder object
     */
    @NonNull
    public WifiAdvertiseSettings.Builder getDefaultAdvertiseSettings(@NonNull String serviceName) {
        return WifiAdvertiseSettings.newBuilder(serviceName)
                .setProtocol(Bonjour.PRESENCE_TCP);
    }
}
