/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.types;

import android.net.nsd.NsdServiceInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.Bonjour;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Service discovery Wifi Record.
 * <br/>
 * This class contains the the discovery service data
 */
public class WifiScanRecord {
    private final String serviceName;
    @NonNull
    private final Bonjour protocol;
    private final String host;
    private final int port;
    @NonNull
    private final Map<String, String> extraData;

    /**
     * Constructs a {@link WifiScanRecord} object
     * given a {@link NsdServiceInfo} object.<b>Wifi Only</b>
     *
     * @param serviceInfo service info object
     */
    public WifiScanRecord(@NonNull NsdServiceInfo serviceInfo) {
        // replace string ended' (<number>)'
        this.serviceName = serviceInfo.getServiceName()
                .replaceAll("(\\s+)?\\(\\d+\\)$", "");
        this.protocol = Bonjour.translate(serviceInfo.getServiceType());
        this.host = serviceInfo.getHost().getHostAddress();
        this.port = serviceInfo.getPort();
        this.extraData = new HashMap<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            for (Map.Entry<String, byte[]> entry : serviceInfo.getAttributes().entrySet()) {
                extraData.put(entry.getKey(), new String(entry.getValue()));
            }
        }
    }

    /**
     * Constructs a {@link WifiScanRecord} object given a set of parameters.<b>Wifi P2p Only</b>
     *
     * @param serviceName      service instance name (service name)
     * @param registrationType bonjour registration type
     * @param extraData        attributes data
     */
    public WifiScanRecord(@NonNull String serviceName, @NonNull String registrationType, @NonNull Map<String, String> extraData) {
        this.serviceName = serviceName;
        this.protocol = Bonjour.translate(registrationType);
        this.host = (extraData.containsKey(Constants.HOST)) ? extraData.get(Constants.HOST) : Constants.UNDEFINED;
        this.port = (extraData.containsKey(Constants.PORT)) ? Integer.valueOf(extraData.get(Constants.PORT)) : 0;
        this.extraData = extraData;
    }

    /**
     * Returns the service instance name.
     *
     * @return instance name
     */
    @NonNull
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Returns the service bonjour protocol use on the discovery
     *
     * @return bonjour protocol
     */
    @NonNull
    private Bonjour getProtocol() {
        return protocol;
    }

    /**
     * Returns a string that represents the host in the networks.
     * <br/>
     * E.g In Wifi case returns an ip address.
     *
     * @return host name
     */
    @NonNull
    public String getHost() {
        return host;
    }

    /**
     * Returns the port where's the service is listening.
     *
     * @return port number
     */
    public int getPort() {
        return port;
    }

    /**
     * Returns the service attributes data (key/value).
     *
     * @return attributes map
     */
    @NonNull
    public Map<String, String> getExtraData() {
        return extraData;
    }


    /**
     * Verifies if the current scan record meets the filter requirements.
     *
     * @param filter filter object
     * @return <tt>true</tt> if valid filter, <tt>false</tt> otherwise
     */
    public boolean validOnFilter(@Nullable WifiScanFilter filter) {
        //if service name defined
        if (filter != null && !filter.getServiceName().equals(Constants.UNDEFINED))
            return filter.getServiceName().equals(serviceName);
        return filter == null || filter.getProtocol() == Bonjour.NONE || filter.getProtocol() == protocol;
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "WifiScanRecord: %s (%s) || Host: %s | Port: %d -> Data %s",
                getServiceName(), getProtocol().toString(), getHost(), getPort(), getExtraData());
    }
}
