/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.types;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.ScanData;

import java.util.Objects;

/**
 * This class represents the scanned data for Wifi.
 */
public class ScanDataWifi extends ScanData<WifiScanRecord> {

    /**
     * Constructs a {@link ScanDataWifi} object given a wifi {@link WifiScanRecord}.
     *
     * @param originalObject wifi {@link ScanDataWifi} object.
     */
    public ScanDataWifi(@NonNull WifiScanRecord originalObject) {
        super(originalObject);
    }

    @Override
    public String toString() {
        return originalObject.toString();
    }

    @Override
    public boolean equals(Object o) {
        return o == this ||
                ((o instanceof ScanDataWifi) &&
                        ((ScanDataWifi) o).originalObject.getServiceName()
                                .equals(originalObject.getServiceName())
                );
    }

    @Override
    public int hashCode() {
        return Objects.hash(originalObject.getServiceName());
    }
}
