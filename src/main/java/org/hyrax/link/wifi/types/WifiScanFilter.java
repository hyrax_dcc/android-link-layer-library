/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.types;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.Bonjour;
import org.hyrax.link.misc.Filter;

/**
 * This class is used to filter discovery wifi results.
 */
public class WifiScanFilter implements Filter<Device> {
    private final String serviceName;
    private final Bonjour protocol;
    private final Filter<Device> filter;

    /**
     * Constructs the {@link WifiScanFilter} class given a builder.
     *
     * @param builder {@link WifiScanFilter} builder
     */
    private WifiScanFilter(@NonNull Builder builder) {
        this.serviceName = builder.serviceName;
        this.protocol = builder.protocol;
        this.filter = builder.filter;
    }

    /**
     * Instantiates a builder object in order to defined the intended parameters.
     *
     * @return a {@link Builder} object
     */
    @NonNull
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Returns the service instance name
     *
     * @return instance name
     */
    @NonNull
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Returns the service bonjour protocol.
     *
     * @return bonjour protocol
     */
    @NonNull
    public Bonjour getProtocol() {
        return protocol;
    }

    /**
     * Checks if the class is builder using default parameters.
     *
     * @return <tt>true</tt> if default, <tt>false</tt> otherwise
     */
    public boolean isDefault() {
        return !hasInstance() && !hasProtocol();
    }

    /**
     * Checks if it has a defined instance.
     *
     * @return <tt>true</tt> if instance is defined, <tt>false</tt> otherwise.
     */
    private boolean hasInstance() {
        return !Constants.UNDEFINED.equals(serviceName);
    }

    /**
     * Checks if it has a defined protocol.
     *
     * @return <tt>true</tt> if protocol is defined, <tt>false</tt> otherwise.
     */
    private boolean hasProtocol() {
        return !(Bonjour.NONE == protocol);
    }

    @Override
    public boolean apply(Device argument) {
        return filter.apply(argument);
    }

    /**
     * {@link WifiScanFilter} class builder
     */
    public static class Builder {
        private String serviceName;
        private Bonjour protocol;
        private Filter<Device> filter;

        /**
         * Builder constructor.
         */
        private Builder() {
            this.serviceName = Constants.UNDEFINED;
            this.protocol = Bonjour.NONE;
            this.filter = Filter.NO_FILTER;
        }

        /**
         * Changes the instance name to filter.
         *
         * @param serviceName service instance name
         * @return builder object
         */
        @NonNull
        public Builder setServiceName(@NonNull String serviceName) {
            this.serviceName = serviceName;
            return this;
        }

        /**
         * Changes the bonjour protocol to filter.
         *
         * @param protocol service bonjour protocol
         * @return builder object
         */
        @NonNull
        public Builder setProtocol(@NonNull Bonjour protocol) {
            this.protocol = protocol;
            return this;
        }

        /**
         * Changes the discovery filter.
         *
         * @param filter discovery filter object
         * @return builder object
         */
        @NonNull
        public Builder setFilter(@NonNull Filter<Device> filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Constructs and returns the {@link WifiScanFilter} object.
         *
         * @return {@link WifiScanFilter} object
         */
        @NonNull
        public WifiScanFilter build() {
            return new WifiScanFilter(this);
        }
    }
}
