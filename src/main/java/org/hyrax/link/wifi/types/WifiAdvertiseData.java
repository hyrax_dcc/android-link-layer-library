/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.types;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class represents the advertise data for Wifi and Wifi P2p.
 * The data is represented by pairs of (key, values)
 */
public class WifiAdvertiseData {
    private final Map<String, String> data;

    /**
     * Constructor.
     */
    public WifiAdvertiseData() {
        this.data = new HashMap<>();
    }

    /**
     * Constructor.
     *
     * @param data object data to be added
     */
    public WifiAdvertiseData(@NonNull Map<String, String> data) {
        this.data = new HashMap<>();
        for (Map.Entry<String, String> entry : data.entrySet())
            addData(entry.getKey(), entry.getValue());
    }

    /**
     * Adds data to the map.
     *
     * @param key   string key
     * @param value string value
     */
    public void addData(@NonNull String key, @NonNull String value) {
        data.put(key, value);
    }

    /**
     * Returns a ready only set keys.
     *
     * @return set object of string
     */
    @NonNull
    public Set<String> getDataKeys() {
        return Collections.unmodifiableSet(data.keySet());
    }

    /**
     * Returns the map data object
     *
     * @return map object
     */
    @NonNull
    public Map<String, String> getData() {
        return data;
    }

    /**
     * Verifies if key exists.
     *
     * @param key string key
     * @return <tt>true</tt> if key exists, <tt>false</tt> otherwise
     */
    public boolean hasKey(@NonNull String key) {
        return data.containsKey(key);
    }

    /**
     * Gets the value associated with key or {@value Constants#UNDEFINED} in case the key does
     * not exists.
     *
     * @param key string key
     * @return the corresponding value or {@value Constants#UNDEFINED}
     */
    public String getValue(@NonNull String key) {
        return getValue(key, Constants.UNDEFINED);
    }

    /**
     * Gets the value associated with key. In case the key does not exists returns {@param valueDefault}.
     *
     * @param key          string key
     * @param valueDefault default return value
     * @return the corresponding value or {@param valueDefault}
     */
    private String getValue(@NonNull String key, @NonNull String valueDefault) {
        String val = data.get(key);
        return (val == null) ? valueDefault : val;
    }
}
