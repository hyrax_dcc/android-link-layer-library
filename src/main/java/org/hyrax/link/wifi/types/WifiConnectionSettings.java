/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.types;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Constants;

/**
 * Wifi connection settings.
 */
public class WifiConnectionSettings {
    @Nullable
    private final Proxy proxy;
    private final String wdPin;

    /**
     * Constructor.
     *
     * @param builder object builder
     */
    private WifiConnectionSettings(Builder builder) {
        this.proxy = builder.proxy;
        this.wdPin = builder.wdPin;
    }

    /**
     * Instantiates a builder object in order to defined connection settings.
     *
     * @return a {@link Builder} object
     */
    @NonNull
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Returns the proxy settings
     *
     * @return proxy object
     */
    @Nullable
    public Proxy getProxy() {
        return proxy;
    }

    /**
     * Returns the Wifi Direct WPS pin to enable the auto connect.
     *
     * @return a string WPS pin
     */
    @NonNull
    public String getWdPin() {
        return wdPin;
    }

    /**
     * Wifi connection settings builder object.
     */
    public static final class Builder {
        @Nullable
        private Proxy proxy;
        private String wdPin;

        private Builder() {
            this.proxy = null;
            this.wdPin = Constants.UNDEFINED;
        }

        /**
         * Sets proxy settings
         *
         * @param proxy proxy object
         * @return builder object
         */
        public Builder setProxy(@NonNull Proxy proxy) {
            this.proxy = proxy;
            return this;
        }

        /**
         * Changes the WPS pin for Wifi Direct.
         * <p/>
         * The idea is to enable the Wifi Direct auto connect, bypassing the user dialog
         * confirmation.
         *
         * @param pin a string pin
         * @return a builder object
         */
        public Builder setWdPin(@NonNull String pin) {
            this.wdPin = pin;
            return this;
        }

        /**
         * Builds and returns the wifi connection settings object
         *
         * @return WifiConnectionSettings object
         */
        public WifiConnectionSettings build() {
            return new WifiConnectionSettings(this);
        }
    }

    /**
     * Proxy object
     */
    public class Proxy {
        public final String host;
        public final int port;

        /**
         * Constructor.
         *
         * @param host proxy host
         * @param port proxy listen port
         */
        public Proxy(@NonNull String host, int port) {
            this.host = host;
            this.port = port;
        }
    }
}
