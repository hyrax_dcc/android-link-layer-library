/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.types;

import android.net.nsd.NsdServiceInfo;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.Bonjour;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;

import java.net.InetAddress;

/**
 * This class represents the advertise settings for Wifi and Wifi P2p based on Bonjour services.
 * <br/>
 * More information <a href="https://en.wikipedia.org/wiki/Zero-configuration_networking" target="_blank">Zero-configuration networking</a>
 */
public class WifiAdvertiseSettings {
    @NonNull
    private final String serviceName;
    private final Bonjour protocol;
    private final int port;
    private final String host;
    private final boolean forceToGoDiscover;

    /**
     * Constructor of {@link WifiAdvertiseSettings} class.
     *
     * @param builder builder object
     */
    private WifiAdvertiseSettings(@NonNull Builder builder) {
        this.serviceName = builder.serviceName;
        this.protocol = builder.protocol;
        this.port = builder.port;
        this.host = builder.host;
        this.forceToGoDiscover = builder.forceToGoDiscovery;
    }

    /**
     * Instantiates a builder object in order to defined the intended parameters.
     *
     * @param serviceName instance/service name
     * @return a {@link Builder} object
     */
    @NonNull
    public static Builder newBuilder(@NonNull String serviceName) {
        return new Builder(serviceName);
    }

    /**
     * Returns the name of instance/service.
     *
     * @return instance name
     */
    @NonNull
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Returns the bonjour service protocol object.
     *
     * @return bonjour object
     */
    @NonNull
    public Bonjour getProtocol() {
        return protocol;
    }

    /**
     * Returns the service server listen port, if any.
     *
     * @return listen port
     */
    @IntRange(from = 0, to = 65535)
    public int getPort() {
        return port;
    }

    /**
     * Returns the host address.
     *
     * @return host address
     */
    @NonNull
    public String getHost() {
        return host;
    }

    /**
     * Returns a boolean meaning to force to discover.
     *
     * @return <tt>true</tt> to force discover <tt>false</tt> otherwise
     */
    public boolean isToForceDiscover() {
        return forceToGoDiscover;
    }

    /**
     * Returns a {@link NsdServiceInfo} object. <b>Wifi only</b>
     *
     * @return {@link NsdServiceInfo} object
     * @throws Exception throws an error if the port is not set
     * @see <a href="https://developer.android.com/reference/android/net/nsd/NsdServiceInfo.html" target="_blank">NsdServiceInfo</a>
     */
    @NonNull
    public NsdServiceInfo getNsdServiceInfo() throws Exception {
        NsdServiceInfo info = new NsdServiceInfo();
        info.setServiceName(serviceName);
        info.setServiceType(protocol.getProtocolName());
        if (port == 0) throw LinkException.build(Reason.INVALID_INSTANCE, "A port must be set.");
        info.setPort(port);
        if (Constants.UNDEFINED.equals(host))
            info.setHost(InetAddress.getLocalHost());
        else
            info.setHost(InetAddress.getByName(host));
        return info;
    }

    /**
     * Builder of {@link WifiAdvertiseSettings} class.
     */
    public static final class Builder {
        @NonNull
        private final String serviceName;
        private Bonjour protocol;
        private int port;
        private String host;
        private boolean forceToGoDiscovery;

        /**
         * Constructor of {@link Builder} class.
         *
         * @param serviceName service name
         */
        private Builder(@NonNull String serviceName) {
            this.serviceName = serviceName;
            this.protocol = Bonjour.PRESENCE_TCP;
            this.port = 0;
            this.host = Constants.UNDEFINED;
            this.forceToGoDiscovery = false;
        }

        /**
         * Changes the bonjour protocol.
         *
         * @param protocol bonjour protocol
         * @return builder object
         */
        @NonNull
        public Builder setProtocol(@NonNull Bonjour protocol) {
            this.protocol = protocol;
            return this;
        }

        /**
         * Changes the server listen port.
         *
         * @param port listen port
         * @return builder object
         */
        @NonNull
        public Builder setPort(int port) {
            this.port = port;
            return this;
        }

        /**
         * Set the host address
         *
         * @param address host address
         * @return builder object
         */
        @NonNull
        public Builder setHost(String address) {
            this.host = address;
            return this;
        }

        /**
         * Forces to go discover.
         * <br/>
         * With Wifi P2p may be helpful to force the discovery, because the services advertisement
         * are not very stable.
         *
         * @param force <tt>true</tt> force, <tt>false</tt> do not force
         * @return builder object
         */
        @NonNull
        public Builder forceToGoDiscovery(boolean force) {
            this.forceToGoDiscovery = force;
            return this;
        }

        /**
         * Returns a {@link WifiAdvertiseSettings} object with defined properties.
         *
         * @return wifi advertise settings object.
         */
        @NonNull
        public WifiAdvertiseSettings build() {
            return new WifiAdvertiseSettings(this);
        }
    }
}
