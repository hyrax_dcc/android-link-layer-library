/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.types;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;

/**
 * This class represents the Wifi and Wifi Direct server settings.
 */
public class WifiServerSettings {
    private final boolean enableProxy;
    private final String wdPin;

    /**
     * Constructor.
     *
     * @param builder object builder
     */
    private WifiServerSettings(@NonNull Builder builder) {
        this.enableProxy = builder.enableProxy;
        this.wdPin = builder.wdPin;
    }

    /**
     * Instantiates a builder object in order to defined server settings.
     *
     * @return a {@link Builder} object
     */
    @NonNull
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Verifies to is to enable the proxy or not.
     *
     * @return <tt>true</tt> if to enable, <tt>false</tt> otherwise
     */
    public boolean enableProxy() {
        return enableProxy;
    }

    /**
     * Returns the Wifi Direct WPS pin to enable the auto connect.
     *
     * @return a string WPS pin
     */
    @NonNull
    public String getWdPin() {
        return wdPin;
    }

    /**
     * Builder of class WifiServerSettings.
     */
    public static final class Builder {
        private boolean enableProxy;
        private String wdPin;

        /**
         * Constructor.
         */
        private Builder() {
            this.enableProxy = false;
            this.wdPin = Constants.UNDEFINED;
        }

        /**
         * Changes the enable proxy state.
         *
         * @param enable <tt>true</tt> if to enable, <tt>false</tt> otherwise
         * @return a builder object
         */
        @NonNull
        public Builder enableProxy(boolean enable) {
            this.enableProxy = enable;
            return this;
        }

        /**
         * Changes the WPS pin for Wifi Direct.
         * <p/>
         * The idea is to enable the Wifi Direct auto connect, bypassing the user dialog
         * confirmation.
         *
         * @param pin a string pin
         * @return a builder object
         */
        @NonNull
        public Builder setWdPin(@NonNull String pin) {
            this.wdPin = pin;
            return this;
        }

        /**
         * Builds and returns the WifiServerSettings object
         *
         * @return WifiServerSettings object
         */
        public WifiServerSettings build() {
            return new WifiServerSettings(this);
        }
    }
}
