/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.direct.translator;

import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.annotation.NonNull;
import android.util.Log;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.event.ConnectionLostEvent;
import org.hyrax.link.misc.event.ConnectionNewEvent;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.Device;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.ConnectionLogicTranslator;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.types.WifiConnectionSettings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link WifiP2pConnectionTranslator} manages the wifi p2p connections.
 * CONNECT/DISCONNECT
 */
public class WifiP2pConnectionTranslator implements ConnectionLogicTranslator<WifiConnectionSettings> {
    @NonNull
    private final WifiP2pManager wifiP2pManager;
    @NonNull
    private final WifiP2pManager.Channel wifiP2pChannel;
    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<Device, ConnectionProperties> connectedDevices;
    /**
     * Listens for connection events.
     */
    private final LinkListener clientListener = new LinkListener<Device>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<Device> outcome) {
            switch (eventType) {
                case LINK_CONNECTION_NEW:
                    //if error
                    outcome.ifError(argument -> {
                        wifiP2pManager.cancelConnect(wifiP2pChannel, null);
                        connectedDevices.remove(argument);
                    });
                    //if success
                    outcome.ifSuccess(argument -> connectedDevices.putIfAbsent(argument,
                            ConnectionProperties
                                    .newBuilder(argument.getUniqueAddress())
                                    .build()));
                    break;

                case LINK_CONNECTION_LOST:
                    connectedDevices.remove(outcome.getOutcome());
                    break;
            }
        }
    };

    /**
     * Constructor of {@link WifiP2pConnectionTranslator} class.
     *
     * @param wifiP2pManager wifi p2p object manager
     * @param wifiP2pChannel wifi p2p channel
     * @param aggregator     listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pManager.html" target="_blank">WifiP2pManager</a>
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pManager.Channel.html" target="_blank">WifiP2pManager.Channel</a>
     */
    public WifiP2pConnectionTranslator(@NonNull WifiP2pManager wifiP2pManager,
                                       @NonNull WifiP2pManager.Channel wifiP2pChannel,
                                       @NonNull Aggregator aggregator) {
        this.wifiP2pManager = wifiP2pManager;
        this.wifiP2pChannel = wifiP2pChannel;
        this.aggregator = aggregator;

        connectedDevices = new ConcurrentHashMap<>();
        aggregator.listen(clientListener, EventCategory.LINK_CONNECTION, true);
    }

    @Override
    public void turnConnectionOn(@NonNull final Device remoteDevice,
                                 @NonNull final ConnectionProperties<WifiConnectionSettings> properties) {
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = remoteDevice.getUniqueAddress();
        config.wps.setup = WpsInfo.PBC;
        config.groupOwnerIntent = 0;
        if (properties.getSettings() != null &&
                !Constants.UNDEFINED.equals(properties.getSettings().getWdPin())) {
            config.wps.pin = properties.getSettings().getWdPin();
        }

        Object prop = connectedDevices.putIfAbsent(remoteDevice, properties);
        if (null == prop) {
            wifiP2pManager.connect(wifiP2pChannel, config, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                }

                @Override
                public void onFailure(int reason) {
                    connectedDevices.remove(remoteDevice);
                    Receiver.notifyEvent(aggregator, new ConnectionNewEvent(remoteDevice,
                            LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                                    "Error connecting to " + remoteDevice)
                    ));
                }
            });
        } else {
            Receiver.notifyEvent(aggregator, new ConnectionNewEvent(remoteDevice,
                    LinkException.build(Reason.ALREADY_STARTED, "Device already connected " + remoteDevice.getUniqueAddress())
            ));
        }
    }

    @Override
    public void turnConnectionOff(@NonNull final Device remoteDevice) {
        final WifiP2pManager.ActionListener listener = new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int reason) {
                Receiver.notifyEvent(aggregator, new ConnectionLostEvent(remoteDevice,
                        LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                                "Error disconnecting from " + remoteDevice)
                ));
            }
        };

        cancelConnect(3, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                wifiP2pManager.removeGroup(wifiP2pChannel, listener);
            }

            @Override
            public void onFailure(int reason) {
                wifiP2pManager.removeGroup(wifiP2pChannel, listener);
            }
        });
        connectedDevices.remove(remoteDevice);
    }

    /**
     * Forces to cancel a connection.
     *
     * @param attempts number of attempts
     * @param listener action listener
     */
    private void cancelConnect(int attempts, @NonNull WifiP2pManager.ActionListener listener) {
        if (attempts == 0)
            listener.onFailure(WifiP2pManager.ERROR);
        else {
            wifiP2pManager.cancelConnect(wifiP2pChannel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    listener.onSuccess();
                }

                @Override
                public void onFailure(int reason) {
                    Log.e("P2p Conn ", WifiDirectLink.mapReasonToReason(reason).name());
                    try {
                        Thread.sleep(500);
                        cancelConnect(attempts - 1, listener);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public boolean isConnectionOn(@NonNull Device remoteDevice) {
        return connectedDevices.containsKey(remoteDevice);
    }

    @NonNull
    @Override
    public Collection<Device> getActiveConnections() {
        return Collections.unmodifiableCollection(new ArrayList<>(connectedDevices.keySet()));
    }

    @Override
    public void detach() {
        aggregator.removeListener(clientListener);

        for (Device device : connectedDevices.keySet())
            turnConnectionOff(device);

        connectedDevices.clear();
    }
}
