/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.direct;

import android.net.wifi.p2p.WifiP2pGroup;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.ServerInfo;
import org.hyrax.link.misc.Constants;

/**
 * This class represents the wifi p2p server info.
 */
public class WifiDirectLinkGroupInfo implements ServerInfo {
    @NonNull
    private final String serverID;
    @NonNull
    private final WifiP2pGroup p2pGroup;

    /**
     * Constructor if {@link WifiDirectLinkGroupInfo} class.
     *
     * @param p2pGroup wifi p2p group object representation
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pGroup.html" target="_blank">WifiP2pGroup</a>
     */
    WifiDirectLinkGroupInfo(@NonNull WifiP2pGroup p2pGroup) {
        this.serverID = Constants.DEFAULT_SERVER_UUID;
        this.p2pGroup = p2pGroup;
    }

    /**
     * Returns the wifi p2p group object.
     *
     * @return wifi p2p group object
     */
    @NonNull
    public WifiP2pGroup getP2pGroup() {
        return p2pGroup;
    }

    @NonNull
    @Override
    public String getIdentifier() {
        return serverID;
    }

    @Override
    public String toString() {
        return String.format("Wifi P2p Server Info:\nSSID: %s |||| Password %s",
                p2pGroup.getNetworkName(), p2pGroup.getPassphrase());
    }
}
