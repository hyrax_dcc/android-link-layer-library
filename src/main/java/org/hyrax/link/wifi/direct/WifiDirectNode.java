/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.direct;

import android.net.wifi.p2p.WifiP2pDevice;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.Device;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.ScanData;
import org.hyrax.link.wifi.types.ScanDataWifi;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Wifi p2p device representation.
 */
public class WifiDirectNode implements Device {
    @NonNull
    private final WifiP2pDevice p2pDevice;
    @NonNull
    private final Set<ScanData> scanData;

    /**
     * Constructor of {@link WifiDirectNode} class.
     *
     * @param p2pDevice wifi p2p remote object
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pDevice.html" target="_blank">WifiP2pDevice</a>
     */
    public WifiDirectNode(@NonNull WifiP2pDevice p2pDevice) {
        this.p2pDevice = p2pDevice;
        this.scanData = new HashSet<>();
    }

    /**
     * Constructor.
     *
     * @param p2pDevice    wifi p2p remote object
     * @param scanDataWifi wifi p2p scanned data
     */
    public WifiDirectNode(@NonNull WifiP2pDevice p2pDevice, final @NonNull ScanDataWifi scanDataWifi) {
        this.p2pDevice = p2pDevice;
        this.scanData = new HashSet<ScanData>() {{
            add(scanDataWifi);
        }};
    }

    @NonNull
    @Override
    public String getName() {
        return p2pDevice.deviceName;
    }

    @NonNull
    @Override
    public String getUniqueAddress() {
        return p2pDevice.deviceAddress;
    }

    @Override
    public int getRssi() {
        return Constants.DEFAULT_RSSI;
    }

    @NonNull
    @Override
    public WifiP2pDevice getOriginalObject() {
        return p2pDevice;
    }

    @NonNull
    @Override
    public Collection<ScanData> getScanData() {
        return scanData;
    }

    @Override
    public void mergeScanData(@NonNull Collection<ScanData> scanData) {
        this.scanData.addAll(scanData);
    }

    @NonNull
    @Override
    public Technology getTechnology() {
        return Technology.WIFI_DIRECT;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof WifiDirectNode &&
                ((WifiDirectNode) o).getUniqueAddress().equals(getUniqueAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUniqueAddress());
    }

    @Override
    public String toString() {
        return String.format("%s: %s (%s): Data %s",
                getTechnology().name(), getName(), getUniqueAddress(), scanData);
    }
}
