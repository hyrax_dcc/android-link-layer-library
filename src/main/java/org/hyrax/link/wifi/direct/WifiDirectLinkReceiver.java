/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.direct;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.annotation.NonNull;
import android.util.Log;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.Device;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.ConnectionLostEvent;
import org.hyrax.link.misc.event.ConnectionNewEvent;
import org.hyrax.link.misc.event.ConnectionServerInfoEvent;
import org.hyrax.link.misc.event.ConnectionServerNewClient;
import org.hyrax.link.misc.event.ConnectionServerOffEvent;
import org.hyrax.link.misc.event.ConnectionServerOnEvent;
import org.hyrax.link.misc.event.DiscoveryFoundEvent;
import org.hyrax.link.misc.event.DiscoveryOffEvent;
import org.hyrax.link.misc.event.DiscoveryOnEvent;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.event.HardwareOffEvent;
import org.hyrax.link.misc.event.HardwareOnEvent;
import org.hyrax.link.misc.event.VisibilityOffEvent;
import org.hyrax.link.misc.event.VisibilityOnEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class {@link WifiDirectLinkReceiver} receives all the Wifi p2p events.
 */

public class WifiDirectLinkReceiver extends Receiver {
    private Collection<WifiP2pDevice> prevClients;
    private boolean groupCreated;
    private boolean enabled;
    private boolean isActive;

    /**
     * Creates a {@link WifiDirectLinkReceiver} object.
     * <br/>
     * It receives an object {@link Aggregator}, in order to access the registered listeners.
     *
     * @param aggregator listeners aggregator
     */
    public WifiDirectLinkReceiver(@NonNull Aggregator aggregator) {
        super(aggregator);
        prevClients = new ArrayList<>();
        groupCreated = false;
        enabled = false;
        isActive = false;
    }

    @NonNull
    @Override
    protected List<Event> actionToTechEvent(@NonNull String action, @NonNull Intent intent) {
        List<Event> events = new ArrayList<>();

        switch (action) {
            case WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION:
                if (enabled && !isActive) {
                    events.add(new HardwareOnEvent());
                    isActive = true;
                }
                break;
            case WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION:
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, WifiP2pManager.ERROR);

                switch (state) {
                    case WifiP2pManager.WIFI_P2P_STATE_ENABLED:
                        enabled = true;
                        break;

                    case WifiP2pManager.WIFI_P2P_STATE_DISABLED:
                        isActive = false;
                        enabled = false;
                        events.add(new HardwareOffEvent());
                        break;
                }
                break;

            case WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION:
                state = intent.getIntExtra(WifiP2pManager.EXTRA_DISCOVERY_STATE, WifiP2pManager.ERROR);
                switch (state) {
                    case WifiP2pManager.WIFI_P2P_DISCOVERY_STARTED:
                        events.add(new DiscoveryOnEvent(Constants.DEFAULT_SCANNER_UUID));
                        events.add(new VisibilityOnEvent(Constants.DEFAULT_ADVERTISER_UUID));
                        break;

                    case WifiP2pManager.WIFI_P2P_DISCOVERY_STOPPED:
                        events.add(new DiscoveryOffEvent(Constants.DEFAULT_SCANNER_UUID));
                        events.add(new VisibilityOffEvent(Constants.DEFAULT_ADVERTISER_UUID));
                        break;
                }
                break;

            case WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION:
                Set<Device> nodes = new HashSet<>();
                WifiP2pDeviceList devices = intent.getParcelableExtra(WifiP2pManager.EXTRA_P2P_DEVICE_LIST);
                if (devices != null) {
                    for (WifiP2pDevice device : devices.getDeviceList())
                        nodes.add(new WifiDirectNode(device));

                    events.add(new DiscoveryFoundEvent(Constants.DEFAULT_SCANNER_UUID, nodes));
                }
                break;

            case WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION:
                final WifiP2pInfo p2pInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO);
                final NetworkInfo netInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                final WifiP2pGroup p2pGroup = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_GROUP);

                Log.d("WifiDirect", String.format("p2pGroup: %s || p2pInfo: %s || netInfo: %s",
                        (p2pGroup == null) ? "NULL" : "Valid",
                        (p2pInfo == null) ? "NULL" : "Valid",
                        (netInfo == null) ? "NULL" : "Valid"));
                if (p2pGroup != null && p2pInfo != null && netInfo != null) {
                    Collection<WifiP2pDevice> currentClients = p2pGroup.getClientList();


                    if (netInfo.isConnected() && !p2pGroup.isGroupOwner()) { //slave
                        currentClients = new ArrayList<WifiP2pDevice>() {{
                            add(p2pGroup.getOwner());
                        }};
                    }

                    boolean groupFormed = netInfo.isConnected() && p2pInfo.groupFormed
                            && p2pGroup.isGroupOwner();

                    Log.d("WifiDirect", String.format("Connected: %s || Available: %s || Formed: %s || Owner: %s || Created: %s",
                            netInfo.isConnected(), netInfo.isAvailable(), p2pInfo.groupFormed, p2pInfo.isGroupOwner, groupCreated));
                    Log.d("WifiDirect", "Go Address " + p2pInfo.groupOwnerAddress);
                    Log.d("WifiDirect", "Go Object " + p2pGroup.getOwner());

                    if (groupFormed && !groupCreated) {
                        //notifies that a group has been created and the group information
                        events.add(new ConnectionServerInfoEvent(
                                new WifiDirectLinkGroupInfo(p2pGroup)));
                        events.add(new ConnectionServerOnEvent(Constants.DEFAULT_SERVER_UUID));
                        groupCreated = true;
                        Log.w("WifiDirect", "Group Created");

                    } else if (p2pGroup.getOwner() == null && !groupFormed && groupCreated) {
                        events.add(new ConnectionServerOffEvent(Constants.DEFAULT_SERVER_UUID));
                        groupCreated = false;
                        Log.e("WifiDirect", "Group Destroyed");
                    }

                    Log.d("WifiDirect", "Current clients " + currentClients);
                    Log.d("WifiDirect", "Prev clients " + prevClients);
                    processClients(currentClients, events);
                    prevClients = currentClients;
                }
                break;
        }
        return events;
    }

    /**
     * Method that identifies if clients have arrived or leave.
     *
     * @param currentClients current clients list
     * @param events         list of object events
     */
    private void processClients(@NonNull Collection<WifiP2pDevice> currentClients, @NonNull List<Event> events) {
        if (currentClients.size() > prevClients.size()) { //new_def clients arrived to the group
            for (WifiP2pDevice dev : currentClients) {
                if (!prevClients.contains(dev)) {
                    WifiDirectNode node = new WifiDirectNode(dev);
                    events.add(new ConnectionNewEvent(node));
                    if (groupCreated)
                        events.add(new ConnectionServerNewClient(node));
                }
            }

        } else if (currentClients.size() < prevClients.size()) { //clients have left the group
            for (WifiP2pDevice dev : prevClients) {
                if (!currentClients.contains(dev))
                    events.add(new ConnectionLostEvent(new WifiDirectNode(dev)));
            }
        }
    }

    @NonNull
    @Override
    public IntentFilter getIntentFilters() {
        return new IntentFilter() {{
            addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
            addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);
            addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
            addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
            addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        }};
    }
}
