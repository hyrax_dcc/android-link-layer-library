/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.direct.translator;

import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.support.annotation.NonNull;
import android.util.Log;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.event.DiscoveryFoundEvent;
import org.hyrax.link.misc.event.DiscoveryOffEvent;
import org.hyrax.link.misc.event.DiscoveryOnEvent;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Bonjour;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.DiscoveryLogicTranslator;
import org.hyrax.link.wifi.types.ScanDataWifi;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.types.WifiScanFilter;
import org.hyrax.link.wifi.types.WifiScanRecord;
import org.hyrax.link.wifi.direct.WifiDirectNode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link WifiP2pDiscoveryTranslator} manages the wifi p2p discovery state.
 * START_DISCOVERY/STOP_DISCOVERY
 */
public class WifiP2pDiscoveryTranslator implements DiscoveryLogicTranslator<Void, WifiScanFilter> {
    private static final String TAG = "WifiP2pDiscovery";
    private final static WifiP2pDnsSdServiceRequest DEFAULT_SERVICE_REQUEST =
            WifiP2pDnsSdServiceRequest.newInstance(UUID.randomUUID().toString(),
                    Bonjour.NONE.toString());
    @NonNull
    private final WifiP2pManager wifiP2pManager;
    @NonNull
    private final WifiP2pManager.Channel wifiP2pChannel;
    @NonNull
    private final Aggregator aggregator;
    @NonNull
    private final ConcurrentHashMap<String, WifiP2pDnsSdServiceRequest> scanners;
    /**
     * Listens for discovery status events.
     */
    private final LinkListener<String> discoveryListener = new LinkListener<String>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<String> outcome) {
            if (outcome.isSuccessful()) {
                switch (eventType) {
                    case LINK_DISCOVERY_ON:
                        if (outcome.isSuccessful()) {
                            if (Constants.DEFAULT_SCANNER_UUID.equals(outcome.getOutcome())) {
                                scanners.putIfAbsent(outcome.getOutcome(), DEFAULT_SERVICE_REQUEST);
                            }
                        }
                        break;

                    case LINK_DISCOVERY_OFF:
                        scanners.remove(outcome.getOutcome());
                        break;
                }
            }
        }
    };

    /**
     * Constructor of {@link WifiP2pDiscoveryTranslator} class.
     *
     * @param wifiP2pManager wifi p2p object manager
     * @param wifiP2pChannel wifi p2p channel
     * @param aggregator     listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pManager.html" target="_blank">WifiP2pManager</a>
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pManager.Channel.html" target="_blank">WifiP2pManager.Channel</a>
     */
    public WifiP2pDiscoveryTranslator(@NonNull WifiP2pManager wifiP2pManager,
                                      @NonNull WifiP2pManager.Channel wifiP2pChannel,
                                      @NonNull Aggregator aggregator) {
        this.wifiP2pManager = wifiP2pManager;
        this.wifiP2pChannel = wifiP2pChannel;
        this.aggregator = aggregator;

        scanners = new ConcurrentHashMap<>();
        aggregator.listen(discoveryListener, EventCategory.LINK_DISCOVERY);
    }

    @Override
    public void turnDiscoveryOn(@NonNull final DiscoveryProperties<Void, WifiScanFilter> discoveryProperties) {
        final String scanId = discoveryProperties.getIdentifier();
        LinkException exception = null;

        if (Constants.DEFAULT_SCANNER_UUID.equals(scanId)) {
            Object prop = scanners.putIfAbsent(scanId, DEFAULT_SERVICE_REQUEST);
            if (null == prop) {
                wifiP2pManager.discoverPeers(wifiP2pChannel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onFailure(int reason) {
                        scanners.remove(scanId);
                        Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(scanId,
                                LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                                        "Start default discovery has failed")
                        ));
                    }
                });
            } else {
                exception = LinkException.build(Reason.ALREADY_STARTED, "Discovery already started: " + scanId);
            }
        } else {
            //service discovery
            startServiceDiscover(discoveryProperties);
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(scanId, exception));
    }

    /**
     * Starts the service discovery
     *
     * @param properties discovery properties object
     */
    private void startServiceDiscover(@NonNull final DiscoveryProperties<Void, WifiScanFilter> properties) {
        final WifiP2pDnsSdServiceRequest request = WifiP2pDnsSdServiceRequest.newInstance();
        final String identifier = properties.getIdentifier();
        final ConcurrentHashMap<String, Map<String, String>> records = new ConcurrentHashMap<>();

        Object prop = scanners.put(identifier, request);
        if (null == prop) {
            WifiP2pManager.DnsSdTxtRecordListener txtListener = (fullDomainName, txtRecordMap, wifiP2pDevice) -> {
                if (null != fullDomainName && null != txtRecordMap && null != wifiP2pDevice) {
                    Log.i(TAG, String.format("FullName: %s || Data %s", fullDomainName, txtRecordMap));
                    records.put(wifiP2pDevice.deviceAddress, txtRecordMap);
                }
            };

            WifiP2pManager.DnsSdServiceResponseListener serviceListener = (serviceName, registrationType, wifiP2pDevice) -> {
                if (null != serviceName && null != registrationType && null != wifiP2pDevice) {
                    Map<String, String> data = records.get(wifiP2pDevice.deviceAddress);
                    if (null != data) {
                        Log.i(TAG, String.format("Service Available %s -> %s", serviceName, registrationType));

                        WifiScanRecord scanRecord = new WifiScanRecord(serviceName, registrationType, data);
                        //if the record meets the requirements
                        if (scanRecord.validOnFilter(properties.getScannerFilter())) {
                            WifiDirectNode p2pNode = new WifiDirectNode(wifiP2pDevice, new ScanDataWifi(scanRecord));
                            Receiver.notifyEvent(aggregator, new DiscoveryFoundEvent(identifier, p2pNode));
                        }

                    } else {
                        Log.e(TAG, "Discovery service empty data " + wifiP2pDevice.deviceAddress);
                    }
                }
            };

            wifiP2pManager.setDnsSdResponseListeners(wifiP2pChannel, serviceListener, txtListener);
            wifiP2pManager.addServiceRequest(wifiP2pChannel, request, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    wifiP2pManager.discoverServices(wifiP2pChannel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(properties.getIdentifier()));
                        }

                        @Override
                        public void onFailure(int reason) {
                            scanners.remove(identifier);
                            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(properties.getIdentifier(),
                                    LinkException.build(WifiDirectLink.mapReasonToReason(reason), "Error starting discover services")
                            ));
                        }
                    });
                }

                @Override
                public void onFailure(int reason) {
                    scanners.remove(identifier);
                    Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(properties.getIdentifier(),
                            LinkException.build(WifiDirectLink.mapReasonToReason(reason), "Error registering service")));
                }
            });

        } else {
            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(properties.getIdentifier(),
                    LinkException.build(Reason.ALREADY_STARTED, "Discovery already started " + identifier)));
        }
    }

    @Override
    public void turnDiscoveryOff(@NonNull final String scanId) {
        WifiP2pDnsSdServiceRequest serviceRequest = scanners.remove(scanId);
        if (null == serviceRequest) {
            Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(scanId,
                    LinkException.build(Reason.NULL_POINTER, "No active discovery " + scanId)));

        } else if (serviceRequest.equals(DEFAULT_SERVICE_REQUEST)) {
            wifiP2pManager.stopPeerDiscovery(wifiP2pChannel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onFailure(int reason) {
                    Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(
                            scanId, LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                            "Error stopping default discovery")
                    ));
                }
            });
        } else {
            stopServiceDiscover(serviceRequest, scanId);
        }
    }

    /**
     * Stops the WifiP2p service discovery.
     *
     * @param request service request object
     * @param scanId  scanner identifier
     */
    private void stopServiceDiscover(@NonNull WifiP2pDnsSdServiceRequest request, @NonNull final String scanId) {
        wifiP2pManager.removeServiceRequest(wifiP2pChannel, request, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(scanId));
            }

            @Override
            public void onFailure(int reason) {
                Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(
                        scanId, LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                        "Error stopping service discovery")
                ));
            }
        });
    }

    @Override
    public boolean isDiscoveryOn(@NonNull String scanId) {
        return scanners.containsKey(scanId);
    }

    @NonNull
    @Override
    public Collection<String> getScanners() {
        return Collections.unmodifiableList(new ArrayList<>(scanners.keySet()));
    }

    @Override
    public void detach() {
        aggregator.removeListener(discoveryListener);

        for (String scanId : scanners.keySet())
            turnDiscoveryOff(scanId);

        wifiP2pManager.clearServiceRequests(wifiP2pChannel, null);
        scanners.clear();
    }
}
