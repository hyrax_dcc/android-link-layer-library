/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.direct;

import android.net.wifi.p2p.WifiP2pGroup;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Constants;

import java.util.Locale;


/**
 * This class represents the WifiP2p group owner authentication credentials.
 */
public class WifiDirectCredentials {
    @Nullable
    private final WifiP2pGroup wifiP2pGroup;
    @Nullable
    private final WifiDirectProxy wifiDirectProxy;

    /**
     * Constructor.
     *
     * @param wifiP2pGroup wifi p2p group object
     * @param wifiDirectProxy wifi proxy object
     */
    public WifiDirectCredentials(@Nullable WifiP2pGroup wifiP2pGroup, @Nullable WifiDirectProxy wifiDirectProxy) {
        this.wifiP2pGroup = wifiP2pGroup;
        this.wifiDirectProxy = wifiDirectProxy;
    }

    /**
     * Returns a string representation of mac address.
     * <br/>
     * If group is not valid returns {@value Constants#UNDEFINED}.
     *
     * @return string mac address
     */
    @NonNull
    public String getMacAddress() {
        return (null != wifiP2pGroup && wifiP2pGroup.isGroupOwner()) ?
                wifiP2pGroup.getOwner().deviceAddress : Constants.UNDEFINED;
    }

    /**
     * Returns the group owner name.
     * <br/>
     * If group is not valid returns {@value Constants#UNDEFINED}.
     *
     * @return string with device name
     */
    @NonNull
    public String getDeviceName() {
        return (null != wifiP2pGroup && wifiP2pGroup.isGroupOwner()) ?
                wifiP2pGroup.getOwner().deviceName : Constants.UNDEFINED;
    }

    /**
     * Returns the network ssid.
     * <br/>
     * If group is not valid returns {@value Constants#UNDEFINED}.
     *
     * @return string with network ssid
     */
    @NonNull
    public String getSSID() {
        return (null != wifiP2pGroup && wifiP2pGroup.isGroupOwner()) ?
                wifiP2pGroup.getNetworkName() : Constants.UNDEFINED;
    }

    /**
     * Returns the password of the current group.
     * <br/>
     * If group is not valid returns {@value Constants#UNDEFINED}.
     *
     * @return string password
     */
    @NonNull
    public String getPassword() {
        return (null != wifiP2pGroup && wifiP2pGroup.isGroupOwner()) ?
                wifiP2pGroup.getPassphrase() : Constants.UNDEFINED;

    }

    /**
     * Returns the proxy listen hots address (Usually an ip address).
     * <br/>
     * If proxy is not valid returns {@value Constants#UNDEFINED}.
     *
     * @return a string host address
     */
    @NonNull
    public String getProxyHost() {
        return (null != wifiP2pGroup && wifiP2pGroup.isGroupOwner() && wifiDirectProxy != null) ?
                wifiDirectProxy.getHost() : Constants.UNDEFINED;
    }

    /**
     * Returns the proxy listen port or 0 if no proxy available.
     *
     * @return a Integer with listen port
     */
    @IntRange(from = 0, to = 65535)
    public int getProxyPort() {
        return (null != wifiP2pGroup && wifiP2pGroup.isGroupOwner() && wifiDirectProxy != null) ?
                wifiDirectProxy.getPort() : 0;
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "Group: %s, \nProxy: %s",
                (null != wifiP2pGroup) ? wifiP2pGroup : "NULL", (null != wifiDirectProxy) ?
                        wifiDirectProxy : "NULL");
    }
}
