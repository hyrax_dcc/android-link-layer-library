/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.direct.translator;

import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.support.annotation.NonNull;
import android.util.Log;

import org.hyrax.link.misc.Executable;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.event.VisibilityOffEvent;
import org.hyrax.link.misc.event.VisibilityOnEvent;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Bonjour;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.VisibilityLogicTranslator;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiAdvertiseSettings;
import org.hyrax.link.wifi.WifiDirectLink;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The wifi p2p does not support the visibility feature with discovery enabled. Thus
 * when a device is discovering means that the device is also visible.
 * <br/>
 * For the visibility feature works use discovery instead.
 */
public class WifiP2pVisibilityTranslator implements VisibilityLogicTranslator<WifiAdvertiseSettings, WifiAdvertiseData> {
    private static final String TAG = "WifiP2pVisibility";
    @NonNull
    private final WifiP2pManager p2pManager;
    @NonNull
    private final WifiP2pManager.Channel p2pChannel;
    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<String, AdvertiserHelper> advertisers;

    /**
     * Listens for visibility events.
     */
    private final LinkListener<String> visibilityListener = new LinkListener<String>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<String> outcome) {
            if (Constants.DEFAULT_ADVERTISER_UUID.equals(outcome.getOutcome())) {
                switch (eventType) {
                    case LINK_VISIBILITY_ON:
                        outcome.ifSuccess(argument ->
                                advertisers.putIfAbsent(outcome.getOutcome(), new AdvertiserHelper()));
                        break;
                    case LINK_VISIBILITY_OFF:
                        advertisers.remove(outcome.getOutcome());
                        break;
                }
            }
        }
    };

    /**
     * Constructor of {@link WifiP2pVisibilityTranslator} class.
     *
     * @param p2pManager wifi p2p object manager
     * @param p2pChannel wifi p2p channel
     * @param aggregator listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pManager.html" target="_blank">WifiP2pManager</a>
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pManager.Channel.html" target="_blank">WifiP2pManager.Channel</a>
     */
    public WifiP2pVisibilityTranslator(@NonNull WifiP2pManager p2pManager,
                                       @NonNull WifiP2pManager.Channel p2pChannel,
                                       @NonNull Aggregator aggregator) {
        this.p2pManager = p2pManager;
        this.p2pChannel = p2pChannel;
        this.aggregator = aggregator;

        advertisers = new ConcurrentHashMap<>();
        aggregator.listen(visibilityListener, EventCategory.LINK_VISIBILITY, true);
    }

    @Override
    public void turnVisibilityOn(@NonNull final VisibilityProperties<WifiAdvertiseSettings, WifiAdvertiseData> visibilityProperties) {
        final String advertiseId = visibilityProperties.getIdentifier();
        LinkException exception = null;

        if (Constants.DEFAULT_ADVERTISER_UUID.equals(advertiseId)) {
            Object prop = advertisers.putIfAbsent(advertiseId, new AdvertiserHelper());
            if (null == prop) {
                p2pManager.discoverPeers(p2pChannel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onFailure(int reason) {
                        advertisers.remove(advertiseId);
                        Receiver.notifyEvent(aggregator, new VisibilityOnEvent(advertiseId,
                                LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                                        "Error starting visibility")
                        ));
                    }
                });
            } else {
                exception = LinkException.build(Reason.ALREADY_STARTED, "Already active " + advertiseId);
            }
        } else {
            if (visibilityProperties.getAdvertiserSettings() == null)
                exception = LinkException.build(Reason.NULL_POINTER, "Please define advertiser settings");
            else {
                startService(visibilityProperties);
            }
        }
        if (null != exception)
            Receiver.notifyEvent(aggregator, new VisibilityOnEvent(advertiseId, exception));
    }

    /**
     * Adds a WifiP2p local service
     *
     * @param properties visibility properties object
     */
    private void startService(@NonNull final VisibilityProperties<WifiAdvertiseSettings, WifiAdvertiseData> properties) {
        //advertise data using Wifi Direct
        final Executable<VisibilityProperties<WifiAdvertiseSettings, WifiAdvertiseData>> advertise = argument -> {
            WifiAdvertiseSettings settings = argument.getAdvertiserSettings();
            WifiAdvertiseData data = (argument.getAdvertiserData() == null) ? new WifiAdvertiseData() : argument.getAdvertiserData();

            Objects.requireNonNull(settings);
            data.addData(Constants.HOST, settings.getHost());
            data.addData(Constants.PORT, Integer.toString(settings.getPort()));

            WifiP2pDnsSdServiceInfo serviceInfo = WifiP2pDnsSdServiceInfo.newInstance(
                    settings.getServiceName(), settings.getProtocol().getProtocolName(), data.getData());

            Object prop = advertisers.putIfAbsent(properties.getIdentifier(), new AdvertiserHelper(serviceInfo, settings.isToForceDiscover()));
            if (null == prop) {
                p2pManager.addLocalService(p2pChannel, serviceInfo, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        Receiver.notifyEvent(aggregator, new VisibilityOnEvent(properties.getIdentifier()));
                    }

                    @Override
                    public void onFailure(int reason) {
                        advertisers.remove(properties.getIdentifier());
                        Receiver.notifyEvent(aggregator, new VisibilityOnEvent(properties.getIdentifier(),
                                LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                                        "Error adding local service")
                        ));
                    }
                });

            } else {
                Receiver.notifyEvent(aggregator, new VisibilityOnEvent(properties.getIdentifier(),
                        LinkException.build(Reason.ALREADY_STARTED,
                                "Visibility service already started: " + properties.getIdentifier())
                ));
            }

        };

        Objects.requireNonNull(properties.getAdvertiserSettings());
        if (properties.getAdvertiserSettings().isToForceDiscover()) {
            p2pManager.discoverPeers(p2pChannel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.i(TAG, "Service discovery force success");
                    advertise.execute(properties);
                    //advertiseData(properties);
                }

                @Override
                public void onFailure(int reason) {
                    Log.e(TAG, "Service discovery force Error " + WifiDirectLink.mapReasonToReason(reason));
                    advertise.execute(properties);

                }
            });
        } else
            advertise.execute(properties);
    }

    @Override
    public void turnVisibilityOff(@NonNull final String advertiseId) {
        //the boolean means to stop the discovery
        final AdvertiserHelper helper = advertisers.remove(advertiseId);
        if (helper != null) {
            if (helper.forceDiscover) {
                p2pManager.stopPeerDiscovery(p2pChannel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        stopService(helper.dnsSdServiceInfo, advertiseId);
                    }

                    @Override
                    public void onFailure(int reason) {
                        Receiver.notifyEvent(aggregator, new VisibilityOffEvent(advertiseId,
                                LinkException.build(WifiDirectLink.mapReasonToReason(reason), "Error stopping visibility")));
                    }
                });
            } else {
                stopService(helper.dnsSdServiceInfo, advertiseId);
            }
        } else {
            Receiver.notifyEvent(aggregator, new VisibilityOffEvent(advertiseId,
                    LinkException.build(Reason.NULL_POINTER, "No active advertiser " + advertiseId)));
        }
    }

    /**
     * Stops a registered service.
     *
     * @param serviceInfo service info object
     * @param advertiseId advertiser identifier
     */
    private void stopService(@NonNull final WifiP2pDnsSdServiceInfo serviceInfo, @NonNull final String advertiseId) {
        p2pManager.removeLocalService(p2pChannel, serviceInfo, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Receiver.notifyEvent(aggregator, new VisibilityOffEvent(advertiseId));
            }

            @Override
            public void onFailure(int reason) {
                Receiver.notifyEvent(aggregator, new VisibilityOffEvent(advertiseId,
                        LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                                "Error removing advertiser")
                ));
            }
        });
    }

    @Override
    public boolean isVisibilityOn(@NonNull String advertiseId) {
        return advertisers.containsKey(advertiseId);
    }

    @NonNull
    @Override
    public Collection<String> getAdvertisers() {
        return Collections.unmodifiableCollection(new ArrayList<>(advertisers.keySet()));
    }

    @Override
    public void detach() {
        aggregator.removeListener(visibilityListener);

        for (String advertiseId : advertisers.keySet()) {
            turnVisibilityOff(advertiseId);
        }

        p2pManager.clearLocalServices(p2pChannel, null);
        advertisers.clear();
    }

    /**
     * Advertiser helper class
     */
    private class AdvertiserHelper {
        @NonNull
        private final WifiP2pDnsSdServiceInfo DEFAULT = WifiP2pDnsSdServiceInfo.newInstance(
                Constants.UNDEFINED, Bonjour.NONE.getProtocolName(), new HashMap<>());

        @NonNull
        private final WifiP2pDnsSdServiceInfo dnsSdServiceInfo;
        private final boolean forceDiscover;

        /**
         * Default constructor.
         */
        AdvertiserHelper() {
            this.dnsSdServiceInfo = DEFAULT;
            this.forceDiscover = true;
        }

        /**
         * Constructor.
         *
         * @param dnsSdServiceInfo wifi direct service object
         * @param forceDiscover    <tt>true</tt> if to force the discovery, <tt>false</tt> otherwise
         */
        AdvertiserHelper(@NonNull WifiP2pDnsSdServiceInfo dnsSdServiceInfo, boolean forceDiscover) {
            this.dnsSdServiceInfo = dnsSdServiceInfo;
            this.forceDiscover = forceDiscover;
        }
    }
}
