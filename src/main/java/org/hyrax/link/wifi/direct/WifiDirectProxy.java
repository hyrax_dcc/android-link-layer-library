/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.direct;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;
import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.HttpProxyServerBootstrap;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;

import java.net.InetSocketAddress;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class that manages a http proxy running on the Wifi p2p group owner.
 */
public class WifiDirectProxy {
    private final HttpProxyServerBootstrap serverBootstrap;
    private final String proxyHost;
    private final AtomicBoolean running;
    private HttpProxyServer proxyServer;

    /**
     * Constructor.
     */
    public WifiDirectProxy() {
        this.serverBootstrap = DefaultHttpProxyServer.bootstrap().withAddress(
                new InetSocketAddress("0.0.0.0", 0));
        this.proxyHost = Constants.DEFAULT_GO_IP;
        this.running = new AtomicBoolean(false);
    }

    /**
     * Start the http proxy.
     */
    public void start() {
        if (running.compareAndSet(false, true)) {
            proxyServer = serverBootstrap.start();
        }
    }

    /**
     * Stops the http proxy.
     */
    public void stop() {
        if (running.compareAndSet(true, false) && null != proxyServer) {
            proxyServer.stop();
        }
    }

    /**
     * Returns the proxy listening port.
     *
     * @return integer port number
     */
    public int getPort() {
        return (null != proxyServer) ? proxyServer.getListenAddress().getPort() : 0;
    }

    /**
     * Returns the host address (Ip address).
     * <br/>
     * Means the current device has a http proxy
     *
     * @return host address
     */
    @NonNull
    public String getHost() {
        return proxyHost;
    }

    @NonNull
    @Override
    public String toString() {
        return "Listen on port " + getPort();
    }
}
