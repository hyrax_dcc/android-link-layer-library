/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.wifi.direct.translator;

import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.event.ConnectionServerOffEvent;
import org.hyrax.link.misc.event.ConnectionServerOnEvent;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.ServerInfo;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.ConnectionServerLogicTranslator;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.direct.WifiDirectCredentials;
import org.hyrax.link.wifi.direct.WifiDirectLinkGroupInfo;
import org.hyrax.link.wifi.direct.WifiDirectProxy;
import org.hyrax.link.wifi.types.WifiServerSettings;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Class {@link WifiP2pConnectionServerTranslator} manages the acceptance of connections from
 * the wifi p2p clients. ACCEPT/REJECT
 */
public class WifiP2pConnectionServerTranslator implements ConnectionServerLogicTranslator<WifiServerSettings> {
    @NonNull
    private final WifiP2pManager wifiP2pManager;
    @NonNull
    private final WifiP2pManager.Channel wifiP2pChannel;
    @NonNull
    private final Aggregator aggregator;

    /**
     * The Wifi direct only allows one server.
     */
    @Nullable
    private ServerHelper serverHelper;

    /**
     * Listen for connection server status event
     */
    private final LinkListener serverListener = new LinkListener<Object>() {

        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<Object> outcome) {
            switch (eventType) {
                case LINK_CONNECTION_SERVER_ON:
                    outcome.ifError(argument -> {
                        if (serverHelper != null) {
                            serverHelper.stop();
                            serverHelper = null;
                        }
                    });
                    break;

                case LINK_CONNECTION_SERVER_OFF:
                    if (serverHelper != null) {
                        serverHelper.stop();
                        serverHelper = null;
                    }
                    break;

                case LINK_CONNECTION_SERVER_INFO:
                    WifiDirectLinkGroupInfo info = (WifiDirectLinkGroupInfo) outcome.getOutcome();
                    if (serverHelper != null) {
                        serverHelper.info = info;
                        String wpsPin = (serverHelper.properties.getSettings() != null)
                                ? serverHelper.properties.getSettings().getWdPin()
                                : Constants.UNDEFINED;

                        if (!Constants.UNDEFINED.equals(wpsPin)) {
                            setWps(wpsPin, new WifiP2pManager.ActionListener() {
                                @Override
                                public void onSuccess() {
                                    Log.w("WifiP2p", "Setting Wps Success");
                                }

                                @Override
                                public void onFailure(int reason) {
                                    Log.w("WifiP2p", "Setting Wps Failure !!");
                                }
                            });
                        }
                    }
                    break;
            }
        }
    };

    /**
     * Constructor of {@link WifiP2pConnectionServerTranslator} class.
     *
     * @param wifiP2pManager wifi p2p object manager
     * @param wifiP2pChannel wifi p2p channel
     * @param aggregator     listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pManager.html" target="_blank">WifiP2pManager</a>
     * @see <a href="https://developer.android.com/reference/android/net/wifi/p2p/WifiP2pManager.Channel.html" target="_blank">WifiP2pManager.Channel</a>
     */
    public WifiP2pConnectionServerTranslator(@NonNull WifiP2pManager wifiP2pManager,
                                             @NonNull WifiP2pManager.Channel wifiP2pChannel,
                                             @NonNull Aggregator aggregator) {
        this.wifiP2pManager = wifiP2pManager;
        this.wifiP2pChannel = wifiP2pChannel;
        this.aggregator = aggregator;

        aggregator.listen(serverListener, EventCategory.LINK_CONNECTION_SERVER, true);
    }

    @Override
    public void turnServerOn(@NonNull final ServerProperties<WifiServerSettings> properties) {
        // only default server allowed
        if (!Constants.DEFAULT_SERVER_UUID.equals(properties.getIdentifier())) {
            Receiver.notifyEvent(aggregator, new ConnectionServerOnEvent(
                    properties.getIdentifier(),
                    LinkException.build(Reason.UNSUPPORTED,
                            "Wifi Direct only default identifier allowed")
            ));
            return;
        }
        if (serverHelper != null) {
            Receiver.notifyEvent(aggregator, new ConnectionServerOnEvent(
                    properties.getIdentifier(),
                    LinkException.build(Reason.ALREADY_STARTED,
                            "Wifi Direct only supports one active server")
            ));
            return;
        }

        serverHelper = new ServerHelper(properties.getIdentifier(), properties);
        wifiP2pManager.createGroup(wifiP2pChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                if (properties.getSettings() != null && properties.getSettings().enableProxy()) {
                    //enable wifi direct proxy to redirect traffic
                    WifiDirectProxy proxy = new WifiDirectProxy();
                    serverHelper.proxy = proxy;
                    proxy.start();
                }
            }

            @Override
            public void onFailure(int reason) {
                Receiver.notifyEvent(aggregator, new ConnectionServerOnEvent(
                        properties.getIdentifier(),
                        LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                                "Error creating group")
                ));
            }
        });
    }

    @Override
    public void turnServerOff(@NonNull final String serverId) {
        if (serverHelper != null) {
            wifiP2pManager.removeGroup(wifiP2pChannel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    if(serverHelper != null) {
                        serverHelper.stop();
                        serverHelper = null;
                    }
                }

                @Override
                public void onFailure(int reason) {
                    Receiver.notifyEvent(aggregator, new ConnectionServerOffEvent(serverId,
                            LinkException.build(WifiDirectLink.mapReasonToReason(reason),
                                    "Destroy group server failed")
                    ));
                }
            });
            return;
        }
        Receiver.notifyEvent(aggregator, new ConnectionServerOffEvent(serverId,
                LinkException.build(Reason.NULL_POINTER, "No active server with id " + serverId)
        ));
    }

    @Override
    public boolean isServerOn(@NonNull String serverId) {
        return serverHelper != null && serverHelper.id.equals(serverId);
    }

    @NonNull
    @Override
    public Collection<String> getActiveServers() {
        ArrayList<String> list = new ArrayList<>();
        if (serverHelper != null)
            list.add(serverHelper.id);
        return Collections.unmodifiableCollection(list);
    }

    /**
     * Returns Wifi P2p authentication information if the current device is the group owner.
     * <br/>
     * If information is not available or the serverId is not valid
     * it will return an empty proto buf object with default values.
     *
     * @param serverId server identifier.
     * @return proto buf object with credentials information.
     */
    @NonNull
    public WifiDirectCredentials getWifiP2pCredentials(@NonNull String serverId) {
        if (serverHelper == null || serverHelper.info == null || !serverHelper.id.equals(serverId))
            return new WifiDirectCredentials(null, null);

        return new WifiDirectCredentials(
                ((WifiDirectLinkGroupInfo) serverHelper.info).getP2pGroup(),
                serverHelper.proxy
        );
    }

    @Override
    public void detach() {
        aggregator.removeListener(serverListener);

        if (serverHelper != null)
            turnServerOff(serverHelper.id);
    }

    /**
     * Changes the Wifi Direct WPS info.
     * <p/>
     * The idea is to enable the Wifi Direct auto connect.
     *
     * @param wpsPin         a string pin
     * @param actionListener a callback result
     */
    private void setWps(@NonNull String wpsPin, @NonNull WifiP2pManager.ActionListener actionListener) {
        WpsInfo wps = new WpsInfo();
        // WpsInfo.PBC and WPS activated: the connection is immediate but opens a window in the GO device
        // the user action is ignored
        wps.setup = WpsInfo.PBC;
        wps.pin = wpsPin;

        Method[] bltMethods = WifiP2pManager.class.getDeclaredMethods();
        String mth = "public void android.net.wifi.p2p.WifiP2pManager.startWps(" +
                "android.net.wifi.p2p.WifiP2pManager$Channel," +
                "android.net.wifi.WpsInfo," +
                "android.net.wifi.p2p.WifiP2pManager$ActionListener)";
        for (Method mReflected : bltMethods) {
            if (mth.equals(mReflected.toGenericString())) {
                try {
                    mReflected.invoke(wifiP2pManager, wifiP2pChannel, wps, actionListener);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                    actionListener.onFailure(WifiP2pManager.ERROR);
                }
            }
        }
    }

    /**
     * Server helper class
     */
    private class ServerHelper {
        private final String id;
        private final ServerProperties<WifiServerSettings> properties;
        @Nullable
        private ServerInfo info;
        @Nullable
        private WifiDirectProxy proxy;

        /**
         * Constructor.
         */
        private ServerHelper(@NonNull String id,
                             @NonNull ServerProperties<WifiServerSettings> properties) {
            this.id = id;
            this.properties = properties;
            this.info = null;
            this.proxy = null;
        }

        /**
         * Stops wifi direct proxy.
         */
        private void stop() {
            if (proxy != null)
                proxy.stop();
        }
    }
}
