/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.util.Collection;

/**
 * This interface is responsible for controlling the link layer operations, which are
 * the following:
 * <br/>
 * <ul>
 * <li>Enable/Disable Hardware</li>
 * <li>Devices Discovery</li>
 * <li>Devices Visibility</li>
 * <li>Devices Connection</li>
 * <li>Device Connection Acceptance</li>
 * <li>Services publication</li>
 * </ul>
 * <br/>
 * This actions are executed on synchronous manner, meaning that the current <b>Thread</b> will
 * block until the result is available.
 * <br/>
 * All the action return an {@link Outcome} which allows to check if the action was
 * performed with success or not. The developer <b>must</b> check always the validity of result.
 * Furthermore with an {@link Outcome} comes the result of the operations as well a
 * {@link Throwable} object indicating the action error.
 *
 * //@param <F> type of link feature
 */
public interface LinkSync<F extends LinkFeatures> extends Link<F> {

    /**
     * Enables the hardware (Sync).
     * <br/>
     * Return an {@link Outcome} object, representing the result of computation.
     *
     * @return the hardware enable result
     */
    @NonNull
    Outcome<Boolean> enable();

    /**
     * Disables the hardware (Sync).
     * <br/>
     * Return an {@link Outcome} object, representing the result of computation.
     *
     * @return the hardware disable result
     */
    @NonNull
    Outcome<Boolean> disable();

    /**
     * Starts by searching for reachable devices in the neighborhood by giving a specific
     * {@link DiscoveryProperties} (Sync).
     * <br/>
     * The {@link DiscoveryProperties} are a set of properties that allows to tune the discovery
     * progress to the developer needs.
     * <br/>
     * Returns an {@link Outcome} object, representing the result of computation.
     * <br/>
     * Note that the time this will be blocked depends entirely of the {@link DiscoveryProperties}
     *
     * @param properties discovery properties
     * @return an object with the discovered devices
     */
    @NonNull
    Outcome<Collection<Device>> discover(@NonNull DiscoveryProperties properties);

    /**
     * Cancels the discovery process given an scanId (Sync).
     * <br/>
     * Returns an {@link Outcome} object, representing the result of computation.
     *
     * @param scannerId scanner identifier
     * @return an object with the cancel discovery action
     */
    @NonNull
    Outcome<String> cancelDiscover(@NonNull String scannerId);

    /**
     * Turns the device visibility on with defined properties (SYNC).
     * <br/>
     * Devices may support more than one visibility process, thus it generates an advertiseId
     * in order to identify the advertiser.
     *
     * @param properties visibility properties
     * @return {@link Outcome} object result with advertiseId
     */
    @NonNull
    Outcome<String> setVisible(@NonNull VisibilityProperties<?, ?>  properties);

    /**
     * Turns the device visibility off given a advertiseId (SYNC).
     * <br/>
     * Devices may support more than one visibility process, thus if an advertiser is disabled does
     * not means that the device is completely invisible. Then to become completely invisible all
     * the advertises must be removed.
     *
     * @param advertiserId advertiser identifier
     * @return {@link Outcome} object result with advertiseId
     */
    @NonNull
    Outcome<String> cancelVisible(@NonNull String advertiserId);

    /**
     * Connects to a remote device (Sync).
     * <br/>
     * Return an {@link Outcome} object, representing the result of computation.
     *
     * @param device     device to connect
     * @param properties connection propertied
     * @return {@link Outcome} object with action result
     */
    @NonNull
    Outcome<Device> connect(@NonNull Device device, @NonNull ConnectionProperties<?> properties);

    /**
     * Disconnects from a remote device (Sync).
     * <br/>
     * Return an {@link Outcome} object, representing the result of computation.
     *
     * @param device device to disconnect
     * @return {@link Outcome} object with action result
     */
    @NonNull
    Outcome<Device> disconnect(@NonNull Device device);

    /**
     * Starts to accept connections (server/access point) from clients with a specific
     * identifier (Sync).
     * <br/>
     * Return an {@link Outcome} object, representing the result of computation.
     *
     * @param properties server properties
     * @return {@link Outcome} object result
     */
    @NonNull
    Outcome<String> accept(@NonNull ServerProperties properties);

    /**
     * Denies more connections from clients (Sync).
     * <br/>
     * Depending of technology the clients can be kept or not, e.g Bluetooth will keep the
     * connections while the Wi-Fi P2p not.
     * <br/>
     * Return an {@link Outcome} object, representing the result of computation.
     *
     * @param serverId connection server object
     * @return {@link Outcome} object result
     */
    @NonNull
    Outcome<String> deny(@NonNull String serverId);
}
