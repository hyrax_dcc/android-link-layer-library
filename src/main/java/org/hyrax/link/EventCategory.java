/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link;

/**
 * Enumeration of events category, in order to group events.
 */
public enum EventCategory {
    // group of events related with link detach - disable.
    LINK_DETACH,
    // group of events related with device hardware.
    LINK_HARDWARE,
    // group of events related with device discovery.
    LINK_DISCOVERY,
    // group of events related with device visibility.
    LINK_VISIBILITY,
    // group of events related with device connection.
    LINK_CONNECTION,
    // group of events related with device server - connection acceptance.
    LINK_CONNECTION_SERVER
}
