/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.jdeferred2.Promise;

import java.util.Collection;

/**
 * This interface is responsible for controlling the link layer operations, which are
 * the following:
 * <br/>
 * <ul>
 * <li>Enable/Disable Hardware</li>
 * <li>Devices Discovery</li>
 * <li>Devices Visibility</li>
 * <li>Devices Connection</li>
 * <li>Device Connection Acceptance</li>
 * <li>Services publication</li>
 * </ul>
 * <br/>
 * This actions are executed on asynchronous manner, meaning that the developer must pass an
 * {@link LinkListener} as argument in order to receiver the result later.
 * <br/>
 * This actions returns promises that allows to chain multiple asynchronous calls easily.
 * <p>
 * //@param <F> type of link feature
 *
 * @see <a href="http://jdeferred.org/" target="_blank">Promise</a>
 */
public interface LinkPromise<F extends LinkFeatures> extends Link<F> {
    /**
     * Enables the hardware (Async).
     * <br/>
     * The result is received through a promise.
     *
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    Promise<Boolean, LinkException, Void> enable();

    /**
     * Disables the hardware (Async).
     * <br/>
     * The result is received through a promise.
     *
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    Promise<Boolean, LinkException, Void> disable();

    /**
     * Starts by searching for reachable devices in the neighborhood by giving a specific
     * {@link DiscoveryProperties} (Async).
     * <br/>
     * The {@link DiscoveryProperties} are a set of properties that allows to tune the discovery
     * progress to the developer needs.
     * <br/>
     * The result is received through a promise.
     *
     * @param properties discovery properties
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */

    Promise<Collection<Device>, LinkException, Collection<Device>> discover(@NonNull DiscoveryProperties properties);

    /**
     * Cancels the discovery process given an scanId (Async).
     * <br/>
     * The result is received through a promise.
     *
     * @param scannerId scanner identifier
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    Promise<String, LinkException, Void> cancelDiscover(@NonNull String scannerId);

    /**
     * Turns the device visibility on with defined properties (Async). The promise will be just
     * resolved if an error happens or when the visibility timer has finished.
     * <br/>
     * The result is received through a promise.
     * <br/>
     * Devices may support more than one visibility process, thus it generates an advertiseId
     * in order to identify the advertiser.
     * <br/>
     * The promise will be just resolve when the visibility process is done .
     *
     * @param properties visibility properties
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    default Promise<String, LinkException, String> setVisible(@NonNull VisibilityProperties properties) {
        return setVisible(properties, false);
    }

    /**
     * Turns the device visibility on with defined properties (Async).
     * <br/>
     * The result is received through a promise.
     * <br/>
     *
     * @param properties       visibility properties
     * @param resolveWhenStart <tt>true</tt> if to resolve promise when the visibility has started,
     *                         <tt>false</tt> to resolve just when the visibility timer has finished.
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    Promise<String, LinkException, String> setVisible(@NonNull VisibilityProperties properties,
                                                      boolean resolveWhenStart);

    /**
     * Turns the device visibility off given a advertiseId (Async).
     * <br/>
     * Devices may support more than one visibility process, thus if an advertiser is disabled does
     * not means that the device is completely invisible. Then to become completely invisible all
     * the advertises must be removed.
     *
     * @param advertiserId advertiser identifier
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    Promise<String, LinkException, Void> cancelVisible(@NonNull String advertiserId);

    /**
     * Connects to a remote device by specifying a timeout (Async).
     * <br/>
     * The result is received through a promise.
     *
     * @param device     device to connect
     * @param properties connection properties
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    Promise<Device, LinkException, Void> connect(@NonNull Device device, @NonNull ConnectionProperties properties);

    /**
     * Disconnects from a remote device (Async).
     * <br/>
     * The result is received through a promise.
     *
     * @param device device to disconnect
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    Promise<Device, LinkException, Void> disconnect(@NonNull Device device);

    /**
     * Starts to accept connections (server/access point) from clients with a specific
     * identifier (Async).
     * <br/>
     * The result is received through a promise.
     *
     * @param properties server properties
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    Promise<String, LinkException, Void> accept(@NonNull ServerProperties properties);

    /**
     * Denies more connections from clients (Async).
     * <br/>
     * Depending of technology the clients can be kept or not, e.g Bluetooth will keep the
     * connections while the Wi-Fi P2p not.
     * <br/>
     * The result is received through a promise.
     *
     * @param serverId connection server object
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    Promise<String, LinkException, Void> deny(@NonNull String serverId);
}
