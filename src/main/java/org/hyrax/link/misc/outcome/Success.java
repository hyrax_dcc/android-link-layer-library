/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.outcome;

import android.support.annotation.NonNull;

import org.hyrax.link.Outcome;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;

/**
 * Class that implements the success of an action result.
 */
public class Success<D> implements Outcome<D> {
    @NonNull
    private final D data;

    /**
     * Create a Success object, representing the success of a task.
     *
     * @param data Returned result of a task
     * @see Outcome
     */
    public Success(@NonNull D data) {
        this.data = data;
    }

    @Override
    public boolean isSuccessful() {
        return true;
    }

    @NonNull
    @Override
    public D getOutcome() {
        return data;
    }

    @NonNull
    @Override
    public LinkException getError() {
        return LinkException.build(Reason.EMPTY_EXCEPTION);
    }

    @Override
    public String toString() {
        return String.format("Success: %s", data);
    }
}
