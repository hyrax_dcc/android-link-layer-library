/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Observable class that intents to register, unregister and notify listener on a thread safe manner.
 * <br/>
 * Furthermore it provides a mode where the listeners can be notified and removed at same time.
 *
 * @param <V> listener object type
 */
public class EventObservable<V> {
    @NonNull
    private final CopyOnWriteArrayList<V> listeners;

    /**
     * Constructor.
     */
    public EventObservable() {
        this.listeners = new CopyOnWriteArrayList<>();
    }

    /**
     * Appends a new listener to the list.
     *
     * @param listener listener object
     */
    public void addListener(@NonNull V listener) {
        listeners.add(listener);
    }

    /**
     * Adds a new listener to the top of the list.
     *
     * @param listener listener object
     */
    void addListenerOnTop(@NonNull V listener) {
        listeners.add(0, listener);
    }

    /**
     * Removes a listener from the list.
     *
     * @param listener listener object
     */
    public void removeListener(@NonNull V listener) {
        listeners.remove(listener);
    }

    /**
     * Removes all the registered listeners.
     */
    public void removeAll() {
        listeners.clear();
    }

    /**
     * Returns the number of registered listeners.
     *
     * @return size of registered listeners
     */
    public int length() {
        return listeners.size();
    }

    /**
     * Notifies all the registered listeners.
     *
     * @param callback callback class that will translates how the call will be performed
     */
    public void notifyListeners(@NonNull Callback<V> callback) {
        List<V> toRemove = new ArrayList<>();
        for (V listener : listeners)
            if (callback.call(listener))
                toRemove.add(listener);
        listeners.removeAll(toRemove);
    }

    /**
     * Callback interface.
     *
     * @param <V> listener object type
     */
    public interface Callback<V> {
        /**
         * Method triggered when a listener is ready to be called.
         *
         * @param listener listener object
         * @return <tt>true</tt> if to remove the listener, <tt>false</tt> otherwise
         */
        boolean call(V listener);
    }
}
