/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.properties;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.StopRule;

import java.util.Collection;

/**
 * General properties for discovery progress.
 *
 * @param <S> scanner settings object
 * @param <F> scanner filter object
 */
public class DiscoveryProperties<S, F> {
    private final String scanId;
    private final int timeout;
    private final boolean stopAfterTimeoutExpiration;
    @Nullable
    private final S scannerSettings;
    @Nullable
    private final F scannerFilter;
    @NonNull
    private final StopRule<Collection<Device>> stopRule;

    /**
     * Constructs a {@link DiscoveryProperties} given a discovery {@link Builder} object.
     *
     * @param builder builder with properties
     */
    private DiscoveryProperties(@NonNull Builder<S, F> builder) {
        this.scanId = builder.scanId;
        this.timeout = builder.timeout;
        this.stopAfterTimeoutExpiration = builder.stopAfterTimeoutExpiration;
        this.scannerSettings = builder.scannerSettings;
        this.scannerFilter = builder.scannerFilter;
        this.stopRule = builder.stopRule;
    }

    /**
     * Instantiates a builder object in order to defined the intended parameters.
     *
     * @return a {@link Builder} object
     */
    @NonNull
    public static <S, F> Builder<S, F> newBuilder() {
        return new Builder<>();
    }

    /**
     * Returns a scanner identifier.
     *
     * @return scanner id
     */
    @NonNull
    public String getIdentifier() {
        return scanId;
    }

    /**
     * Returns the discovery time.
     *
     * @return discovery time
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * Verifies if the discovery is to be stopped after the progress done
     *
     * @return <tt>true</tt> to stop, <tt>false</tt> otherwise
     */
    public boolean stopAfterTimeoutExpiration() {
        return stopAfterTimeoutExpiration;
    }

    /**
     * Returns the discovery settings.
     *
     * @return scanner settings object
     */
    @Nullable
    public S getScannerSettings() {
        return scannerSettings;
    }

    /**
     * Returns the discovery filter.
     *
     * @return scanner filter object
     */
    @Nullable
    public F getScannerFilter() {
        return scannerFilter;
    }

    /**
     * Returns the stop rule.
     *
     * @return stop rule object
     */
    @NonNull
    public StopRule<Collection<Device>> getStopRule() {
        return stopRule;
    }

    /**
     * {@link DiscoveryProperties} builder.
     */
    public static class Builder<S, F> {
        private String scanId;
        private int timeout;
        private boolean stopAfterTimeoutExpiration;
        @Nullable
        private S scannerSettings;
        @Nullable
        private F scannerFilter;
        @NonNull
        private StopRule<Collection<Device>> stopRule;

        /**
         * Constructor of {@link Builder} class.
         */
        private Builder() {
            this.scanId = Constants.DEFAULT_SCANNER_UUID;
            this.timeout = Constants.DEFAULT_DISCOVERY_TIME;
            this.stopAfterTimeoutExpiration = true;
            this.scannerSettings = null;
            this.scannerFilter = null;
            this.stopRule = StopRule.NO_RULE;
        }

        /**
         * Changes the scan identifier.
         * <br/>
         * Default: {@value Constants#DEFAULT_SCANNER_UUID}
         *
         * @param scanId scan identifier
         * @return builder object
         */
        @NonNull
        public Builder<S, F> setScanId(@NonNull String scanId) {
            this.scanId = scanId;
            return this;
        }

        /**
         * Changes de discovery time
         * <br/>
         * Default: {@value Constants#DEFAULT_DISCOVERY_TIME}
         *
         * @param timeout discovery time
         * @return builder object
         */
        @NonNull
        public Builder<S, F> setTimeout(int timeout) {
            this.timeout = timeout;
            return this;
        }

        /**
         * Changes if the discovery is to stop after the progress done or not.
         * <br/>
         * Default: true
         * <br/>
         * In Wifi P2p the discovery must run before connection, otherwise it won't be able to
         * connect.
         *
         * @param stop <tt>true</tt> to stop visibility, <tt>false</tt> otherwise
         * @return builder object
         */
        @NonNull
        public Builder<S, F> stopAfterTimeoutExpiration(boolean stop) {
            stopAfterTimeoutExpiration = stop;
            return this;
        }

        /**
         * Changes the discovery settings.
         *
         * @param settings scanner settings object
         * @return builder object
         */
        @NonNull
        public Builder<S, F> setScannerSettings(@NonNull S settings) {
            this.scannerSettings = settings;
            return this;
        }

        /**
         * Changes the discovery filter.
         *
         * @param filter scanner filter object
         * @return builder object
         */
        @NonNull
        public Builder<S, F> setScannerFilter(@NonNull F filter) {
            this.scannerFilter = filter;
            return this;
        }

        /**
         * Changes the stop rule.
         * The discovery will stop when the rules have been met.
         *
         * @param stopRule stop object rule
         * @return builder object
         */
        @NonNull
        public Builder<S, F> setStopRule(@NonNull StopRule<Collection<Device>> stopRule) {
            this.stopRule = stopRule;
            return this;
        }

        /**
         * Returns a {@link VisibilityProperties} object with the defined visibility properties.
         *
         * @return object with discoverable properties
         */
        @NonNull
        public DiscoveryProperties<S, F> build() {
            return new DiscoveryProperties<>(this);
        }
    }
}
