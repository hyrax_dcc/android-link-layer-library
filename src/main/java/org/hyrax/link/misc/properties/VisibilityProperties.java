/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.properties;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Constants;

/**
 * General properties for devices visibility.
 *
 * @param <S> advertiser settings object
 * @param <D> advertiser data object
 */
public class VisibilityProperties<S, D> {
    private final String advertiseId;
    private final int timeout;
    private final boolean stopAfterTimeoutExpiration;
    @Nullable
    private final S advertiserSettings;
    @Nullable
    private final D advertiserData;

    /**
     * Constructs a {@link VisibilityProperties} given a discoverable {@link Builder} object.
     *
     * @param builder builder with properties
     */
    private VisibilityProperties(@NonNull Builder<S, D> builder) {
        this.advertiseId = builder.advertiseId;
        this.timeout = builder.timeout;
        this.stopAfterTimeoutExpiration = builder.stopAfterTimeoutExpiration;
        this.advertiserSettings = builder.advertiserSettings;
        this.advertiserData = builder.advertiserData;
    }

    /**
     * Instantiates a builder object in order to defined the intended parameters.
     *
     * @return a {@link Builder} object
     */
    @NonNull
    public static <S, D> Builder<S, D> newBuilder() {
        return new Builder<>();
    }

    /**
     * Returns an advertiser identifier.
     *
     * @return advertiser id
     */
    @NonNull
    public String getIdentifier() {
        return advertiseId;
    }

    /**
     * Returns the visibility time.
     *
     * @return visibility time
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * Verifies if the visibility is to be stopped after the progress done
     *
     * @return <tt>true</tt> to stop, <tt>false</tt> otherwise
     */
    public boolean stopAfterTimeoutExpiration() {
        return stopAfterTimeoutExpiration;
    }

    /**
     * Return the visibility settings object
     *
     * @return advertiser settings object
     */
    @Nullable
    public S getAdvertiserSettings() {
        return advertiserSettings;
    }

    /**
     * Returns the visibility data object.
     *
     * @return advertiser data object
     */
    @Nullable
    public D getAdvertiserData() {
        return advertiserData;
    }

    /**
     * {@link VisibilityProperties} builder.
     */
    public static final class Builder<S, D> {
        private String advertiseId;
        private int timeout;
        private boolean stopAfterTimeoutExpiration;
        @Nullable
        private S advertiserSettings;
        @Nullable
        private D advertiserData;


        /**
         * Constructor of {@link Builder} class.
         */
        private Builder() {
            this.advertiseId = Constants.DEFAULT_ADVERTISER_UUID;
            this.timeout = Constants.DEFAULT_VISIBILITY_TIME;
            this.stopAfterTimeoutExpiration = true;
            this.advertiserSettings = null;
            this.advertiserData = null;
        }

        /**
         * Changes the advertiser identifier.
         * <br/>
         * Default: {@value Constants#DEFAULT_ADVERTISER_UUID}
         *
         * @param advertiseId advertiser identifier
         * @return builder object
         */
        @NonNull
        public Builder<S, D> setAdvertiseId(@NonNull String advertiseId) {
            this.advertiseId = advertiseId;
            return this;
        }

        /**
         * Changes de visibility time
         * <br/>
         * Default: {@value Constants#DEFAULT_VISIBILITY_TIME}
         * <br/>
         * If timeout is zero the visibility will last forever (won't stop), thus it must be stopped
         * manually.
         *
         * @param timeout visibility time
         * @return builder object
         */
        @NonNull
        public Builder<S, D> setTimeout(int timeout) {
            this.timeout = timeout;
            return this;
        }

        /**
         * Changes if the visibility is to stop after the progress done or not.
         * <br/>
         * Default: true
         * <br/>
         * In Bluetooth Le the visibility must be permanent(<tt>false</tt>) otherwise the gatt
         * connections will be lost.
         *
         * @param stop <tt>true</tt> to stop visibility, <tt>false</tt> otherwise
         * @return builder object
         */
        @NonNull
        public Builder<S, D> stopAfterTimeoutExpiration(boolean stop) {
            stopAfterTimeoutExpiration = stop;
            return this;
        }

        /**
         * Changes the visibility settings.
         *
         * @param settings advertiser settings object
         * @return builder object
         */
        @NonNull
        public Builder<S, D> setAdvertiserSettings(@NonNull S settings) {
            this.advertiserSettings = settings;
            return this;
        }

        /**
         * Changes the visibility data.
         *
         * @param data advertiser data object
         * @return builder object
         */
        @NonNull
        public Builder<S, D> setAdvertiserData(@NonNull D data) {
            this.advertiserData = data;
            return this;
        }

        /**
         * Returns a {@link VisibilityProperties} object with the defined visibility properties.
         *
         * @return object with discoverable properties
         */
        @NonNull
        public VisibilityProperties<S, D> build() {
            return new VisibilityProperties<>(this);
        }
    }
}
