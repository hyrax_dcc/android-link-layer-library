/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.properties;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Constants;

/**
 * General properties for creating a server (connections acceptance).
 */
public class ServerProperties<S> {
    private final String identifier;
    private final String name;
    @Nullable
    private final S settings;

    /**
     * Constructs a {@link ServerProperties} given a connection {@link Builder} object.
     *
     * @param builder builder with properties
     */
    private ServerProperties(@NonNull Builder<S> builder) {
        this.identifier = builder.identifier;
        this.name = builder.name;
        this.settings = builder.settings;
    }

    /**
     * Instantiates a builder object in order to defined the intended parameters.
     *
     * @return a {@link Builder} object
     */
    @NonNull
    public static <S> Builder<S> newBuilder() {
        return new Builder<>();
    }

    /**
     * Returns the server identifier.
     *
     * @return server identifier.
     */
    @NonNull
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Returns the server name.
     *
     * @return server name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the server settings.
     *
     * @return server settings object
     */
    @Nullable
    public S getSettings() {
        return settings;
    }

    /**
     * {@link ServerProperties} builder.
     */
    public static final class Builder<S> {
        private String identifier;
        private String name;
        @Nullable
        private S settings;

        /**
         * Builder constructor.
         */
        private Builder() {
            this.identifier = Constants.DEFAULT_SERVER_UUID;
            this.name = Constants.DEFAULT_SERVER_NAME;
            this.settings = null;
        }

        /**
         * Changes the server identifier
         *
         * @param identifier server identifier
         * @return builder object
         */
        @NonNull
        public Builder<S> setIdentifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        /**
         * Changes the name of a server
         * <br/>
         * Default: {@value Constants#DEFAULT_SERVER_NAME}
         *
         * @param name server name
         * @return builder object
         */
        @NonNull
        public Builder<S> setName(String name) {
            this.name = name;
            return this;
        }

        /**
         * Changes the server settings.
         *
         * @param settings server settings object
         * @return builder object
         */
        @NonNull
        public Builder<S> setSettings(@NonNull S settings) {
            this.settings = settings;
            return this;
        }

        /**
         * Returns a {@link ServerProperties} object with the defined server properties.
         *
         * @return object with server properties
         */
        @NonNull
        public ServerProperties<S> build() {
            return new ServerProperties<>(this);
        }
    }
}
