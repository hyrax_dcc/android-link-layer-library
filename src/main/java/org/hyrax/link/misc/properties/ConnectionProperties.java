/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.properties;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Constants;

/**
 * General properties for establish a connection.
 *
 * @param <S> connection settings
 */
public class ConnectionProperties<S> {
    @NonNull
    private final String serverIdentifier;
    private final String password;
    private final int timeout;
    private final boolean removeWhenDisconnect;
    @Nullable
    private final S settings;

    /**
     * Constructs a {@link ConnectionProperties} given a connection {@link Builder} object.
     *
     * @param builder builder with properties
     */
    private ConnectionProperties(@NonNull Builder<S> builder) {
        this.serverIdentifier = builder.serverIdentifier;
        this.password = builder.password;
        this.timeout = builder.timeout;
        this.removeWhenDisconnect = builder.removeWhenDisconnect;
        this.settings = builder.settings;
    }

    /**
     * Instantiates a builder object in order to defined the intended parameters.
     *
     * @param serverIdentifier server identifier. e.g ssid, uuid
     * @return a {@link Builder} object
     */
    @NonNull
    public static <S> Builder<S> newBuilder(@NonNull String serverIdentifier) {
        return new Builder<>(serverIdentifier);
    }

    /**
     * Returns a network identifier.
     *
     * @return network identifier
     */
    @NonNull
    public String getIdentifier() {
        return serverIdentifier;
    }

    /**
     * Returns a network password.
     *
     * @return network password
     */
    @NonNull
    public String getPassword() {
        return password;
    }

    /**
     * Returns a connection timeout
     *
     * @return timeout
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * Verifies if the network is to be remove after disconnect.
     *
     * @return <tt>true</tt> to remove, <tt>false</tt> otherwise
     */
    public boolean toRemoveWhenDisconnect() {
        return removeWhenDisconnect;
    }

    /**
     * Returns the current settings, if available or Null
     *
     * @return connection settings object
     */
    @Nullable
    public S getSettings() {
        return this.settings;
    }

    /**
     * {@link ConnectionProperties} builder.
     */
    public static final class Builder<S> {
        @NonNull
        private final String serverIdentifier;
        private String password;
        private int timeout;
        private boolean removeWhenDisconnect;
        @Nullable
        private S settings;

        /**
         * Builder constructor.
         *
         * @param serverIdentifier server identifier. e.g ssid, uuid
         */
        private Builder(@NonNull String serverIdentifier) {
            this.serverIdentifier = serverIdentifier;
            this.password = Constants.UNDEFINED;
            this.timeout = Constants.DEFAULT_CONNECTION_TIME;
            this.removeWhenDisconnect = false;
            this.settings = null;
        }

        /**
         * Changes the network password.
         * <br/>
         * Default: {@value Constants#UNDEFINED}
         *
         * @param password network password
         * @return builder object
         */
        @NonNull
        public Builder<S> setPassword(@NonNull String password) {
            this.password = password;
            return this;
        }

        /**
         * Changes the connection timeout.
         * <br/>
         * Default: {@value Constants#DEFAULT_CONNECTION_TIME}
         *
         * @param timeout connection timeout
         * @return builder object
         */
        @NonNull
        public Builder<S> setTimeout(int timeout) {
            this.timeout = timeout;
            return this;
        }

        /**
         * Instruction to remove, or not, the network after disconnect
         * <br/>
         * Default: depends of technology
         *
         * @param remove <tt>true</tt> to remove, <tt>false</tt> otherwise
         * @return builder object
         */
        @NonNull
        public Builder<S> removeWhenDisconnect(boolean remove) {
            this.removeWhenDisconnect = remove;
            return this;
        }

        /**
         * Changes the connection settings
         *
         * @param settings general settings object
         * @return builder object
         */
        @NonNull
        public Builder<S> setSettings(@NonNull S settings) {
            this.settings = settings;
            return this;
        }

        /**
         * Returns a {@link ConnectionProperties} object with the defined connection properties.
         *
         * @return object with connection properties
         */
        @NonNull
        public ConnectionProperties<S> build() {
            return new ConnectionProperties<>(this);
        }
    }
}
