/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc;

import android.support.annotation.NonNull;

/**
 * This enum class allows to defined zero configuration bonjour services.
 * <br/>
 * There is dozens of services and here a just represented a few.
 * <br/>
 * For more information about the bonjour services access to
 * <a href="http://zerospan.org/proj/wiki/CompleteListOfServices" target="_blank">Bonjour services</a>
 */
public enum Bonjour {
    PRESENCE_TCP("_presence._tcp"),
    SERVICE_TCP("_service._tcp"),

    NONE("none");
    //SSH("ssh"),
    //PRINTER("printer");

    @NonNull
    private final String protocol;

    /**
     * Constructor of enum service.
     * <br/>
     * Format: _<service_name>._<transport_protocol>
     *
     * @param protocol bonjour service format.
     */
    Bonjour(@NonNull String protocol) {
        this.protocol = protocol;
    }

    /**
     * Return a bonjour protocol object from a given string.
     *
     * @param protocolStr protocol string name
     * @return bonjour protocol object
     */
    @NonNull
    public static Bonjour translate(@NonNull String protocolStr) {
        Bonjour[] values = Bonjour.values();

        for (Bonjour b : values) {
            if (b.getProtocolName().equals(protocolStr) ||
                    protocolStr.startsWith(b.getProtocolName()) ||
                    protocolStr.endsWith(b.getProtocolName())) {
                return b;
            }
        }

        return NONE;
    }

    /**
     * Returns the name of protocol.
     *
     * @return protocol name
     */
    @NonNull
    public String getProtocolName() {
        return protocol;
    }

    @Override
    public String toString() {
        return getProtocolName();
    }
}
