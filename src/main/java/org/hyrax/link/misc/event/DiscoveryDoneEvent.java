/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.event;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.misc.outcome.Error;
import org.hyrax.link.misc.outcome.Success;
import org.hyrax.link.misc.exception.LinkException;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Hardware discovery done event.
 * <br/>
 * Event triggered when the discovery progress has been finished.
 */
public class DiscoveryDoneEvent extends Event<Collection<Device>> {

    /**
     * Constructor of {@link DiscoveryDoneEvent}, success.
     *
     * @param scannerId scanner identifier
     * @param devices   discovered list of devices
     */
    public DiscoveryDoneEvent(@NonNull String scannerId, @NonNull Collection<Device> devices) {
        super(LinkEvent.LINK_DISCOVERY_DONE, new Success<>(devices), scannerId);
    }

    /**
     * Constructor of {@link DiscoveryDoneEvent}, failure.
     *
     * @param scannerId scanner identifier
     * @param throwable error object
     */
    public DiscoveryDoneEvent(@NonNull String scannerId, @NonNull LinkException throwable) {
        super(LinkEvent.LINK_DISCOVERY_DONE,
                new Error<>(throwable, new ArrayList<>()), scannerId);
    }
}
