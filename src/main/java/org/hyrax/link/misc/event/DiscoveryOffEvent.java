/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.event;

import android.support.annotation.NonNull;

import org.hyrax.link.LinkEvent;
import org.hyrax.link.misc.outcome.Error;
import org.hyrax.link.misc.outcome.Success;
import org.hyrax.link.misc.exception.LinkException;

/**
 * Hardware stopped discovery event.
 * <br/>
 * Event triggered when the hardware discovery has been stopped.
 */
public class DiscoveryOffEvent extends Event<String> {

    /**
     * Constructor of {@link DiscoveryOffEvent}, success.
     *
     * @param scanId scanner identifier
     */
    public DiscoveryOffEvent(@NonNull String scanId) {
        super(LinkEvent.LINK_DISCOVERY_OFF, new Success<>(scanId), scanId);
    }

    /**
     * Constructor of {@link DiscoveryOffEvent}, failure.\
     *
     * @param scanId    scanner identifier
     * @param throwable error object
     */
    public DiscoveryOffEvent(@NonNull String scanId, @NonNull LinkException throwable) {
        super(LinkEvent.LINK_DISCOVERY_OFF, new Error<>(throwable, scanId), scanId);
    }
}
