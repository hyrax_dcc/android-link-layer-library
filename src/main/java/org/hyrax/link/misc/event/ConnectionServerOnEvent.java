/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.event;

import android.support.annotation.NonNull;

import org.hyrax.link.LinkEvent;
import org.hyrax.link.misc.outcome.Error;
import org.hyrax.link.misc.outcome.Success;
import org.hyrax.link.misc.exception.LinkException;

/**
 * Connection server started event.
 * <br/>
 * Event triggered when a connection server is started.
 */
public class ConnectionServerOnEvent extends Event<String> {

    /**
     * Constructor of {@link ConnectionServerOnEvent}, success.
     *
     * @param serverId server identifier
     */
    public ConnectionServerOnEvent(@NonNull String serverId) {
        super(LinkEvent.LINK_CONNECTION_SERVER_ON, new Success<>(serverId), serverId);
    }

    /**
     * Constructor of {@link ConnectionServerOnEvent}, failure.
     *
     * @param serverId  server identifier
     * @param throwable error object
     */
    public ConnectionServerOnEvent(@NonNull String serverId, @NonNull LinkException throwable) {
        super(LinkEvent.LINK_CONNECTION_SERVER_ON, new Error<>(throwable, serverId), serverId);
    }
}
