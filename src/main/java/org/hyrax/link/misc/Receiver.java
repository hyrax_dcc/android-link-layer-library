/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import org.hyrax.link.misc.event.Event;

import java.util.List;

/**
 * Abstract class {@link Receiver} which is the bridge between the raw events and the events
 * defined thought {@link Event} class.
 *
 * @see Event
 * @see <a href="https://developer.android.com/reference/android/content/BroadcastReceiver.html" target="_blank">BroadcastReceiver</a>
 */

public abstract class Receiver extends BroadcastReceiver {
    @NonNull
    private final Aggregator aggregator;

    /**
     * Creates a {@link Receiver} object.
     * <br/>
     * It receives an object {@link Aggregator}, in order to access the registered listeners.
     *
     * @param aggregator listener {@link Aggregator}
     */
    public Receiver(@NonNull Aggregator aggregator) {
        this.aggregator = aggregator;
    }

    /**
     * Notifies the listeners in the {@link Aggregator} that an a link layer event has been
     * triggered.
     *
     * @param aggregator listeners aggregator
     * @param event      the triggered event
     * @see Aggregator
     * @see Event
     */
    public static void notifyEvent(@NonNull Aggregator aggregator, @NonNull Event event) {
        aggregator.notifyListeners(event.getEvent(), event.getFilter(), event.getOutcome());
    }

    /**
     * This method receives the Android events.
     *
     * @param context application {@link Context}
     * @param intent  data {@link Intent}
     * @see <a href="https://developer.android.com/reference/android/content/BroadcastReceiver.html" target="_blank">BroadcastReceiver</a>
     * @see <a href="https://developer.android.com/reference/android/content/Context.html", target="_blank">Context<a/>
     * @see <a href="https://developer.android.com/reference/android/content/Intent.html" target="_blank">Intent</a>
     */
    @Override
    public void onReceive(final Context context, @NonNull Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            final List<Event> techEvents = actionToTechEvent(action, intent);

            for (Event event : techEvents) {
                notifyEvent(aggregator, event);
            }
        }
    }

    /**
     * Return a list of {@link Event}, by converting raw events into events defined thought
     * {@link Event} class.
     * <br/>
     * This method is responsible for mapping the Android generated event into well defined classes
     * implemented by the link layer.
     *
     * @param action raw event identifier
     * @param intent extra event data
     * @return a list of mapped events
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/List.html" target="_blank">List</a>
     * @see <a href="https://developer.android.com/reference/android/content/Intent.html" target="_blank">Intent</a>
     */
    @NonNull
    protected abstract List<Event> actionToTechEvent(@NonNull String action, @NonNull Intent intent);

    /**
     * Returns an {@link IntentFilter} used to listen Android events.
     * <br/>
     * The Android dispatches events in common channel, the must be filtered using an {@link IntentFilter}.
     *
     * @return an {@link IntentFilter} object
     * @see <a href="https://developer.android.com/reference/android/content/IntentFilter.html" target="_blank">IntentFilter</a>
     */
    @NonNull
    public abstract IntentFilter getIntentFilters();
}
