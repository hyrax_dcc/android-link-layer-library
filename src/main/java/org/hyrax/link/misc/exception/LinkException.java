/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.exception;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;

/**
 * {@link LinkException } is used to distinguish if the exception occurred during the link layer
 * runtime or by external factors.
 */
public class LinkException extends Exception {
    @NonNull
    private final Reason reason;

    /**
     * Constructor of {@link LinkException} class, given a reason and message.
     *
     * @param reason  error classification
     * @param message error message
     */
    private LinkException(@NonNull Reason reason, @NonNull String message) {
        super(reason.name() + ": " + message);
        this.reason = reason;
    }

    /**
     * Constructor of {@link LinkException} class, and {@link Exception} object.
     *
     * @param e exception object
     */
    private LinkException(@NonNull Exception e) {
        super(e);
        this.reason = Reason.NO_LINK_EXCEPTION;
    }

    @NonNull
    public static LinkException build(@NonNull Exception e) {
        return new LinkException(e);
    }

    @NonNull
    public static LinkException build(@NonNull Reason reason) {
        return build(reason, Constants.UNDEFINED);
    }

    @NonNull
    public static LinkException build(@NonNull Reason reason, @NonNull String string) {
        return new LinkException(reason, string);
    }

    /**
     * Returns the reason of the exception
     *
     * @return error reason
     */
    @NonNull
    public Reason getReason() {
        return reason;
    }
}
