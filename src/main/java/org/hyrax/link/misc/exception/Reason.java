/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.exception;

/**
 * Class that means to classify the error source.
 */
public enum Reason {
    /**
     * Undefined exception. Usually is not an exception at all.
     */
    EMPTY_EXCEPTION,
    /**
     * Exception not related with link layer.
     */
    NO_LINK_EXCEPTION,
    /**
     * Link not attach exception.
     */
    DETACHED,
    /**
     * The logic timeout has expired.
     */
    LOGIC_TIMEOUT,
    /**
     * NULL resource.
     */
    NULL_POINTER,
    /**
     * The device has the location disable.
     */
    LOCATION_DISABLED,
    /**
     * Error starting a logic action.
     */
    ERROR_START,
    /**
     * Error stopping a logic action.
     */
    ERROR_STOP,
    /**
     * Means that the rules for some instance have not been met.
     */
    INVALID_INSTANCE,
    /**
     * Means the some resource has been started.
     */
    ALREADY_STARTED,
    /**
     * Means that something is not supported.
     */
    UNSUPPORTED,
    /**
     * Too much data to sent.
     */
    DATA_TOO_LARGE,
    /**
     * Too many active resources.
     */
    TOO_MANY_ACTIVE,
    /**
     * An undefined internal error,
     */
    INTERNAL_ERROR,
    /**
     * Warning. Just gives some feedback.
     */
    WARNING,
    /**
     * Resource is busy.
     */
    BUSY,
}
