/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc;

import android.support.annotation.NonNull;

/**
 * This class represents the LinkFF Layer constants
 */
public class Constants {
    //global constants
    @NonNull
    public static final String DEFAULT_SERVER_NAME = "LINK_LAYER_SERVER_NAME";
    //use always the uuid in lower case
    @SuppressWarnings("SpellCheckingInspection")
    @NonNull
    public static final String DEFAULT_SERVER_UUID = "172ea7f1-cbfe-48fb-a313-47e395cdd642";
    @NonNull
    public static final String DEFAULT_ADVERTISER_UUID = "8e2e613b-59dc-4af1-9ce8-c54c0c72bd6c";
    @NonNull
    public static final String DEFAULT_SCANNER_UUID = "edc1b196-c5e2-4a92-985a-daa38ebc6c05";

    @NonNull
    public static final String UNDEFINED = "UNDEFINED";
    @NonNull
    public static final String HOST = "HOST";
    @NonNull
    public static final String PORT = "PORT";
    @NonNull
    public static final String DEFAULT_GO_IP = "192.168.49.1";

    public static final int DEFAULT_DISCOVERY_TIME = 8000; //8 seconds
    public static final int DEFAULT_VISIBILITY_TIME = 12000; //12seconds
    public static final int DEFAULT_CONNECTION_TIME = 20000; //20 seconds
    public static final int DEFAULT_LOGIC_TIMEOUT = 3000; //3 seconds
    public static final int DEFAULT_HARDWARE_ENABLE_TIMEOUT = 10000; // 10 seconds
    public static final short DEFAULT_RSSI = -50;
}


