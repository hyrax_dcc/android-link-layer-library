/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.sync;

import android.support.annotation.NonNull;

import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.misc.event.DiscoveryOffEvent;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.logic.translator.DiscoveryLogicTranslator;

/**
 * Logic device discovery disable.
 * <br/>
 * This class is an abstraction to disable the device discovery and it can be used by multiple
 * technologies.
 */
public class DiscoveryStopSync extends LogicControllerSync<String> {
    @NonNull
    private final DiscoveryLogicTranslator logicTranslator;
    @NonNull
    private final String scanId;
    /**
     * Listens discovery stop events.
     */
    private final LinkListener<String> stopListener = (eventType, outcome) -> notifyEventResult(outcome);

    /**
     * Constructor of class {@link DiscoveryStopSync}.
     *
     * @param logicTranslator translates logic commands into physical ones
     * @param scanId          scanner identifier
     */
    public DiscoveryStopSync(@NonNull DiscoveryLogicTranslator logicTranslator, @NonNull String scanId) {
        super();
        this.logicTranslator = logicTranslator;
        this.scanId = scanId;
    }

    @Override
    protected void onStart() {
        aggregator.listenOnce(stopListener, LinkEvent.LINK_DISCOVERY_OFF, scanId, true);
        logicTranslator.turnDiscoveryOff(scanId);
    }

    @NonNull
    @Override
    protected Event<String> onError(@NonNull LinkException error) {
        return new DiscoveryOffEvent(scanId, error);
    }

    @Override
    protected void onFinish() {
        aggregator.removeListener(stopListener);
    }
}
