/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.async;

import android.os.Handler;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;

import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Abstract class that enables asynchronous extra control over the calls to the android libraries.
 * <br/>
 * The logic actions enable extra features which are not available in the libraries provide
 * by Android. One of this features, by default, is the ability to synchronise the result of an
 * action at any time, by allowing to mix asynchronous and synchronous programming models
 * transparently.
 */
public abstract class LogicControllerAsync<R> {
    private final LinkEvent event;
    private final Object filter;
    private final int timeout;
    private final AtomicBoolean triggered;

    Handler workerHandler;
    /**
     * Base listener that listen for the default event to be triggered.
     */
    private final LinkListener<R> baseListener = new LinkListener<R>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<R> outcome) {
            if (triggered.compareAndSet(false, true)) {
                workerHandler.removeCallbacks(timer);
                onActionReturned(outcome);
            }
        }
    };
    Aggregator aggregator;
    /**
     * This runnable will triggered if the execution timeout has expired
     */
    private final Runnable timer = new Runnable() {
        @Override
        public void run() {
            if (triggered.compareAndSet(false, true)) {
                aggregator.removeListener(baseListener);
                publishEvent(
                        onError(LinkException.build(Reason.LOGIC_TIMEOUT,
                                String.format(Locale.ENGLISH, "Timeout has expired %d for event %s",
                                        timeout, event.name()))));
            }
        }
    };

    /**
     * Constructor.
     *
     * @param event  the event to be executed
     * @param filter the event object filter
     */
    LogicControllerAsync(@NonNull LinkEvent event, @NonNull Object filter) {
        this(event, filter, Constants.DEFAULT_LOGIC_TIMEOUT);
    }

    /**
     * Constructor.
     *
     * @param event   the event to be executed
     * @param filter  the event object filter
     * @param timeout the maximum execution time
     */
    LogicControllerAsync(@NonNull LinkEvent event, @NonNull Object filter, int timeout) {
        this.event = event;
        this.filter = filter;
        this.timeout = timeout;
        this.triggered = new AtomicBoolean(false);
    }

    /**
     * Sets the listener aggregator object.
     *
     * @param aggregator aggregator object
     */
    public void setAggregator(@NonNull Aggregator aggregator) {
        this.aggregator = aggregator;
    }

    /**
     * Sets the worker handler.
     * This handler is a thread that will be used for tasks execution purposes.
     *
     * @param workerHandler handler object
     */
    public void setWorkerHandler(@NonNull Handler workerHandler) {
        this.workerHandler = workerHandler;
    }

    /**
     * Performs the logic action.
     */
    public void perform() {
        Objects.requireNonNull(aggregator);
        Objects.requireNonNull(workerHandler);

        aggregator.listenOnce(baseListener, event, filter, true);
        workerHandler.postDelayed(timer, timeout);

        onStart();
    }

    /**
     * This abstract method is called when is ready perform the intended action.
     */
    abstract void onStart();

    /**
     * This abstract method is called when an action has concluded it's execution with errors.
     * <br/>
     * The objective is to return an error event associated with the action.
     *
     * @param error exception error
     * @return an {@link Event} error object representation
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html?is-external=true" target="_blank">Exception</a>
     */
    @NonNull
    abstract Event<R> onError(@NonNull LinkException error);

    /**
     * This abstract method is called when action execution has finished.
     * <br/>
     * This method should be used to clean whatever need to be cleansed.
     */
    void onActionReturned(@NonNull Outcome<R> outcome) {
        //do nothing
    }


    /**
     * Notifies a new_def {@link Event}.
     *
     * @param event event to be notified
     */
    void publishEvent(@NonNull Event<R> event) {
        Receiver.notifyEvent(aggregator, event);
    }
}
