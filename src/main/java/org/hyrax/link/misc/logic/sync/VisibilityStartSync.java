/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.sync;

import android.os.Handler;
import android.support.annotation.NonNull;

import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.event.VisibilityOnEvent;
import org.hyrax.link.misc.logic.StopVisibilityAfterTimeout;
import org.hyrax.link.misc.logic.translator.VisibilityLogicTranslator;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.misc.exception.LinkException;

/**
 * Logic device visibility enable.
 * <br/>
 * This class is an abstraction to enable the device visibility and it can be used by multiple technologies.
 */
public class VisibilityStartSync extends LogicControllerSync<String> {
    @NonNull
    private final VisibilityLogicTranslator logicTranslator;
    @NonNull
    private final VisibilityProperties properties;
    @NonNull
    private final Handler workerHandler;
    /**
     * Listens visibility start events.
     */
    private final LinkListener<String> startListener = (eventType, outcome) -> notifyEventResult(outcome);

    /**
     * Constructor of class {@link VisibilityStartSync}.
     *
     * @param workerHandler   worker handler
     * @param logicTranslator translates logic commands into physical ones
     * @param properties      visibility properties
     */
    public VisibilityStartSync(@NonNull Handler workerHandler,
                               @NonNull VisibilityLogicTranslator logicTranslator,
                               @NonNull VisibilityProperties properties) {
        super();
        this.workerHandler = workerHandler;
        this.logicTranslator = logicTranslator;
        this.properties = properties;
    }

    @Override
    protected void onStart() {
        aggregator.listenOnce(startListener, LinkEvent.LINK_VISIBILITY_ON,
                properties.getIdentifier(), true);
        workerHandler.postDelayed(
                new StopVisibilityAfterTimeout(aggregator, workerHandler, logicTranslator, properties),
                properties.getTimeout());
        logicTranslator.turnVisibilityOn(properties);
    }

    @NonNull
    @Override
    protected Event<String> onError(@NonNull LinkException error) {
        return new VisibilityOnEvent(properties.getIdentifier(), error);
    }

    @Override
    protected void onFinish() {
        aggregator.removeListener(startListener);
    }
}
