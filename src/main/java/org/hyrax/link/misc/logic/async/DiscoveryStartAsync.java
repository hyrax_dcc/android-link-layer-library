/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.async;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.misc.Filter;
import org.hyrax.link.misc.event.DiscoveryDoneEvent;
import org.hyrax.link.misc.event.DiscoveryOnEvent;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.logic.translator.DiscoveryLogicTranslator;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Logic device discovery enable. More precisely, to discover the nearby devices.
 * <br/>
 * This class is an abstraction to enable the device discovery and it can be used by multiple
 * technologies.
 */
public class DiscoveryStartAsync extends LogicControllerAsync<String> {
    private final ConcurrentHashMap<Device, Device> discoveredDevices;
    private final DiscoveryLogicTranslator logicTranslator;
    private final DiscoveryProperties properties;
    private final AtomicBoolean triggered;
    /**
     * Listens progress listener events.
     */
    private final LinkListener<Collection<Device>> progressListener = new LinkListener<Collection<Device>>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<Collection<Device>> outcome) {
            outcome.ifSuccess(devices -> {
                if (triggered.get())
                    return;
                //if the link has a link layer type filter
                if (properties.getScannerFilter() instanceof Filter) {
                    Filter<Device> filter = (Filter<Device>) properties.getScannerFilter();
                    for (Device device : outcome.getOutcome())
                        if (filter.apply(device)) addToDiscovered(device);
                } else {
                    for (Device device : outcome.getOutcome())
                        addToDiscovered(device);
                }
                //disables the discovery process when all the rules have been met
                if (properties.getStopRule().stop(discoveredDevices.keySet())) {
                    //logicTranslator.turnDiscoveryOff(properties.getIdentifier());
                    workerHandler.removeCallbacks(discoveryTimer);
                    workerHandler.postAtFrontOfQueue(discoveryTimer);
                }
            });
        }

        /**
         * Adds a device to the discovered collection.
         * @param device discovered device
         */
        private void addToDiscovered(@NonNull Device device) {
            Device res = discoveredDevices.putIfAbsent(device, device);
            if (null != res)
                res.mergeScanData(device.getScanData());
        }
    };
    /**
     * Listens discovery off events.
     */
    private final LinkListener<Boolean> stopListener = new LinkListener<Boolean>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<Boolean> outcome) {
            if (triggered.compareAndSet(false, true)) {
                workerHandler.removeCallbacks(discoveryTimer);
                workerHandler.post(() -> {
                    aggregator.removeListener(progressListener);
                    Receiver.notifyEvent(aggregator,
                            new DiscoveryDoneEvent(properties.getIdentifier(), discoveredDevices.values()));
                });
            }
        }
    };
    /**
     * This runnable will triggered when the discovery time expire.
     */
    private final Runnable discoveryTimer = new Runnable() {
        @Override
        public void run() {
            aggregator.removeListener(progressListener);
            if (properties.stopAfterTimeoutExpiration() && logicTranslator.isDiscoveryOn(properties.getIdentifier()))
                logicTranslator.turnDiscoveryOff(properties.getIdentifier());

            else if (triggered.compareAndSet(false, true)) {
                aggregator.removeListener(stopListener);
                Receiver.notifyEvent(aggregator,
                        new DiscoveryDoneEvent(properties.getIdentifier(), discoveredDevices.values()));
            }
        }
    };

    /**
     * Constructor.
     *
     * @param logicTranslator translates discovery logic commands into physical ones
     * @param properties      discovery properties
     */
    public DiscoveryStartAsync(@NonNull DiscoveryLogicTranslator logicTranslator,
                               @NonNull DiscoveryProperties properties) {
        super(LinkEvent.LINK_DISCOVERY_ON, properties.getIdentifier());
        this.logicTranslator = logicTranslator;
        this.properties = properties;
        this.discoveredDevices = new ConcurrentHashMap<>();
        this.triggered = new AtomicBoolean(false);
    }

    @Override
    void onStart() {
        aggregator.listenOnce(progressListener, LinkEvent.LINK_DISCOVERY_FOUND, properties.getIdentifier(), true);
        aggregator.listenOnce(stopListener, LinkEvent.LINK_DISCOVERY_OFF, properties.getIdentifier(), true);

        logicTranslator.turnDiscoveryOn(properties);
    }

    @NonNull
    @Override
    Event<String> onError(@NonNull final LinkException error) {
        workerHandler.post(() -> Receiver.notifyEvent(aggregator, new DiscoveryDoneEvent(properties.getIdentifier(), error)));
        return new DiscoveryOnEvent(properties.getIdentifier(), error);
    }

    @Override
    void onActionReturned(@NonNull Outcome<String> outcome) {
        if (!outcome.isSuccessful())
            clean();
        else
            workerHandler.postDelayed(discoveryTimer, properties.getTimeout());
    }

    /**
     * Unregisters the listeners.
     */
    private void clean() {
        workerHandler.post(() -> {
            aggregator.removeListener(progressListener);
            aggregator.removeListener(stopListener);
        });
    }
}
