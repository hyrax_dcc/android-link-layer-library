/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.sync;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.misc.event.ConnectionLostEvent;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.logic.translator.ConnectionLogicTranslator;

/**
 * Logic disconnection from a remote {@link Device}.
 * <br/>
 * This class is an abstraction to perform a disconnection and it can be used by multiple technologies.
 */
public class ConnectionStopSync extends LogicControllerSync<Device> {
    @NonNull
    private final ConnectionLogicTranslator logicTranslator;
    @NonNull
    private final Device device;
    /**
     * Listens device disconnections events.
     */
    private final LinkListener<Device> listener = (eventType, outcome) -> notifyEventResult(outcome);

    /**
     * Constructor of class {@link ConnectionStopSync}.
     *
     * @param logicTranslator translates logic commands into physical ones
     * @param device          {@link Device} to connect
     */
    public ConnectionStopSync(@NonNull ConnectionLogicTranslator logicTranslator,
                              @NonNull Device device) {
        super();
        this.logicTranslator = logicTranslator;
        this.device = device;
    }

    @Override
    protected void onStart() {
        aggregator.listenOnce(listener, LinkEvent.LINK_CONNECTION_LOST, device, true);
        if (logicTranslator.isConnectionOn(device)) {
            logicTranslator.turnConnectionOff(device);
        } else {
            publishEvent(new ConnectionLostEvent(device));
        }
    }

    @NonNull
    @Override
    protected Event<Device> onError(@NonNull LinkException error) {
        return new ConnectionLostEvent(device, error);
    }

    @Override
    protected void onFinish() {
        aggregator.removeListener(listener);
    }
}
