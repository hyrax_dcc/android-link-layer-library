/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.sync;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Abstract class that enables synchronous control over the calls to the android libraries.
 * <br/>
 * The logic actions enable extra features which are not available in the libraries provide
 * by Android. One of this features, by default, is the ability to synchronise the result of an
 * action at any time, by allowing to mix asynchronous and synchronous programming models
 * transparently.
 *
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Callable.html" target="_blank">Callable</a>
 */
public abstract class LogicControllerSync<R> implements Callable<Outcome<R>> {
    @NonNull
    private final AtomicReference<Outcome<R>> outcomeLogic;
    private final int timeout;
    private final Lock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();
    @NonNull
    private final AtomicBoolean isResultAvailable;
    Aggregator aggregator;

    /**
     * Constructor of a generic {@link LogicControllerSync} object.
     * <br/>
     * In this logic control each action has default maximum time
     * {@value Constants#DEFAULT_LOGIC_TIMEOUT} milliSeconds
     * to be executed, otherwise it will thrown and exception.
     */
    LogicControllerSync() {
        this(Constants.DEFAULT_LOGIC_TIMEOUT);
    }

    /**
     * Constructor of a generic {@link LogicControllerSync} object.
     * <br/>
     * In this logic control each action has a defined maximum time to be executed,
     * otherwise it will thrown and exception.
     *
     * @param timeout action max time execution
     */
    LogicControllerSync(int timeout) {
        this.timeout = timeout;

        this.isResultAvailable = new AtomicBoolean(false);
        this.outcomeLogic = new AtomicReference<>(null);
    }

    /**
     * Sets the listener aggregator object.
     *
     * @param aggregator aggregator object
     * @see Aggregator
     */
    public void setAggregator(@NonNull Aggregator aggregator) {
        this.aggregator = aggregator;
    }

    /**
     * Waits for a period of time, that can be interrupted at any time.
     *
     * @param time sleep time
     * @throws InterruptedException the current {@link Thread} is interrupted
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/InterruptedException.html" target="_blank">InterruptedException</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.html" target="_blank">Thread</a>
     */
    void waitPeriod(long time) throws InterruptedException {
        try {
            lockPeriodOfTime(time);
        } catch (TimeoutException e) {
            //Lock period has elapsed. In this case is not an error.
        }
    }

    /**
     * Waits for a period of time, that can be interrupted at any time.
     * <br/>
     * Although if the time elapses an error will be thrown. The idea of this method is to provide
     * a certain time for a task be executed, throwing an error if it was not completed in time.
     *
     * @param time maximum execution time
     * @throws InterruptedException the current {@link Thread} is interrupted
     * @throws TimeoutException     the time has elapsed
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/InterruptedException.html" target="_blank">InterruptedException</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/TimeoutException.html" target="_blank">TimeoutException</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.html" target="_blank">Thread</a>
     */
    void waitPeriodAndThrow(long time) throws TimeoutException, InterruptedException {
        lockPeriodOfTime(time);
    }

    /**
     * Locks the current {@link Thread} for a period of time, throwing an exception if the time has
     * elapsed or whether the current {@link Thread} is interrupted.
     *
     * @param time maximum execution time
     * @throws InterruptedException the current {@link Thread} is interrupted
     * @throws TimeoutException     the time has elapsed
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/InterruptedException.html" target="_blank">InterruptedException</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/TimeoutException.html" target="_blank">TimeoutException</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/TimeUnit.html" target="_blank">TimeUnit</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.html" target="_blank">Thread</a>
     */
    private void lockPeriodOfTime(long time) throws InterruptedException, TimeoutException {
        lock.lock();
        try {
            while (!isResultAvailable.get()) {
                if (!condition.await(time, TimeUnit.MILLISECONDS))
                    throw new TimeoutException("The waiting time has expired!");
            }

            isResultAvailable.compareAndSet(true, false);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Notifies, that the current action has been done.
     * <br/>
     * As an action has a maximum time to be executed, when a action has been performed
     * (either successfully or not) this method permits to notify the blocked {@link Thread}
     * to continue the execution.
     *
     * @param outcome result of an action
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.html" target="_blank">Thread</a>
     */
    void notifyEventResult(@NonNull Outcome<R> outcome) {
        outcomeLogic.set(outcome);
        continueLogic();
    }

    /**
     * Continues the logic execution, by jumping thread blocking steps.
     */
    void continueLogic() {
        lock.lock();
        try {
            if (isResultAvailable.compareAndSet(false, true))
                condition.signal();
        } finally {
            lock.unlock();
        }
    }

    /**
     * This abstract method is called when is ready perform the intended action.
     *
     * @throws LinkException exception object meaning that the execution has produce errors
     */
    abstract void onStart() throws LinkException;

    /**
     * This method is called when the action has been performed. This method is used in case
     * a current logic action needs to executed something more.
     *
     * @throws LinkException exception object meaning that the execution has produce errors
     */
    void onAfterStart() throws LinkException {

    }

    /**
     * This abstract method is called when an action has concluded it's execution with errors.
     * <br/>
     * The objective is to return an error event associated with the action.
     *
     * @param error exception error
     * @return an {@link Event} error object representation
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html?is-external=true" target="_blank">Exception</a>
     */

    @NonNull
    abstract Event<R> onError(@NonNull LinkException error);

    /**
     * This abstract method is called when action execution has finished.
     * <br/>
     * This method should be used to clean whatever need to be cleansed.
     */
    abstract void onFinish();

    @Nullable
    @Override
    public Outcome<R> call() throws Exception {
        Objects.requireNonNull(aggregator);
        Outcome<R> result = null;
        Event<R> errorEvent = null;
        try {
            onStart();
            waitPeriodAndThrow(timeout);
            onAfterStart();

            result = outcomeLogic.get();
            if (null == result)
                throw LinkException.build(Reason.NULL_POINTER, "Outcome is NULL");

        } catch (LinkException e1) {
            e1.printStackTrace();
            errorEvent = onError(e1);

        } catch (TimeoutException e2) {
            e2.printStackTrace();
            errorEvent = onError(LinkException.build(Reason.LOGIC_TIMEOUT, e2.getMessage()));

        } finally {
            onFinish();
        }
        if (null != errorEvent) {
            publishEvent(errorEvent);
            result = errorEvent.getOutcome();
        }

        return result;
    }

    /**
     * Notifies a new_def {@link Event}.
     *
     * @param event event to be notified
     */
    void publishEvent(@NonNull Event<R> event) {
        Receiver.notifyEvent(aggregator, event);
    }
}
