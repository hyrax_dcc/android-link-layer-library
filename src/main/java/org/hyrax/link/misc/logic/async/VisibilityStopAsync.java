/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.async;

import android.support.annotation.NonNull;

import org.hyrax.link.LinkEvent;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.event.VisibilityOffEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.logic.translator.VisibilityLogicTranslator;

/**
 * Logic device visibility disable.
 * <br/>
 * This class is an abstraction to disable the device visibility and it can be used by multiple
 * technologies.
 */
public class VisibilityStopAsync extends LogicControllerAsync<String> {
    @NonNull
    private final VisibilityLogicTranslator logicTranslator;
    @NonNull
    private final String advertiserId;

    /**
     * Constructor of class {@link VisibilityStopAsync}.
     *
     * @param logicTranslator translates logic commands into physical ones
     * @param advertiserId    advertiser identifier
     */
    public VisibilityStopAsync(@NonNull VisibilityLogicTranslator logicTranslator,
                               @NonNull String advertiserId) {
        super(LinkEvent.LINK_VISIBILITY_OFF, advertiserId);
        this.logicTranslator = logicTranslator;
        this.advertiserId = advertiserId;
    }


    @Override
    void onStart() {
        logicTranslator.turnVisibilityOff(advertiserId);
    }

    @NonNull
    @Override
    Event<String> onError(@NonNull LinkException error) {
        return new VisibilityOffEvent(advertiserId, error);
    }
}
