/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.translator;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.properties.ServerProperties;

import java.util.Collection;

/**
 * Interface that translates logic commands to physical ones, by managing the state of device
 * servers.
 * <br/>
 * A device with an active server will be able to receive connections from clients.
 * <p>
 * Created on 27/10/16.
 */

public interface ConnectionServerLogicTranslator<S> {

    /**
     * Creates a new server.
     *
     * @param properties server properties arguments
     */
    void turnServerOn(@NonNull ServerProperties<S> properties);

    /**
     * Disables a specific server with identifier.
     *
     * @param serverId id of a server
     */
    void turnServerOff(@NonNull String serverId);

    /**
     * Verifies if a specific server identifier is active.
     *
     * @param serverId <tt>true</tt> if active, <tt>false</tt> otherwise
     */
    boolean isServerOn(@NonNull String serverId);

    /**
     * Returns a collection of active servers, string id, and their information.
     *
     * @return active servers
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/Collection.html" target="_blank">Collection</a>
     */
    @NonNull
    Collection<String> getActiveServers();

    /**
     * Cleans whatever needs to be cleansed
     */
    void detach();
}
