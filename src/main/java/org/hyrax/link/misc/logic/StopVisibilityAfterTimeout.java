/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic;

import android.os.Handler;
import android.support.annotation.NonNull;

import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.event.VisibilityDoneEvent;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.logic.translator.VisibilityLogicTranslator;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class that manages the visibility expiration time.
 */
public class StopVisibilityAfterTimeout implements Runnable {
    private final Aggregator aggregator;
    private final Handler workerHandler;
    private final VisibilityLogicTranslator logicTranslator;
    private final VisibilityProperties visibilityProperties;
    private final AtomicBoolean triggered;
    /**
     * Listens visibility on events.
     */
    private final LinkListener<String> startListener = new LinkListener<String>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull final Outcome<String> outcome) {
            if (!outcome.isSuccessful()) {
                workerHandler.removeCallbacks(StopVisibilityAfterTimeout.this);
                workerHandler.post(() -> {
                    aggregator.removeListener(stopListener);
                    Receiver.notifyEvent(aggregator,
                            new VisibilityDoneEvent(visibilityProperties.getIdentifier(), outcome.getError()));
                });
            }
        }
    };
    /**
     * Listens visibility off events.
     */
    private final LinkListener<String> stopListener = new LinkListener<String>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<String> outcome) {
            if (triggered.compareAndSet(false, true)) {
                workerHandler.removeCallbacks(StopVisibilityAfterTimeout.this);
                workerHandler.post(() -> {
                    aggregator.removeListener(startListener);
                    Receiver.notifyEvent(aggregator, new VisibilityDoneEvent(visibilityProperties.getIdentifier()));
                });
            }
        }
    };

    /**
     * Constructor.
     *
     * @param aggregator           listener {@link Aggregator}
     * @param workerHandler        worker handler
     * @param logicTranslator      translates logic commands into physical ones
     * @param visibilityProperties visibility properties
     */
    public StopVisibilityAfterTimeout(@NonNull Aggregator aggregator, @NonNull Handler workerHandler,
                                      @NonNull VisibilityLogicTranslator logicTranslator,
                                      @NonNull VisibilityProperties visibilityProperties) {
        this.aggregator = aggregator;
        this.workerHandler = workerHandler;
        this.logicTranslator = logicTranslator;
        this.visibilityProperties = visibilityProperties;
        this.triggered = new AtomicBoolean(false);

        aggregator.listenOnce(startListener, LinkEvent.LINK_VISIBILITY_ON, visibilityProperties.getIdentifier(), true);
        aggregator.listenOnce(stopListener, LinkEvent.LINK_VISIBILITY_OFF, visibilityProperties.getIdentifier(), true);
    }

    @Override
    public void run() {
        aggregator.removeListener(startListener);
        if (visibilityProperties.stopAfterTimeoutExpiration() &&
                logicTranslator.isVisibilityOn(visibilityProperties.getIdentifier())) {
            logicTranslator.turnVisibilityOff(visibilityProperties.getIdentifier());

        } else if (triggered.compareAndSet(false, true)) {
            aggregator.removeListener(stopListener);
            Receiver.notifyEvent(aggregator, new VisibilityDoneEvent(visibilityProperties.getIdentifier()));
        }
    }
}
