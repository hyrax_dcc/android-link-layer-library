/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.sync;

import android.support.annotation.NonNull;
import android.util.Log;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.Device;
import org.hyrax.link.misc.Filter;
import org.hyrax.link.misc.event.DiscoveryDoneEvent;
import org.hyrax.link.misc.event.DiscoveryOnEvent;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.logic.translator.DiscoveryLogicTranslator;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;

/**
 * Logic device discovery enable. More precisely, to discover the nearby devices.
 * <br/>
 * This class is an abstraction to enable the device discovery and it can be used by multiple
 * technologies.
 */
public class DiscoveryStartSync extends LogicControllerSync<Collection<Device>> {
    @NonNull
    private final DiscoveryLogicTranslator logicTranslator;
    @NonNull
    private final DiscoveryProperties properties;
    @NonNull
    private final ConcurrentHashMap<Device, Device> discoveredDevices;
    @NonNull
    private final String scanId;

    //private volatile boolean doneWithError;
    /**
     * Listens progress listener events.
     */
    private final LinkListener<Collection<Device>> progressListener = new LinkListener<Collection<Device>>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<Collection<Device>> outcome) {
            outcome.ifSuccess(devices -> {
                //if the link has a link layer type filter
                if (properties.getScannerFilter() instanceof Filter) {
                    Filter<Device> filter = (Filter<Device>) properties.getScannerFilter();
                    for (Device device : outcome.getOutcome())
                        if (filter.apply(device)) addToDiscovered(device);
                } else {
                    for (Device device : outcome.getOutcome())
                        addToDiscovered(device);
                }
                //disables the discovery process when all the rules have been met
                if (properties.getStopRule().stop(discoveredDevices.keySet()))
                    continueLogic();
            });
        }

        /**
         * Adds a device to the discovered collection.
         * @param device discovered device
         */
        private void addToDiscovered(@NonNull Device device) {
            Device res = discoveredDevices.putIfAbsent(device, device);
            if (null != res)
                res.mergeScanData(device.getScanData());
        }
    };
    /**
     * Listens discovery off events.
     */
    private final LinkListener<Boolean> stopListener = (eventType, outcome) -> continueLogic();

    /**
     * Constructor of class {@link DiscoveryStartSync}.
     *
     * @param logicTranslator translates discovery logic commands into physical ones
     * @param properties      discovery properties
     */
    public DiscoveryStartSync(@NonNull DiscoveryLogicTranslator logicTranslator,
                              @NonNull DiscoveryProperties properties) {
        super();

        this.logicTranslator = logicTranslator;
        this.properties = properties;
        this.discoveredDevices = new ConcurrentHashMap<>();
        this.scanId = properties.getIdentifier();
    }

    @Override
    protected void onStart() throws LinkException {
        aggregator.listenOnce(progressListener, LinkEvent.LINK_DISCOVERY_FOUND, scanId, true);
        aggregator.listenOnce(stopListener, LinkEvent.LINK_DISCOVERY_OFF, scanId, true);

        DiscoveryOn discovery = new DiscoveryOn(logicTranslator, properties);
        try {
            discovery.setAggregator(aggregator);
            Outcome<String> res = discovery.call();
            if (null == res)
                throw LinkException.build(Reason.INTERNAL_ERROR, "Start with error. Outcome NULL");

            if (res.isSuccessful()) continueLogic();
            else throw res.getError();

        } catch (Exception e) {
            if (e instanceof LinkException)
                throw LinkException.build(((LinkException) e).getReason(), e.getMessage());
            throw LinkException.build(e);
        }
    }

    @Override
    protected void onAfterStart() throws LinkException {
        try {
            waitPeriod(properties.getTimeout());
        } catch (InterruptedException e) {
            throw LinkException.build(e);
        }

        final Event<Collection<Device>> event =
                new DiscoveryDoneEvent(scanId, discoveredDevices.values());

        if (properties.stopAfterTimeoutExpiration() && logicTranslator.isDiscoveryOn(scanId)) {
            logicTranslator.turnDiscoveryOff(scanId);
            try {
                waitPeriodAndThrow(Constants.DEFAULT_LOGIC_TIMEOUT);
            } catch (@NonNull TimeoutException | InterruptedException e) {
                Log.w("Discovery", "Time has expired");
            }
        }
        publishDoneEvent(event);
    }

    /**
     * Publish the discovery done event.
     *
     * @param event event to be published
     */
    private void publishDoneEvent(@NonNull Event<Collection<Device>> event) {
        publishEvent(event);
        notifyEventResult(event.getOutcome());
    }

    @NonNull
    @Override
    protected Event<Collection<Device>> onError(@NonNull LinkException error) {
        return new DiscoveryDoneEvent(scanId, error);
    }

    @Override
    protected void onFinish() {
        aggregator.removeListener(progressListener);
        aggregator.removeListener(stopListener);
    }

    /**
     * Logic device discovery enable.
     * <br/>
     * This class is an abstraction to enable the device discovery and it can be used by multiple technologies.
     */
    private class DiscoveryOn extends LogicControllerSync<String> {
        @NonNull
        private final DiscoveryLogicTranslator logicTranslator;
        @NonNull
        private final DiscoveryProperties discoveryProperties;
        /**
         * Listen for discovery on event
         */
        private final LinkListener<String> startListener = (eventType, outcome) -> notifyEventResult(outcome);

        /**
         * Constructor of class {@link DiscoveryOn}.
         *
         * @param logicTranslator translates logic commands into physical ones
         */
        DiscoveryOn(@NonNull DiscoveryLogicTranslator logicTranslator, @NonNull DiscoveryProperties properties) {
            super();

            this.logicTranslator = logicTranslator;
            this.discoveryProperties = properties;
        }

        @Override
        protected void onStart() {
            aggregator.listenOnce(startListener, LinkEvent.LINK_DISCOVERY_ON, scanId, true);

            logicTranslator.turnDiscoveryOn(discoveryProperties);
        }

        @NonNull
        @Override
        protected Event<String> onError(@NonNull LinkException error) {
            return new DiscoveryOnEvent(scanId, error);
        }

        @Override
        protected void onFinish() {
            aggregator.removeListener(startListener);
        }
    }
}
