/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.translator;

/**
 * Interface to translate logic commands to physical ones, by managing the state of hardware.
 * <br/>
 * Change the state of hardware is the ability to enable and disable it.
 * <p>
 * Created on 14/10/16.
 */

public interface HardwareLogicTranslator {

    /**
     * Turns the hardware on.
     */
    void turnHardwareOn();

    /**
     * Turns the hardware off.
     */
    void turnHardwareOff();

    /**
     * Verifies if the hardware is active.
     *
     * @return <tt>true</tt> if active, <tt>false</tt> otherwise
     */
    boolean isHardwareOn();

    /**
     * Cleans whatever needs to be cleansed
     */
    void detach();
}
