/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.async;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.event.HardwareOnEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.logic.translator.HardwareLogicTranslator;

/**
 * Logic hardware enable.
 * <br/>
 * This class is an abstraction to enable the hardware and it can be used by multiple technologies.
 */
public class HardwareStartAsync extends LogicControllerAsync<Boolean> {
    private final HardwareLogicTranslator hardwareTranslator;

    /**
     * Constructor.
     *
     * @param hardwareTranslator translates logic commands into physical ones
     */
    public HardwareStartAsync(@NonNull HardwareLogicTranslator hardwareTranslator) {
        super(LinkEvent.LINK_HARDWARE_ON, Aggregator.NO_FILTER, Constants.DEFAULT_HARDWARE_ENABLE_TIMEOUT);
        this.hardwareTranslator = hardwareTranslator;
    }

    @Override
    void onStart() {
        if (hardwareTranslator.isHardwareOn()) {
            publishEvent(new HardwareOnEvent());
        } else {
            hardwareTranslator.turnHardwareOn();
        }
    }

    @NonNull
    @Override
    Event<Boolean> onError(@NonNull LinkException error) {
        return new HardwareOnEvent(error);
    }
}

