/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.logic.translator;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.Device;

import java.util.Collection;

/**
 * Interface that translates logic commands to physical ones, by managing the state of hardware
 * connection.
 * <br/>
 * After a discovery step a connection process is usually needed in order be physically connected
 * to the remote device.
 * <p>
 * Created on 14/10/16.
 */

public interface ConnectionLogicTranslator<S> {

    /**
     * Attempts to establish a physical link to remote {@link Device}.
     *
     * @param remoteDevice remote object device representation
     * @param properties   connection properties arguments
     */
    void turnConnectionOn(@NonNull Device remoteDevice, @NonNull ConnectionProperties<S> properties);

    /**
     * Attempt to close the physical link from remote {@link Device}.
     *
     * @param remoteDevice remote object device representation
     */
    void turnConnectionOff(@NonNull Device remoteDevice);

    /**
     * Verifies if a connection to a remote {@link Device} is active.
     *
     * @param remoteDevice remote object device representation
     * @return <tt>true</tt> if connected, <tt>false</tt> otherwise
     */
    boolean isConnectionOn(@NonNull Device remoteDevice);

    /**
     * Returns a collection of active connections to remote devices.
     *
     * @return active connections
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/Collection.html" target="_blank">Collection</a>
     */
    @NonNull
    Collection<Device> getActiveConnections();

    /**
     * Cleans whatever needs to be cleansed
     */
    void detach();
}
