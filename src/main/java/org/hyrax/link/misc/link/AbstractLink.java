/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.link;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.LinkDetachEvent;
import org.hyrax.link.misc.logic.async.ConnectionServerStartAsync;
import org.hyrax.link.misc.logic.async.LogicControllerAsync;
import org.hyrax.link.misc.outcome.Error;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.Device;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkAsync;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.LinkPromise;
import org.hyrax.link.LinkSync;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.LinkLayerRuntimeException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.async.ConnectionServerStopAsync;
import org.hyrax.link.misc.logic.async.ConnectionStartAsync;
import org.hyrax.link.misc.logic.async.ConnectionStopAsync;
import org.hyrax.link.misc.logic.async.DiscoveryStartAsync;
import org.hyrax.link.misc.logic.async.DiscoveryStopAsync;
import org.hyrax.link.misc.logic.async.HardwareStartAsync;
import org.hyrax.link.misc.logic.async.HardwareStopAsync;
import org.hyrax.link.misc.logic.async.VisibilityStartAsync;
import org.hyrax.link.misc.logic.async.VisibilityStopAsync;
import org.hyrax.link.misc.logic.sync.ConnectionServerStartSync;
import org.hyrax.link.misc.logic.sync.ConnectionServerStopSync;
import org.hyrax.link.misc.logic.sync.ConnectionStartSync;
import org.hyrax.link.misc.logic.sync.ConnectionStopSync;
import org.hyrax.link.misc.logic.sync.DiscoveryStartSync;
import org.hyrax.link.misc.logic.sync.DiscoveryStopSync;
import org.hyrax.link.misc.logic.sync.HardwareStartSync;
import org.hyrax.link.misc.logic.sync.HardwareStopSync;
import org.hyrax.link.misc.logic.sync.LogicControllerSync;
import org.hyrax.link.misc.logic.sync.VisibilityStartSync;
import org.hyrax.link.misc.logic.sync.VisibilityStopSync;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;

import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Abstract class the represents a generic control of a technology.
 */
public abstract class AbstractLink<F extends LinkFeatures> {
    @NonNull
    protected final Aggregator aggregator;
    @NonNull
    private final AtomicBoolean attached;
    @NonNull
    private final Technology technology;
    protected LinkContext linkContext;
    protected Handler workerHandler;
    /**
     * Logic translators object
     */
    protected Translators translators;

    @Nullable
    private Sync syncApi;
    @Nullable
    private Async asyncApi;
    @Nullable
    private AsyncPromise asyncPromiseApi;

    /**
     * Constructor.
     *
     * @param technology defined technology
     */
    protected AbstractLink(@NonNull Technology technology) {
        this.technology = technology;
        this.attached = new AtomicBoolean(false);
        this.aggregator = new Aggregator(attached);
    }

    /**
     * Changes the link context. The change can only happen once.
     *
     * @param linkContext link context object
     */
    public void setLinkContext(@NonNull LinkContext linkContext) {
        if (this.linkContext != null)
            throw new LinkLayerRuntimeException("Link context already set");
        this.linkContext = linkContext;
    }

    /**
     * Changes the worker handler. The change can only happen once.
     *
     * @param workerHandler worker handler object
     */
    public void setWorkerHandler(@NonNull Handler workerHandler) {
        if (this.workerHandler != null)
            throw new LinkLayerRuntimeException("Worker handler already set");
        this.workerHandler = workerHandler;
    }

    /**
     * Initializes the translators class.
     *
     * @return translators object
     */
    @NonNull
    protected abstract Translators initTranslators();

    /**
     * This method will be triggered right after the method 'attach' is called.
     */
    protected abstract void onAttach();

    /**
     * Attaches the link. Initialization
     */
    public void attach() {
        if (attached.compareAndSet(false, true)) {
            if (linkContext == null)
                throw new LinkLayerRuntimeException("Link context is NULL");
            if (workerHandler == null)
                throw new LinkLayerRuntimeException("Worker handler is NULL");

            this.translators = initTranslators();
            linkContext.registerReceiver(translators.receiver,
                    translators.receiver.getIntentFilters(), null, workerHandler);
            onAttach();
            return;
        }
        throw new LinkLayerRuntimeException("Link already attached");
    }

    /**
     * Method triggered when 'detach' called.
     */
    protected abstract void onDetach();

    /**
     * Return the link extra features
     *
     * @return link features object
     */
    protected abstract F getFeatures();

    /**
     * Detaches the link
     */
    public void detach() {
        if (attached.compareAndSet(true, false)) {
            Receiver.notifyEvent(aggregator, new LinkDetachEvent());
            linkContext.unregisterReceiver(translators.receiver);
            aggregator.clear();

            translators.hardwareLogicTranslator.detach();
            translators.discoveryLogicTranslator.detach();
            translators.visibilityLogicTranslator.detach();
            translators.connectionServerLogicTranslator.detach();
            translators.connectionLogicTranslator.detach();

            onDetach();
        }
    }

    /**
     * Returns the link synchronous api.
     *
     * @return synchronous api object
     */
    public LinkSync<F> getSyncApi() {
        if (syncApi == null)
            syncApi = new Sync();
        return syncApi;
    }

    /**
     * Returns the link asynchronous api.
     *
     * @return asynchronous api object
     */
    public LinkAsync<F> getAsyncApi() {
        if (asyncApi == null)
            asyncApi = new Async();
        return asyncApi;
    }

    /**
     * Returns the link asynchronous promise api.
     *
     * @return asynchronous promise api object
     */
    public LinkPromise<F> getPromiseApi() {
        if (asyncPromiseApi == null)
            asyncPromiseApi = new AsyncPromise();
        return asyncPromiseApi;
    }

    /**
     * Verifies if the current link is attached.
     *
     * @return <tt>true</tt> if attached, <tt>false</tt> otherwise
     */
    public boolean isAttached() {
        return attached.get();
    }

    /**
     * Class that defined the synchronous behaviour.
     */
    private class Sync extends LinkImpl implements LinkSync<F> {

        @NonNull
        @Override
        public Outcome<Boolean> enable() {
            return executeTaskSync(
                    new HardwareStartSync(translators.hardwareLogicTranslator),
                    false);
        }

        @NonNull
        @Override
        public Outcome<Boolean> disable() {
            return executeTaskSync(
                    new HardwareStopSync(translators.hardwareLogicTranslator),
                    false);
        }

        @NonNull
        @Override
        public Outcome<Collection<Device>> discover(@NonNull DiscoveryProperties properties) {
            return executeTaskSync(
                    new DiscoveryStartSync(translators.discoveryLogicTranslator, properties),
                    new HashSet<>()
            );
        }

        @NonNull
        @Override
        public Outcome<String> cancelDiscover(@NonNull String scannerId) {
            return executeTaskSync(
                    new DiscoveryStopSync(translators.discoveryLogicTranslator, scannerId),
                    scannerId
            );
        }

        @NonNull
        @Override
        public Outcome<String> setVisible(@NonNull VisibilityProperties properties) {
            return executeTaskSync(
                    new VisibilityStartSync(workerHandler, translators.visibilityLogicTranslator, properties),
                    properties.getIdentifier()
            );
        }

        @NonNull
        @Override
        public Outcome<String> cancelVisible(@NonNull String advertiserId) {
            return executeTaskSync(
                    new VisibilityStopSync(translators.visibilityLogicTranslator, advertiserId),
                    advertiserId);
        }

        @NonNull
        @Override
        public Outcome<Device> connect(@NonNull Device device, @NonNull ConnectionProperties properties) {
            return executeTaskSync(
                    new ConnectionStartSync(translators.connectionLogicTranslator, device, properties),
                    device
            );
        }

        @NonNull
        @Override
        public Outcome<Device> disconnect(@NonNull Device device) {
            return executeTaskSync(
                    new ConnectionStopSync(translators.connectionLogicTranslator, device),
                    device
            );
        }

        @NonNull
        @Override
        public Outcome<String> accept(@NonNull ServerProperties properties) {
            return executeTaskSync(
                    new ConnectionServerStartSync(translators.connectionServerLogicTranslator, properties),
                    properties.getIdentifier()
            );
        }

        @NonNull
        @Override
        public Outcome<String> deny(@NonNull String serverId) {
            return executeTaskSync(
                    new ConnectionServerStopSync(translators.connectionServerLogicTranslator, serverId),
                    serverId
            );
        }

        /**
         * Executes an sync task.
         *
         * @param syncTask     synchronous controller logic object
         * @param defaultValue default value in case of error
         * @return result object
         */
        private <R> Outcome<R> executeTaskSync(@NonNull LogicControllerSync<R> syncTask,
                                               @NonNull R defaultValue) {
            if (attached.get()) {
                try {
                    syncTask.setAggregator(aggregator);
                    Outcome<R> outcome = syncTask.call();
                    return (outcome == null)
                            ?
                            new Error<>(LinkException.build(Reason.NULL_POINTER,
                                    "Outcome result is NULL"), defaultValue)
                            :
                            outcome;

                } catch (Exception e) {
                    return new Error<>(LinkException.build(e), defaultValue);
                }
            }
            return new Error<>(LinkException.build(Reason.DETACHED, "Already detached link: " +
                    getTechnology()), defaultValue);
        }
    }

    /**
     * Class that defined the asynchronous behaviour.
     */
    private class Async extends LinkImpl implements LinkAsync<F> {

        @Override
        public void enable(@NonNull LinkListener<Boolean> listener) {
            executeTaskAsync(
                    new HardwareStartAsync(translators.hardwareLogicTranslator),
                    listener,
                    LinkEvent.LINK_HARDWARE_ON,
                    Aggregator.NO_FILTER,
                    false);
        }

        @Override
        public void disable(@NonNull LinkListener<Boolean> listener) {
            executeTaskAsync(
                    new HardwareStopAsync(translators.hardwareLogicTranslator),
                    listener,
                    LinkEvent.LINK_HARDWARE_OFF,
                    Aggregator.NO_FILTER,
                    false);
        }

        @Override
        public void discover(@NonNull DiscoveryProperties properties,
                             @NonNull LinkListener<Collection<Device>> doneListener) {
            executeTaskAsync(
                    new DiscoveryStartAsync(translators.discoveryLogicTranslator, properties),
                    doneListener,
                    LinkEvent.LINK_DISCOVERY_DONE,
                    properties.getIdentifier(),
                    new HashSet<>());
        }

        @Override
        public void discover(@NonNull DiscoveryProperties properties,
                             @NonNull LinkListener<Collection<Device>> doneListener,
                             @NonNull LinkListener<Collection<Device>> progressListener) {
            aggregator.listenOnce(
                    progressListener,
                    LinkEvent.LINK_DISCOVERY_FOUND,
                    properties.getIdentifier(),
                    false);

            executeTaskAsync(
                    new DiscoveryStartAsync(translators.discoveryLogicTranslator, properties),
                    doneListener,
                    LinkEvent.LINK_DISCOVERY_DONE,
                    properties.getIdentifier(),
                    new HashSet<>());
        }

        @Override
        public void cancelDiscover(@NonNull String scannerId,
                                   @NonNull LinkListener<String> listenerStop) {
            executeTaskAsync(
                    new DiscoveryStopAsync(translators.discoveryLogicTranslator, scannerId),
                    listenerStop,
                    LinkEvent.LINK_DISCOVERY_OFF,
                    scannerId,
                    scannerId);
        }

        @Override
        public void setVisible(@NonNull VisibilityProperties properties,
                               @NonNull LinkListener<String> startListener) {
            executeTaskAsync(
                    new VisibilityStartAsync(translators.visibilityLogicTranslator, properties),
                    startListener,
                    LinkEvent.LINK_VISIBILITY_ON,
                    properties.getIdentifier(),
                    properties.getIdentifier());
        }

        @Override
        public void setVisible(@NonNull VisibilityProperties properties,
                               @NonNull LinkListener<String> startListener,
                               @NonNull LinkListener<String> doneListener) {
            aggregator.listenOnce(
                    doneListener,
                    LinkEvent.LINK_VISIBILITY_DONE,
                    properties.getIdentifier(),
                    false);

            executeTaskAsync(
                    new VisibilityStartAsync(translators.visibilityLogicTranslator, properties),
                    startListener,
                    LinkEvent.LINK_VISIBILITY_ON,
                    properties.getIdentifier(),
                    properties.getIdentifier());
        }

        @Override
        public void cancelVisible(@NonNull String advertiserId,
                                  @NonNull LinkListener<String> listener) {
            executeTaskAsync(
                    new VisibilityStopAsync(translators.visibilityLogicTranslator, advertiserId),
                    listener,
                    LinkEvent.LINK_VISIBILITY_OFF,
                    advertiserId,
                    advertiserId);
        }

        @Override
        public void connect(@NonNull Device device, @NonNull ConnectionProperties properties,
                            @NonNull LinkListener<Device> listener) {
            executeTaskAsync(
                    new ConnectionStartAsync(translators.connectionLogicTranslator, device, properties),
                    listener,
                    LinkEvent.LINK_CONNECTION_NEW,
                    device,
                    device);
        }

        @Override
        public void disconnect(@NonNull Device device, @NonNull LinkListener<Device> listener) {
            executeTaskAsync(
                    new ConnectionStopAsync(translators.connectionLogicTranslator, device),
                    listener,
                    LinkEvent.LINK_CONNECTION_LOST,
                    device,
                    device);
        }

        @Override
        public void accept(@NonNull ServerProperties properties,
                           @NonNull LinkListener<String> listener) {
            executeTaskAsync(
                    new ConnectionServerStartAsync(translators.connectionServerLogicTranslator, properties),
                    listener,
                    LinkEvent.LINK_CONNECTION_SERVER_ON,
                    properties.getIdentifier(),
                    properties.getIdentifier());
        }

        @Override
        public void deny(@NonNull String serverId, @NonNull LinkListener<String> listener) {
            executeTaskAsync(
                    new ConnectionServerStopAsync(translators.connectionServerLogicTranslator, serverId),
                    listener,
                    LinkEvent.LINK_CONNECTION_SERVER_OFF,
                    serverId,
                    serverId);
        }

        /**
         * Executes an async task.
         *
         * @param asyncTask    asynchronous logic task
         * @param listener     event listener
         * @param event        event type
         * @param filter       object filter
         * @param defaultValue default value in case of error
         */
        private <R> void executeTaskAsync(@NonNull LogicControllerAsync asyncTask,
                                          @NonNull LinkListener<R> listener, @NonNull LinkEvent event,
                                          @NonNull Object filter, @NonNull R defaultValue) {
            if (attached.get()) {
                aggregator.listenOnce(listener, event, filter, false);
                asyncTask.setAggregator(aggregator);
                asyncTask.setWorkerHandler(workerHandler);

                asyncTask.perform();

                return;
            }
            listener.onEvent(event, new Error<>(LinkException.build(Reason.DETACHED,
                    "Link already detached"), defaultValue));
        }
    }

    private class AsyncPromise extends LinkImpl implements LinkPromise<F> {

        @Override
        public Promise<Boolean, LinkException, Void> enable() {
            final DeferredObject<Boolean, LinkException, Void> defer = new DeferredObject<>();

            getAsyncApi().enable((eventType, outcome) -> processOutcome(defer, outcome));
            return defer.promise();
        }

        @Override
        public Promise<Boolean, LinkException, Void> disable() {
            final DeferredObject<Boolean, LinkException, Void> defer = new DeferredObject<>();

            getAsyncApi().disable(((eventType, outcome) -> processOutcome(defer, outcome)));
            return defer.promise();
        }

        @Override
        public Promise<Collection<Device>, LinkException, Collection<Device>> discover(@NonNull DiscoveryProperties properties) {
            final DeferredObject<Collection<Device>, LinkException, Collection<Device>> defer =
                    new DeferredObject<>();

            getAsyncApi().discover(properties,
                    (eventType, outcome) -> processOutcome(defer, outcome),
                    (eventType, outcome) -> notifyOutcome(defer, outcome));
            return defer.promise();
        }

        @Override
        public Promise<String, LinkException, Void> cancelDiscover(@NonNull String scannerId) {
            final DeferredObject<String, LinkException, Void> defer = new DeferredObject<>();

            getAsyncApi().cancelDiscover(scannerId,
                    (eventType, outcome) -> processOutcome(defer, outcome));
            return defer.promise();
        }

        @Override
        public Promise<String, LinkException, String> setVisible(@NonNull VisibilityProperties properties,
                                                                 boolean resolveWhenStart) {
            final DeferredObject<String, LinkException, String> defer = new DeferredObject<>();
            if (resolveWhenStart) {
                getAsyncApi().setVisible(properties,
                        ((eventType, outcome) -> processOutcome(defer, outcome)));
            } else {
                getAsyncApi().setVisible(properties,
                        (eventType, outcome) -> notifyOutcome(defer, outcome),
                        (eventType, outcome) -> processOutcome(defer, outcome));
            }
            return defer.promise();
        }

        @Override
        public Promise<String, LinkException, Void> cancelVisible(@NonNull String advertiserId) {
            final DeferredObject<String, LinkException, Void> defer = new DeferredObject<>();

            getAsyncApi().cancelVisible(advertiserId,
                    (eventType, outcome) -> processOutcome(defer, outcome));
            return defer.promise();
        }

        @Override
        public Promise<Device, LinkException, Void> connect(@NonNull Device device,
                                                            @NonNull ConnectionProperties properties) {
            final DeferredObject<Device, LinkException, Void> defer = new DeferredObject<>();

            getAsyncApi().connect(device, properties,
                    (eventType, outcome) -> processOutcome(defer, outcome));
            return defer.promise();
        }

        @Override
        public Promise<Device, LinkException, Void> disconnect(@NonNull Device device) {
            final DeferredObject<Device, LinkException, Void> defer = new DeferredObject<>();

            getAsyncApi().disconnect(device,
                    (eventType, outcome) -> processOutcome(defer, outcome));
            return defer.promise();
        }

        @Override
        public Promise<String, LinkException, Void> accept(@NonNull ServerProperties properties) {
            final DeferredObject<String, LinkException, Void> defer = new DeferredObject<>();

            getAsyncApi().accept(properties,
                    (eventType, outcome) -> processOutcome(defer, outcome));
            return defer.promise();
        }

        @Override
        public Promise<String, LinkException, Void> deny(@NonNull String serverId) {
            final DeferredObject<String, LinkException, Void> defer = new DeferredObject<>();

            getAsyncApi().deny(serverId,
                    (eventType, outcome) -> processOutcome(defer, outcome));
            return defer.promise();
        }

        /**
         * Process the promise outcome by verifying if it's an error success.
         *
         * @param defer   deferred object
         * @param outcome link outcome object
         * @param <R>     type used for the promise resolve. When the operation it's done.
         */
        private <R> void processOutcome(@NonNull DeferredObject<R, LinkException, ?> defer,
                                        @NonNull Outcome<R> outcome) {
            if (outcome.isSuccessful())
                defer.resolve(outcome.getOutcome());
            else
                defer.reject(outcome.getError());
        }

        /**
         * Notifies the progress of an link action.
         *
         * @param defer   deferred object
         * @param outcome link outcome object
         * @param <P>     type used for the promise notify. Operation progress.
         */
        private <P> void notifyOutcome(@NonNull DeferredObject<?, LinkException, P> defer,
                                       @NonNull Outcome<P> outcome) {
            if (outcome.isSuccessful() && defer.isPending())
                defer.notify(outcome.getOutcome());
        }
    }

    /**
     * Interface link implementation
     */
    class LinkImpl implements Link<F> {
        @Override
        public boolean isEnabled() {
            return translators.hardwareLogicTranslator.isHardwareOn();
        }

        @Override
        public boolean isDiscovering(@NonNull String scannerId) {
            return translators.discoveryLogicTranslator.isDiscoveryOn(scannerId);
        }

        @NonNull
        @Override
        public Collection<String> getScanners() {
            return translators.discoveryLogicTranslator.getScanners();
        }

        @Override
        public boolean isVisible(@NonNull String advertiserId) {
            return translators.visibilityLogicTranslator.isVisibilityOn(advertiserId);
        }

        @NonNull
        @Override
        public Collection<String> getAdvertisers() {
            return translators.visibilityLogicTranslator.getAdvertisers();
        }

        @Override
        public boolean isConnected(@NonNull Device device) {
            return translators.connectionLogicTranslator.isConnectionOn(device);
        }

        @NonNull
        @Override
        public Collection<Device> getConnected() {
            return translators.connectionLogicTranslator.getActiveConnections();
        }

        @Override
        public boolean isAccepting(@NonNull String serverId) {
            return translators.connectionServerLogicTranslator.isServerOn(serverId);
        }

        @NonNull
        @Override
        public Collection<String> getAccepted() {
            return translators.connectionServerLogicTranslator.getActiveServers();
        }

        @Override
        public <R> void listen(@NonNull LinkListener<R> listener) {
            aggregator.listen(listener);
        }

        @Override
        public <R> void listen(@NonNull EventCategory category, @NonNull LinkListener<R> listener) {
            aggregator.listen(listener, category);
        }

        @Override
        public <R> void listen(@NonNull LinkEvent linkEvent, @NonNull LinkListener<R> listener) {
            aggregator.listen(listener, linkEvent);
        }

        @Override
        public <R> void listen(@NonNull LinkEvent linkEvent, @NonNull Object filter,
                               @NonNull LinkListener<R> listener) {
            aggregator.listen(listener, linkEvent, filter, false);
        }

        @Override
        public <R> void listenOnce(@NonNull LinkEvent linkEvent, @NonNull Object filter,
                                   @NonNull LinkListener<R> listener) {
            aggregator.listenOnce(listener, linkEvent, filter, false);
        }

        @Override
        public void removeListener(@NonNull LinkListener listener) {
            aggregator.removeListener(listener);
        }

        @NonNull
        @Override
        public Technology getTechnology() {
            return technology;
        }

        @Override
        public boolean isSupported() {
            return linkContext.isSupported(getTechnology());
        }

        @Override
        @NonNull
        public F getFeatures() {
            return AbstractLink.this.getFeatures();
        }
    }
}

