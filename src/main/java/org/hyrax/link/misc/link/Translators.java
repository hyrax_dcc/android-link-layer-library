/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.link;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.logic.translator.ConnectionLogicTranslator;
import org.hyrax.link.misc.logic.translator.ConnectionServerLogicTranslator;
import org.hyrax.link.misc.logic.translator.DiscoveryLogicTranslator;
import org.hyrax.link.misc.logic.translator.HardwareLogicTranslator;
import org.hyrax.link.misc.logic.translator.VisibilityLogicTranslator;

/**
 * Translators object.
 */
public final class Translators {
    @NonNull
    final Receiver receiver;
    @NonNull
    final HardwareLogicTranslator hardwareLogicTranslator;
    @NonNull
    final DiscoveryLogicTranslator discoveryLogicTranslator;
    @NonNull
    final VisibilityLogicTranslator visibilityLogicTranslator;
    @NonNull
    public final ConnectionServerLogicTranslator connectionServerLogicTranslator;
    @NonNull
    public final ConnectionLogicTranslator connectionLogicTranslator;

    /**
     * Constructor.
     *
     * @param receiver                        an android broadcast receiver object
     * @param hardwareLogicTranslator         hardware translator
     * @param discoveryLogicTranslator        discovery translator
     * @param visibilityLogicTranslator       visibility translator
     * @param connectionServerLogicTranslator connection server translator
     * @param connectionLogicTranslator       connection translator
     */
    public Translators(@NonNull Receiver receiver,
                       @NonNull HardwareLogicTranslator hardwareLogicTranslator,
                       @NonNull DiscoveryLogicTranslator discoveryLogicTranslator,
                       @NonNull VisibilityLogicTranslator visibilityLogicTranslator,
                       @NonNull ConnectionServerLogicTranslator connectionServerLogicTranslator,
                       @NonNull ConnectionLogicTranslator connectionLogicTranslator) {
        this.receiver = receiver;
        this.hardwareLogicTranslator = hardwareLogicTranslator;
        this.discoveryLogicTranslator = discoveryLogicTranslator;
        this.visibilityLogicTranslator = visibilityLogicTranslator;
        this.connectionServerLogicTranslator = connectionServerLogicTranslator;
        this.connectionLogicTranslator = connectionLogicTranslator;
    }
}