/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.link;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.Device;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;

/**
 * Interface that allows the links to defined extra action/configuration features.
 */
public interface LinkFeatures {
    /**
     * Returns a discovery builder object.
     *
     * @return discovery properties builder
     */
    DiscoveryProperties.Builder<?, ?> newDiscoveryBuilder();

    /**
     * Returns a visibility builder object.
     *
     * @return visibility properties builder
     */
    VisibilityProperties.Builder<?, ?> newVisibilityBuilder();

    /**
     * Returns a connection builder object.
     *
     * @param device device object to connect to
     * @return connection properties builder
     */
    ConnectionProperties.Builder<?> newConnectionBuilder(@NonNull Device device);

    /**
     * Returns a server builder object
     *
     * @return server properties builder
     */
    ServerProperties.Builder<?> newServerBuilder();
}
