/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.link;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.Technology;

import java.util.Collection;

/**
 * This interface is responsible for querying the links about the current state.
 * <p>
 * //@param <F> type of feature object
 */
public interface Link<F extends LinkFeatures> {

    /**
     * Checks if the hardware is enabled.
     *
     * @return <tt>true</tt> if enabled, <tt>false</tt> otherwise
     */
    boolean isEnabled();

    /**
     * Checks if a specific scanner is active.
     *
     * @param scannerId scanner identifier
     * @return <tt>true</tt> if active, <tt>false</tt> otherwise
     */
    boolean isDiscovering(@NonNull String scannerId);

    /**
     * Returns a collection of active scanners.
     *
     * @return a ready-only collection of scanners identifiers.
     */
    @NonNull
    Collection<String> getScanners();

    /**
     * Verifies if the advertiser is active.
     *
     * @param advertiserId advertiser identifier
     * @return {@link Outcome} object result.
     */
    boolean isVisible(@NonNull String advertiserId);

    /**
     * Returns a collection of active advertisers.
     *
     * @return a ready-only collection of advertisers identifiers.
     */
    @NonNull
    Collection<String> getAdvertisers();

    /**
     * Checks if client device is connected.
     *
     * @param device remove device
     * @return <tt>true</tt> if connected, <tt>false</tt> otherwise
     */
    boolean isConnected(@NonNull Device device);

    /**
     * Returns a collection of connected devices.
     *
     * @return a ready-only collection of {@code Device}
     */
    @NonNull
    Collection<Device> getConnected();

    /**
     * Verifies of the server connection is active.
     *
     * @param serverId server identifier
     * @return <tt>true</tt> if active, <tt>false</tt> otherwise
     */
    boolean isAccepting(@NonNull String serverId);

    /**
     * Returns a collections of active acceptance servers.
     *
     * @return a ready-only collection of server identifiers
     */
    @NonNull
    Collection<String> getAccepted();

    /**
     * Listen for all events produced by the link layer.
     *
     * @param listener link listener object
     * @param <R>      type of listener result
     */
    <R> void listen(@NonNull LinkListener<R> listener);

    /**
     * Listen for all events of the link layer, filtered by {@link EventCategory}.
     *
     * @param category event category to filter
     * @param listener link listener object
     * @param <R>      type of listener result
     */
    <R> void listen(@NonNull EventCategory category, @NonNull LinkListener<R> listener);

    /**
     * Listen for all events of the link layer, filtered by {@link LinkEvent}
     *
     * @param linkEvent event type to filter
     * @param listener  link listener object
     * @param <R>       type of listener result
     */
    <R> void listen(@NonNull LinkEvent linkEvent, @NonNull LinkListener<R> listener);

    /**
     * Listen for all events of the link layer, filtered by {@link LinkEvent}
     *
     * @param linkEvent event type to filter
     * @param filter    extra object filter, e.g an id
     * @param listener  link listener object
     * @param <R>       type of listener result
     */
    <R> void listen(@NonNull LinkEvent linkEvent, @NonNull Object filter, @NonNull LinkListener<R> listener);

    /**
     * Listen just one event of the link layer, filtered by {@link LinkEvent} and an extra object
     *
     * @param linkEvent event type to filter
     * @param filter    extra object filter, e.g an id
     * @param listener  link listener object
     * @param <R>       type of listener result
     */
    <R> void listenOnce(@NonNull LinkEvent linkEvent, @NonNull Object filter, @NonNull LinkListener<R> listener);


    /**
     * Removes a listener from the listener registry.
     *
     * @param listener link listener object
     */
    void removeListener(@NonNull LinkListener listener);

    /**
     * Returns the link technology.
     *
     * @return link technology
     */
    @NonNull
    Technology getTechnology();

    /**
     * Checks if the current device supports the link technology.
     *
     * @return <tt>true</tt> if supports, <tt>false</tt> otherwise
     */
    boolean isSupported();

    /**
     * Returns extra features that a specific link may have.
     *
     * @return specific link object
     */
    @NonNull
    F getFeatures();
}
