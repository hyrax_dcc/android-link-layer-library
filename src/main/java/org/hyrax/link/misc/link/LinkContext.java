/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc.link;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.nsd.NsdManager;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Technology;
import org.hyrax.link.misc.exception.LinkLayerRuntimeException;

import java.util.Objects;

/**
 * Link layer {@link Context} object.
 */
public class LinkContext extends ContextWrapper {
    public LinkContext(@NonNull final Context context) {
        super(context);
    }

    /**
     * Returns the default bluetooth adapter.
     *
     * @return bluetooth adapter object
     */
    @NonNull
    public BluetoothAdapter getBluetoothAdapter() {
        return BluetoothAdapter.getDefaultAdapter();
    }

    /**
     * Returns the bluetooth manager object.
     *
     * @return bluetooth manager object
     */
    @NonNull
    public BluetoothManager getBluetoothManager() {
        BluetoothManager m = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        Objects.requireNonNull(m, "BLUETOOTH_SERVICE is Null");
        return m;
    }

    /**
     * Returns the bluetooth Gatt Server object.
     *
     * @param serverCallback server callback
     * @return bluetooth gatt server object.
     */
    @Nullable
    public BluetoothGattServer getBluetoothGattServer(BluetoothGattServerCallback serverCallback) {
        return getBluetoothManager().openGattServer(this, serverCallback);
    }

    /**
     * Returns the bluetooth le advertiser.
     *
     * @return bluetooth le advertiser
     */
    @Nullable
    @TargetApi(value = 21)
    public BluetoothLeAdvertiser getBluetoothLeAdvertiser() {
        return getBluetoothAdapter().getBluetoothLeAdvertiser();
    }

    /**
     * Returns the bluetooth le scanner.
     *
     * @return bluetooth le scanner
     */
    @Nullable
    @TargetApi(value = 21)
    public BluetoothLeScanner getBluetoothLeScanner() {
        return getBluetoothAdapter().getBluetoothLeScanner();
    }

    /**
     * Returns the wifi manager object
     *
     * @return wifi manager object
     */
    @NonNull
    public WifiManager getWifiManager() {
        WifiManager w = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        Objects.requireNonNull(w, "WIFI_SERVICE is Null");
        return w;
    }

    /**
     * Returns the wifi p2p manager object
     *
     * @return wifi p2p manager object
     */
    @NonNull
    public WifiP2pManager getWifiP2pManager() {
        WifiP2pManager w = (WifiP2pManager) getApplicationContext().getSystemService(Context.WIFI_P2P_SERVICE);
        Objects.requireNonNull(w, "WIFI_P2P_SERVICE is Null");
        return w;
    }

    /**
     * Returns the nsd manager object.
     *
     * @return nsg manger object
     */
    @NonNull
    public NsdManager getNsdManager() {
        NsdManager n = (NsdManager) getApplicationContext().getSystemService(Context.NSD_SERVICE);
        Objects.requireNonNull(n, "NSD_SERVICE is Null");
        return n;
    }

    /**
     * Returns the location manager object.
     *
     * @return location manager object
     */
    @NonNull
    public LocationManager getLocationManager() {
        LocationManager l = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Objects.requireNonNull(l, "LOCATION_SERVICE is Null");
        return l;
    }

    /**
     * Returns the connectivity manager object
     *
     * @return connectivity manager object
     */
    @NonNull
    public ConnectivityManager getConnectivityManager() {
        ConnectivityManager c = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        Objects.requireNonNull(c, "CONNECTIVITY_SERVICE is Null");
        return c;
    }

    /**
     * Verifies if a technology is supported or not.
     *
     * @param technology the technology
     * @return <tt>true</tt> if supported, <tt>false</tt> otherwise
     */
    public boolean isSupported(@NonNull Technology technology) {
        String type;
        switch (technology) {
            case BLUETOOTH:
                type = PackageManager.FEATURE_BLUETOOTH;
                break;

            case BLUETOOTH_LE:
                type = PackageManager.FEATURE_BLUETOOTH_LE;
                break;

            case WIFI:
                type = PackageManager.FEATURE_WIFI;
                break;

            case WIFI_DIRECT:
                type = PackageManager.FEATURE_WIFI_DIRECT;
                break;

            case MOBILE:
                type = PackageManager.FEATURE_TELEPHONY;
                break;

            default:
                throw new LinkLayerRuntimeException("Unknown type " + technology);
        }
        return getPackageManager().hasSystemFeature(type);
    }

    public boolean hasPermission(@NonNull String permission) {
        int result = checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}
