/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc;

import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.misc.exception.LinkLayerRuntimeException;

import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class that aggregates all the listeners.
 * <br/>
 * The listeners are classified by category ({@link EventCategory}), event type ({@link LinkEvent})
 * and by an object key (not mandatory) defined in each event type.
 */
@AnyThread
public class Aggregator {
    public static final Object NO_FILTER = new Object();
    private static final String TAG = "Aggregator";
    private static final EventCategoryWrapper UNDEFINED_CATEGORY = new EventCategoryWrapper();
    @NonNull
    private final AtomicBoolean linkAttached;
    @NonNull
    private final ConcurrentHashMap<LinkListener<?>, LinkEventWrapper> allListenersMap;
    @NonNull
    private final ConcurrentHashMap<EventCategoryWrapper, EventObservable<LinkListener<?>>> observableMap;

    /**
     * Creates a {@link Aggregator} object.
     */
    public Aggregator(@NonNull AtomicBoolean linkAttached) {
        this.linkAttached = linkAttached;
        this.allListenersMap = new ConcurrentHashMap<>();
        this.observableMap = new ConcurrentHashMap<>();
    }

    /**
     * Registers a temporary listener given an {@link LinkEvent}, a filter object and addition priority.
     * <br/>
     * An {@link LinkListener} is automatically removed after being called, except
     * if the event is a progress event, which will be remove automatically, also,
     * after the progress done.
     *
     * @param listener  implemented listener
     * @param linkEvent type of event
     * @param filter    event filter object
     * @param onTop     if <tt>true</tt> adds listener to the top, <tt>false</tt> just appends it.
     */
    public void listenOnce(@NonNull LinkListener<?> listener, @NonNull LinkEvent linkEvent,
                           @NonNull Object filter, boolean onTop) {
        if (linkAttached.get()) {
            LinkEventWrapper wrapper = new LinkEventWrapper(linkEvent, filter, LinkEventWrapper.ONCE_TYPE);
            if (allListenersMap.putIfAbsent(listener, wrapper) == null) {
                observableMap.putIfAbsent(wrapper.categoryWrapper, new EventObservable<>());
                if (onTop)
                    observableMap.get(wrapper.categoryWrapper).addListenerOnTop(listener);
                else
                    observableMap.get(wrapper.categoryWrapper).addListener(listener);
                return;
            }
            throw new LinkLayerRuntimeException("Duplicate entry listener");
        }
        Log.e(TAG, "ListenOnce: Link already detached");
    }

    /**
     * Registers an active listener.
     * <br/>
     * This method will allow to listen all the events of the link layer on a
     * {@link LinkListener} object
     *
     * @param listener implemented listener
     */
    public void listen(@NonNull LinkListener<?> listener) {
        addListener(listener, new LinkEventWrapper(new EventCategoryWrapper()), false);
    }

    /**
     * Registers an active listener, given an {@link EventCategory}.
     * <br/>
     * This method will allow to listen all the events of a certain category on a
     * {@link LinkListener} object
     *
     * @param listener implemented listener
     * @param category the category to be listen
     */
    public void listen(@NonNull LinkListener<?> listener, @NonNull EventCategory category) {
        listen(listener, category, false);
    }

    /**
     * Registers a active listener given an {@link EventCategory} and addition priority.
     * <br/>
     * This method will allow to listen all the events of a certain category first on a
     * {@link LinkListener} object
     *
     * @param listener implemented listener
     * @param category the category to be listen
     * @param onTop    if <tt>true</tt> adds listener to the top, <tt>false</tt> just appends it.
     */
    public void listen(@NonNull LinkListener<?> listener, @NonNull EventCategory category, boolean onTop) {
        addListener(listener, new LinkEventWrapper(new EventCategoryWrapper(category)), onTop);
    }

    /**
     * Registers a active listener given an {@link LinkEvent}.
     * <br/>
     * This method will allow to listen all the events of a certain type on a
     * {@link LinkListener} object
     *
     * @param listener  implemented listener
     * @param eventType type of event to listen
     */
    public void listen(@NonNull LinkListener<?> listener, @NonNull LinkEvent eventType) {
        listen(listener, eventType, NO_FILTER, false);
    }

    /**
     * Registers a active listener given an {@link LinkEvent}, a object filter and addition priority.
     * <br/>
     * This method will allow to listen all the events of a certain category first on a
     * {@link LinkListener} object
     *
     * @param listener   implemented listener
     * @param eventType  type of event to listen
     * @param identifier event filter object
     * @param onTop      if <tt>true</tt> adds listener to the top, <tt>false</tt> just appends it.
     */
    public void listen(@NonNull LinkListener<?> listener, @NonNull LinkEvent eventType,
                       @NonNull Object identifier, boolean onTop) {
        addListener(listener, new LinkEventWrapper(eventType, identifier, LinkEventWrapper.ALWAYS_TYPE), onTop);
    }

    /**
     * Registers a listener by a given link event wrapper.
     *
     * @param listener     implemented listener
     * @param eventWrapper type of event  (wrapper)
     * @param onTop        if <tt>true</tt> adds listener to the top, <tt>false</tt> just appends it.
     */
    private void addListener(@NonNull LinkListener<?> listener, @NonNull LinkEventWrapper eventWrapper, boolean onTop) {
        if (linkAttached.get()) {
            if (allListenersMap.putIfAbsent(listener, eventWrapper) == null) {
                observableMap.putIfAbsent(eventWrapper.categoryWrapper, new EventObservable<>());
                if (onTop)
                    observableMap.get(eventWrapper.categoryWrapper).addListenerOnTop(listener);
                else
                    observableMap.get(eventWrapper.categoryWrapper).addListener(listener);
                return;
            }
            throw new LinkLayerRuntimeException("Duplicate entry listener");
        }
        Log.e(TAG, "addListener: Link already detached");
    }

    /**
     * Removes a specific listener from the {@link Aggregator}.
     *
     * @param listener implemented listener
     */
    public void removeListener(@NonNull LinkListener<?> listener) {
        assert listener != null;
        LinkEventWrapper wrapper = allListenersMap.remove(listener);
        if (wrapper != null) {
            EventObservable<LinkListener<?>> eventObservable = observableMap.get(wrapper.categoryWrapper);
            if (eventObservable != null)
                eventObservable.removeListener(listener);
        }
    }

    /**
     * Clears all the existing listeners.
     */
    public void clear() {
        Log.d(TAG, "Report All " + allListenersMap.size());
        for (LinkEventWrapper wrapper : allListenersMap.values())
            Log.d(TAG, "All Event: " + wrapper);

        for (Map.Entry<EventCategoryWrapper, EventObservable<LinkListener<?>>> entry : observableMap.entrySet())
            entry.getValue().removeAll();
        observableMap.clear();
        allListenersMap.clear();
    }

    /**
     * Notifies all listeners available. It also removes the listeners as they are triggered,
     * excepting the progressive listener that are just removed when the progress finishes.
     *
     * @param linkEvent link event to be triggered
     * @param filter    link event object filter
     * @param outcome   link event outcome result object
     */
    void notifyListeners(@NonNull final LinkEvent linkEvent, @NonNull final Object filter, final @NonNull Outcome outcome) {
        if (linkAttached.get()) {
            //the link discovery done is a special case because it need to remove the associated link discovery found event
            if (LinkEvent.LINK_DISCOVERY_DONE == linkEvent)
                removeDiscoveryFoundEvents(filter);

            final LinkEventWrapper eventWrapperTriggered = new LinkEventWrapper(linkEvent, filter, LinkEventWrapper.NONE_TYPE);
            //get listener that match the triggered event category
            EventObservable<LinkListener<?>> eventObservableCategory = observableMap.get(eventWrapperTriggered.categoryWrapper);
            if (eventObservableCategory != null) {
                eventObservableCategory.notifyListeners(listener -> {
                    LinkEventWrapper savedWrapper = allListenersMap.get(listener);
                    if (savedWrapper != null) {
                        /*
                         * Check if can trigger the event.
                         * Trigger rules:
                         *  1) The link event wrappers must be equals --OR--
                         *  2) The link event type of saved listener is UNDEFINED --OR--
                         *  3) The both link events are equals and the saved listener has a default filter
                         */
                        if (eventWrapperTriggered.equals(savedWrapper) ||
                                savedWrapper.isUndefined() ||
                                (eventWrapperTriggered.linkEvent == savedWrapper.linkEvent && savedWrapper.isDefaultFilter())) {
                            listener.onEvent(linkEvent, outcome);
                        }
                        //if it's a temporary listener (ONCE listener type), then removes it
                        if (LinkEventWrapper.ONCE_TYPE == savedWrapper.listenerType &&
                                linkEvent.isActionDone() &&
                                eventWrapperTriggered.equals(savedWrapper)) {
                            allListenersMap.remove(listener);
                            return true;
                        }
                    }
                    return false;
                });
            }
            //get listener that match the undefined category
            EventObservable<LinkListener<?>> eventObservableCatUndefined = observableMap.get(UNDEFINED_CATEGORY);
            if (eventObservableCatUndefined != null) {
                eventObservableCatUndefined.notifyListeners(listener -> {
                    listener.onEvent(linkEvent, outcome);
                    return false;
                });
            }
            return;
        }
        Log.e(TAG, "notifyTemporaryListeners: Link already detached " + linkAttached.get());
    }

    /**
     * Removes link discovery found events in case of a discovery done event has been triggered.
     * <br/>
     * As the discovery found event is a progress event, it needs to be taken care differently
     *
     * @param filter link event object filter
     */
    private void removeDiscoveryFoundEvents(@NonNull Object filter) {
        final LinkEventWrapper progressWrapper = new LinkEventWrapper(
                LinkEvent.LINK_DISCOVERY_FOUND, filter, LinkEventWrapper.ONCE_TYPE);
        EventObservable<LinkListener<?>> eventObservable = observableMap.get(progressWrapper.categoryWrapper);
        if (eventObservable != null) {
            eventObservable.notifyListeners(listener -> {
                LinkEventWrapper savedWrapper = allListenersMap.get(listener);
                if (savedWrapper != null) {
                    //if listener is type ONCE and the two event wrappers are equals, removes the listener
                    if (LinkEventWrapper.ONCE_TYPE == savedWrapper.listenerType && savedWrapper.equals(progressWrapper)) {
                        allListenersMap.remove(listener);
                        return true;
                    }
                }
                return false;
            });
        }
    }

    /**
     * Event category wrapper. This class solves the undefined category problem.
     */
    private static final class EventCategoryWrapper {
        private static final UUID UNDEFINED_EVENT_CATEGORY = UUID.randomUUID();
        @Nullable
        private final EventCategory eventCategory;
        @Nullable
        private final UUID randomUUID;
        private final int hash;

        /**
         * Constructor of undefined category.
         */
        private EventCategoryWrapper() {
            this.eventCategory = null;
            this.randomUUID = UNDEFINED_EVENT_CATEGORY;
            this.hash = randomUUID.hashCode();
        }

        /**
         * Constructor of well defined category.
         *
         * @param eventCategory event category object
         */
        private EventCategoryWrapper(@NonNull EventCategory eventCategory) {
            this.eventCategory = eventCategory;
            this.randomUUID = null;
            assert eventCategory != null;
            this.hash = eventCategory.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof EventCategoryWrapper) {
                EventCategoryWrapper comp = (EventCategoryWrapper) o;
                return o == this ||
                        (comp.eventCategory != null && this.eventCategory != null
                                && comp.eventCategory == this.eventCategory) ||
                        (comp.randomUUID != null && this.randomUUID != null
                                && comp.randomUUID.equals(this.randomUUID));
            }
            return false;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public String toString() {
            if (eventCategory != null)
                return eventCategory.name();
            return "UNDEFINED";
        }
    }

    /**
     * Link event wrapper. This class solves the undefined event problem.
     */
    private static final class LinkEventWrapper {
        private static final UUID UNDEFINED_LINK_EVENT = UUID.randomUUID();
        //Undefined type
        private static final int NONE_TYPE = -1;
        //The listener will be triggered just once.
        private static final int ONCE_TYPE = 0;
        //The listener will be always triggered until be removed.
        private static final int ALWAYS_TYPE = 1;
        @Nullable
        private final LinkEvent linkEvent;
        @NonNull
        private final EventCategoryWrapper categoryWrapper;
        @NonNull
        private final Object filter;
        @Nullable
        private final UUID randomUUID;
        private final int listenerType;
        private final int hash;

        /**
         * Constructor of undefined link event.
         *  @param categoryWrapper event category wrapper
         *
         */
        private LinkEventWrapper(@NonNull EventCategoryWrapper categoryWrapper) {
            this.linkEvent = null;
            this.categoryWrapper = categoryWrapper;
            this.randomUUID = UNDEFINED_LINK_EVENT;
            this.filter = NO_FILTER;
            this.listenerType = LinkEventWrapper.ALWAYS_TYPE;
            this.hash = randomUUID.hashCode();
        }

        /**
         * Constructor of defined link event.
         *
         * @param linkEvent    link event object type
         * @param filter       link event object filter
         * @param listenerType listener type
         */
        private LinkEventWrapper(@NonNull LinkEvent linkEvent, @NonNull Object filter, int listenerType) {
            assert linkEvent != null;
            this.linkEvent = linkEvent;
            this.categoryWrapper = new EventCategoryWrapper(linkEvent.getCategory());
            this.randomUUID = null;
            this.filter = filter;
            this.listenerType = listenerType;
            this.hash = Objects.hash(linkEvent.getCategory(), linkEvent, filter);
        }

        /**
         * Verifies if the event is undefined.
         *
         * @return <tt>true</tt> if undefined, <tt>false</tt> otherwise
         */
        private boolean isUndefined() {
            return linkEvent == null && randomUUID != null && randomUUID.equals(UNDEFINED_LINK_EVENT);
        }

        /**
         * Returns if the filter is default or not
         *
         * @return <tt>true</tt> if default, <tt>false</tt> otherwise
         */
        private boolean isDefaultFilter() {
            return filter.equals(Aggregator.NO_FILTER);
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof LinkEventWrapper) {
                LinkEventWrapper comp = (LinkEventWrapper) o;
                return o == this ||
                        (comp.linkEvent != null && this.linkEvent != null &&
                                comp.linkEvent.getCategory() == this.linkEvent.getCategory() &&
                                comp.linkEvent == this.linkEvent &&
                                comp.filter.equals(this.filter)) ||
                        (comp.randomUUID != null && this.randomUUID != null &&
                                comp.randomUUID.equals(this.randomUUID));
            }
            return false;
        }

        @Override
        public int hashCode() {
            return hash;
        }

        @Override
        public String toString() {
            if (linkEvent != null)
                return String.format(Locale.ENGLISH, "Cat: %s | Evt: %s | Default: %s | Filter: %s",
                        linkEvent.getCategory(), linkEvent, isDefaultFilter(), filter);
            return "Cat: UNDEFINED | Evt: UNDEFINED | Default: true | Filter: UNDEFINED";
        }
    }
}
