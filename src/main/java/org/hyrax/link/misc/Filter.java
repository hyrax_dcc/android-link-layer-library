/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.misc;

/**
 * General interface filter in order to discard undesired objects.
 *
 * @param <A> type object to be filtered
 */
public interface Filter<A> {
    /**
     * No filter object.
     */
    Filter NO_FILTER = argument -> true;

    /**
     * Method triggered when a filter needs to be applied to a specific object
     *
     * @param argument object to apply the filter
     * @return <tt>true</tt> if is a valid object or <tt>false</tt> of not
     */
    boolean apply(A argument);
}
