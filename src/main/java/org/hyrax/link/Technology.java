/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link;

/**
 * Enumeration of wireless technologies supported.
 */
public enum Technology {
    /**
     * Legacy Bluetooth technology control
     */
    BLUETOOTH,
    /**
     * Bluetooth Low Energy technology control
     */
    BLUETOOTH_LE,
    /**
     * Legacy Wifi technology control
     */
    WIFI,
    /**
     * Wifi Direct, aka Wifi P2P, technology control
     */
    WIFI_DIRECT,
    /**
     * 3G/4G data technology control
     */
    MOBILE
}
