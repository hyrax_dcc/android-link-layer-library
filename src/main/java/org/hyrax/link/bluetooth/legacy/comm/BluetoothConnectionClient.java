/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy.comm;

import android.bluetooth.BluetoothSocket;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.bluetooth.legacy.BluetoothNode;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.ConnectionLostEvent;
import org.hyrax.link.misc.event.ConnectionNewEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.LinkLayerRuntimeException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class {@link BluetoothConnectionClient} represents a connection to a remote bluetooth device.
 */
public class BluetoothConnectionClient implements Runnable {
    //byte exchange meaning that the endpoints clients are ready to transfer data
    private static final int REGISTERED = 1;
    private static final int REGISTERED_ACK = 2;
    @NonNull
    private final BluetoothNode device;
    @NonNull
    private final UUID uuid;
    @NonNull
    private final Aggregator aggregator;
    private final AtomicBoolean isRunning;
    @NonNull
    private final BluetoothComm bluetoothComm;
    @Nullable
    private BluetoothSocket bluetoothSocket;
    @Nullable
    private InputStream inputStream;
    @Nullable
    private OutputStream outputStream;

    /**
     * Constructor of class {@link BluetoothConnectionClient}, client side.
     * <br/>
     * This constructor is used when a client desires to establish a connection with a remote
     * device.
     *
     * @param device     remote device
     * @param uuid       server uuid
     * @param aggregator listener {@link Aggregator}
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/UUID.html" target="_blank">UUID</a>
     */
    public BluetoothConnectionClient(@NonNull BluetoothNode device, @NonNull UUID uuid,
                                     @NonNull Aggregator aggregator) {
        this.device = device;
        this.uuid = uuid;
        this.aggregator = aggregator;
        this.bluetoothSocket = null;
        this.isRunning = new AtomicBoolean(false);
        this.bluetoothComm = new BluetoothComm(this);
    }

    /**
     * Constructor of class {@link BluetoothConnectionClient}, server side.
     * <br/>
     * This constructor is used when a server receives a connection from a client.
     *
     * @param device     remote device
     * @param uuid       server uuid
     * @param aggregator listener {@link Aggregator}
     * @param socket     bluetooth socket
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/UUID.html" target="_blank">UUID</a>
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothSocket.html" target="_blank">BluetoothSocket</a>
     */
    BluetoothConnectionClient(@NonNull BluetoothNode device, @NonNull UUID uuid,
                              @NonNull Aggregator aggregator, @NonNull BluetoothSocket socket) {
        this(device, uuid, aggregator);
        this.bluetoothSocket = socket;
    }

    /**
     * Returns the socket output stream.
     *
     * @return the output stream or null if not available yet
     */
    @Nullable
    OutputStream getOutputStream() {
        return outputStream;
    }

    /**
     * Returns the {@link BluetoothComm} object.
     *
     * @return bluetooth comm object
     */
    @NonNull
    public BluetoothComm getBluetoothComm() {
        return bluetoothComm;
    }

    @Override
    public void run() {
        boolean hasInitiated = false;
        if (isRunning.compareAndSet(false, true)) {
            try {
                try {
                    if (null == bluetoothSocket) {
                        bluetoothSocket = device.getOriginalObject().createInsecureRfcommSocketToServiceRecord(uuid);
                        bluetoothSocket.connect();
                    }
                    inputStream = bluetoothSocket.getInputStream();
                    outputStream = bluetoothSocket.getOutputStream();
                    Receiver.notifyEvent(aggregator, new ConnectionNewEvent(device));
                    hasInitiated = true;
                } catch (IOException e) {
                    Receiver.notifyEvent(aggregator,
                            new ConnectionNewEvent(device, LinkException.build(e)));
                }

                try {
                    if (inputStream != null) {
                        byte b1 = (byte) inputStream.read();
                        if (b1 != REGISTERED)
                            throw new IOException("Undefined ready byte notify " + b1);

                        final boolean defined = bluetoothComm.isCommListenerDefined();
                        if (defined)
                            sendByte(REGISTERED_ACK);

                        byte b2 = (byte) inputStream.read();
                        if (b2 != REGISTERED_ACK)
                            throw new IOException("Undefined ready byte ack " + b2);

                        if (!defined)
                            sendByte(REGISTERED_ACK);

                        //this will block the thread until everything has been read
                        bluetoothComm.onInputStreamReady(inputStream);
                    }
                } catch (IOException e) {
                    if (isRunning.get()) e.printStackTrace();
                }
            } finally {
                close();
                isRunning.set(false);
                if (hasInitiated)
                    Receiver.notifyEvent(aggregator, new ConnectionLostEvent(device));
                bluetoothComm.onDestroy();
            }
        } else
            throw new LinkLayerRuntimeException("Bluetooth client already started");
    }

    /**
     * Notifies the the communication listener is now available.
     */
    public void registerNotify() {
        sendByte(REGISTERED);
    }

    /**
     * Writes a byte to output stream.
     *
     * @param byteInt byte to be written
     */
    private void sendByte(int byteInt) {
        try {
            if (outputStream != null)
                outputStream.write(byteInt);
            else
                throw new IOException("Null Output Stream");
        } catch (IOException e) {
            e.printStackTrace();
            close();
        }
    }

    /**
     * Returns the uuid to which is connected.
     *
     * @return server uuid
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/UUID.html" target="_blank">UUID</a>
     */
    @NonNull
    public UUID getUuid() {
        return uuid;
    }

    @NonNull
    public BluetoothNode getDevice() {
        return device;
    }

    /**
     * Disconnects the client.
     */
    public void disconnect() {
        if (isRunning.compareAndSet(true, false))
            close();
    }

    /**
     * Verifies if the thread is running.
     *
     * @return <tt>true</tt> if running, <tt>false</tt> otherwise
     */
    public boolean isRunning() {
        return isRunning.get();
    }

    /**
     * Closes the socket and input/output streams.
     */
    private void close() {
        try {
            if (inputStream != null) inputStream.close();
            if (outputStream != null) outputStream.close();
            if (bluetoothSocket != null) bluetoothSocket.close();
        } catch (IOException | NullPointerException e) {
            //do nothing
        }
    }

    @Override
    public boolean equals(Object o) {
        return (o == this) ||
                ((o instanceof BluetoothConnectionClient)
                        && device.equals(((BluetoothConnectionClient) o).device)
                );
    }

    @Override
    public int hashCode() {
        return device.hashCode();
    }
}
