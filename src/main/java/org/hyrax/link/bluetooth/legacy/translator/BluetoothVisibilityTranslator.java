/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy.translator;

import android.bluetooth.BluetoothAdapter;
import android.support.annotation.NonNull;

import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.VisibilityOffEvent;
import org.hyrax.link.misc.event.VisibilityOnEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.VisibilityLogicTranslator;
import org.hyrax.link.misc.properties.VisibilityProperties;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link BluetoothVisibilityTranslator} manages the Bluetooth visibility state.
 * VISIBLE/INVISIBLE
 */
public class BluetoothVisibilityTranslator implements VisibilityLogicTranslator {
    @NonNull
    private final BluetoothAdapter bluetoothAdapter;
    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<String, VisibilityProperties> advertisers;

    /**
     * Constructor of {@link BluetoothVisibilityTranslator} class.
     *
     * @param bluetoothAdapter android bluetooth manager object
     * @param aggregator       listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html" target="_blank">BluetoothAdapter</a>
     */
    public BluetoothVisibilityTranslator(@NonNull BluetoothAdapter bluetoothAdapter,
                                         @NonNull Aggregator aggregator) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.aggregator = aggregator;

        advertisers = new ConcurrentHashMap<>();
    }

    @Override
    public void turnVisibilityOn(@NonNull final VisibilityProperties visibilityProperties) {
        final String advertiseId = visibilityProperties.getIdentifier();

        LinkException exception = null;

        if (Constants.DEFAULT_ADVERTISER_UUID.equals(advertiseId)) {
            Object prop = advertisers.putIfAbsent(advertiseId, visibilityProperties);

            if (null == prop) {
                LinkListener<String> stopListener = (eventType, outcome) -> advertisers.remove(advertiseId);

                aggregator.listenOnce(stopListener, LinkEvent.LINK_VISIBILITY_OFF, advertiseId, true);

                if (!setScanMode(BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)) {
                    aggregator.removeListener(stopListener);
                    advertisers.remove(advertiseId);
                    exception = LinkException.build(Reason.ERROR_START, "Error starting visibility");
                }

            } else {
                exception = LinkException.build(Reason.ALREADY_STARTED, "Already active " + advertiseId);
            }

        } else {
            exception = LinkException.build(Reason.INVALID_INSTANCE, "Invalid advertiser identifier. Default only");
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new VisibilityOnEvent(advertiseId, exception));
    }

    @Override
    public void turnVisibilityOff(@NonNull String advertiseId) {
        VisibilityProperties properties = advertisers.remove(advertiseId);

        LinkException exception = null;
        if (null != properties) {
            boolean set = setScanMode(BluetoothAdapter.SCAN_MODE_CONNECTABLE);
            if (!set)
                exception = LinkException.build(Reason.ERROR_STOP,
                        "Error stopping visibility " + advertiseId);
        } else {
            exception = LinkException.build(Reason.NULL_POINTER, "No active advertiser");
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new VisibilityOffEvent(advertiseId, exception));
    }

    @Override
    public boolean isVisibilityOn(@NonNull String advertiseId) {
        return advertisers.containsKey(advertiseId) &&
                bluetoothAdapter.getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE;
    }

    @NonNull
    @Override
    public Collection<String> getAdvertisers() {
        return Collections.unmodifiableList(new ArrayList<>(advertisers.keySet()));
    }

    /**
     * Reflection method that changes the bluetooth scan state.
     * <br/>
     * Returns <tt>true</tt> if the mode was successfully changed, <tt>false</tt> otherwise
     *
     * @param mode bluetooth scan mode
     * @return <tt>true</tt> if success, <tt>false</tt> otherwise
     */
    private boolean setScanMode(int mode) {
        Method[] bltMethods = BluetoothAdapter.class.getDeclaredMethods();
        for (Method mReflected : bltMethods) {
            if ("public boolean android.bluetooth.BluetoothAdapter.setScanMode(int)".equals(mReflected.toGenericString())) {
                try {
                    return (Boolean) mReflected.invoke(bluetoothAdapter, mode);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public void detach() {
        advertisers.clear();
    }
}
