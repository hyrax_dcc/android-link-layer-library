/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy.translator;

import android.bluetooth.BluetoothAdapter;
import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.Filter;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.DiscoveryOffEvent;
import org.hyrax.link.misc.event.DiscoveryOnEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.DiscoveryLogicTranslator;
import org.hyrax.link.misc.properties.DiscoveryProperties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link BluetoothDiscoveryTranslator} manages the bluetooth discovery state.
 * START_DISCOVERY/STOP_DISCOVERY
 */
public class BluetoothDiscoveryTranslator implements DiscoveryLogicTranslator<Void, Filter<Device>> {
    @NonNull
    private final BluetoothAdapter bluetoothAdapter;
    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<String, DiscoveryProperties> scanners;

    /**
     * Constructor of {@link BluetoothDiscoveryTranslator} class.
     *
     * @param bluetoothAdapter android bluetooth manager object
     * @param aggregator       listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html" target="_blank">BluetoothAdapter</a>
     */
    public BluetoothDiscoveryTranslator(@NonNull BluetoothAdapter bluetoothAdapter, @NonNull Aggregator aggregator) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.aggregator = aggregator;

        scanners = new ConcurrentHashMap<>();
    }

    @Override
    public void turnDiscoveryOn(@NonNull DiscoveryProperties<Void, Filter<Device>> discoveryProperties) {
        final String scanId = discoveryProperties.getIdentifier();

        LinkException exception = null;

        if (Constants.DEFAULT_SCANNER_UUID.equals(scanId)) {
            Object prop = scanners.putIfAbsent(scanId, discoveryProperties);
            if (null == prop) {
                LinkListener<Collection<Device>> doneListener = (eventType, outcome) ->
                        outcome.ifError(argument -> scanners.remove(scanId));
                LinkListener<String> stopListener = (eventType, outcome) -> scanners.remove(scanId);

                aggregator.listenOnce(doneListener, LinkEvent.LINK_DISCOVERY_DONE, scanId, true);
                aggregator.listenOnce(stopListener, LinkEvent.LINK_DISCOVERY_OFF, scanId, true);

                if (!bluetoothAdapter.startDiscovery()) {
                    aggregator.removeListener(stopListener);
                    scanners.remove(scanId);
                    exception = LinkException.build(Reason.ERROR_START, "Error starting discovery");
                }
            } else {
                exception = LinkException.build(Reason.ALREADY_STARTED, "Discovery already active");
            }
        } else {
            exception = LinkException.build(Reason.INVALID_INSTANCE, "Invalid scanner identifier. Default only");
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(scanId, exception));
    }

    @Override
    public void turnDiscoveryOff(@NonNull String scanId) {
        LinkException exception = null;
        DiscoveryProperties properties = scanners.remove(scanId);
        if (null == properties) {
            exception = LinkException.build(Reason.NULL_POINTER, "No active discovery");
        } else if (!bluetoothAdapter.cancelDiscovery()) {
            exception = LinkException.build(Reason.ERROR_STOP, "Error stopping discovery");
        }
        if (null != exception)
            Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(scanId, exception));
    }

    @Override
    public boolean isDiscoveryOn(@NonNull String scanId) {
        return scanners.containsKey(scanId) && bluetoothAdapter.isDiscovering();
    }

    @NonNull
    @Override
    public Collection<String> getScanners() {
        return Collections.unmodifiableList(new ArrayList<>(scanners.keySet()));
    }

    @Override
    public void detach() {
        scanners.clear();
    }
}
