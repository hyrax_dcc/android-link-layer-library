/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.DiscoveryFoundEvent;
import org.hyrax.link.misc.event.DiscoveryOffEvent;
import org.hyrax.link.misc.event.DiscoveryOnEvent;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.event.HardwareOffEvent;
import org.hyrax.link.misc.event.HardwareOnEvent;
import org.hyrax.link.misc.event.VisibilityOffEvent;
import org.hyrax.link.misc.event.VisibilityOnEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Class {@link BluetoothLinkReceiver} receives all the bluetooth events and maps to well
 * defined {@link Event}
 */
public class BluetoothLinkReceiver extends Receiver {
    private int prevScanMode;

    /**
     * Constructor of {@link BluetoothLinkReceiver} class.
     *
     * @param aggregator listener aggregator
     */
    public BluetoothLinkReceiver(@NonNull Aggregator aggregator) {
        super(aggregator);
    }

    @NonNull
    @Override
    protected List<Event> actionToTechEvent(@NonNull String action, @NonNull Intent intent) {
        List<Event> events = new ArrayList<>();

        switch (action) {
            case BluetoothAdapter.ACTION_STATE_CHANGED:
                int hardwareState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (hardwareState) {
                    case BluetoothAdapter.STATE_ON:
                        events.add(new HardwareOnEvent());
                        break;

                    case BluetoothAdapter.STATE_OFF:
                        events.add(new HardwareOffEvent());
                        break;
                }
                break;
            case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                events.add(new DiscoveryOnEvent(Constants.DEFAULT_SCANNER_UUID));
                break;

            case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                events.add(new DiscoveryOffEvent(Constants.DEFAULT_SCANNER_UUID));
                break;

            case BluetoothAdapter.ACTION_SCAN_MODE_CHANGED:
                int scanMode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE,
                        BluetoothAdapter.ERROR);

                if (BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE != prevScanMode
                        && BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE == scanMode) {
                    events.add(new VisibilityOnEvent(Constants.DEFAULT_ADVERTISER_UUID));

                } else if (BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE == prevScanMode
                        && BluetoothAdapter.SCAN_MODE_CONNECTABLE == scanMode) {
                    events.add(new VisibilityOffEvent(Constants.DEFAULT_ADVERTISER_UUID));
                }
                prevScanMode = scanMode;
                break;
            case BluetoothDevice.ACTION_FOUND:
                final short rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Constants.DEFAULT_RSSI);
                final BluetoothDevice device =
                        intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (null != device) {
                    events.add(new DiscoveryFoundEvent(Constants.DEFAULT_SCANNER_UUID,
                            new BluetoothNode(device, rssi)));
                }
                break;

            case BluetoothDevice.ACTION_UUID:
                /*Parcelable[] uuid = intent.getParcelableArrayExtra(BluetoothDevice.EXTRA_UUID);
                BluetoothDevice dev = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (null != dev) {
                    Log.w(TAG, "Dev " + dev);
                }
                if (null != uuid) {
                    Log.w(TAG, "Size " + uuid.length);
                    for (int i = 0; i < uuid.length; i++) {
                        ParcelUuid u = (ParcelUuid) uuid[i];
                        Log.w(TAG, "UUID " + u);
                    }
                    Log.w(TAG, "UUID " + uuid);
                } else {
                    Log.w(TAG, "NULL");
                }*/

                break;
        }
        return events;
    }

    @NonNull
    @Override
    public IntentFilter getIntentFilters() {
        return new IntentFilter() {{
            addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
            addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
            addAction(BluetoothDevice.ACTION_FOUND);
            addAction(BluetoothDevice.ACTION_UUID);
            //addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
            // addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
            //addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
            //addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
            // addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);
            //addAction(BluetoothDevice.ACTION_NAME_CHANGED);
        }};
    }
}
