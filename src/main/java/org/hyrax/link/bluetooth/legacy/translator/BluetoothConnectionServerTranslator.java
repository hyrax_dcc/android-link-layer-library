/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy.translator;

import android.bluetooth.BluetoothAdapter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothConnectionClient;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothConnectionServer;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.EventObservable;
import org.hyrax.link.misc.Observer;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.ServerInfo;
import org.hyrax.link.misc.event.ConnectionServerOffEvent;
import org.hyrax.link.misc.event.ConnectionServerOnEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.ConnectionServerLogicTranslator;
import org.hyrax.link.misc.properties.ServerProperties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link BluetoothConnectionServerTranslator} manages the acceptance of connections from
 * the clients. ACCEPT/REJECT
 */
public class BluetoothConnectionServerTranslator implements ConnectionServerLogicTranslator<Void> {
    @NonNull
    private final BluetoothAdapter bluetoothAdapter;
    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<String, BluetoothConnectionServer> connectedServers;
    @NonNull
    private final ConcurrentHashMap<String, ServerInfo> connectedServersInfo;
    //for new clients events
    @NonNull
    private final EventObservable<Observer<BluetoothConnectionClient>> newClientsObservable;

    /**
     * Listen for all connection server events
     */
    private final LinkListener serverListener = new LinkListener<String>() {

        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<String> outcome) {
            switch (eventType) {
                case LINK_CONNECTION_SERVER_ON:
                    outcome.ifError(argument -> {
                        connectedServersInfo.remove(outcome.getOutcome());
                        connectedServers.remove(outcome.getOutcome());
                    });
                    break;

                case LINK_CONNECTION_SERVER_OFF:
                    outcome.ifSuccess(arg -> {
                        connectedServersInfo.remove(outcome.getOutcome());
                        connectedServers.remove(outcome.getOutcome());
                    });
                    break;
            }
        }
    };
    /**
     * Listen for connection {@link ServerInfo} events
     */
    private final LinkListener serverInfoListener = new LinkListener<ServerInfo>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<ServerInfo> outcome) {
            ServerInfo info = outcome.getOutcome();
            connectedServersInfo.put(info.getIdentifier(), info);
        }
    };

    /**
     * Constructor of {@link BluetoothConnectionServerTranslator} class.
     *
     * @param bluetoothAdapter android bluetooth manager object
     * @param aggregator       listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html" target="_blank">BluetoothAdapter</a>
     */
    public BluetoothConnectionServerTranslator(@NonNull BluetoothAdapter bluetoothAdapter, @NonNull Aggregator aggregator) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.aggregator = aggregator;

        this.connectedServers = new ConcurrentHashMap<>();
        this.connectedServersInfo = new ConcurrentHashMap<>();
        aggregator.listen(serverListener, EventCategory.LINK_CONNECTION_SERVER, true);
        aggregator.listen(serverInfoListener, LinkEvent.LINK_CONNECTION_SERVER_INFO, Aggregator.NO_FILTER, true);
        this.newClientsObservable = new EventObservable<>();
    }

    @Override
    public void turnServerOn(@NonNull ServerProperties<Void> properties) {
        LinkException exception = null;
        try {
            BluetoothConnectionServer server = new BluetoothConnectionServer(
                    bluetoothAdapter, aggregator, UUID.fromString(properties.getIdentifier()),
                    properties.getName()
            );

            Object prop = connectedServers.putIfAbsent(properties.getIdentifier(), server);
            if (null == prop) {
                //listens for new clients and notifies it
                server.listenNewClients(object ->
                        newClientsObservable.notifyListeners(listener -> {
                            listener.onEvent(object);
                            return false;
                        }));

                new Thread(server).start();
            } else {
                exception = LinkException.build(Reason.ALREADY_STARTED, "Server already started: " +
                        properties.getIdentifier());
            }
        } catch (IllegalArgumentException e) {
            exception = LinkException.build(e);
            connectedServersInfo.remove(properties.getIdentifier());
            connectedServers.remove(properties.getIdentifier());
        }
        if (null != exception)
            Receiver.notifyEvent(aggregator, new ConnectionServerOnEvent(properties.getIdentifier(),
                    exception
            ));
    }

    @Override
    public void turnServerOff(@NonNull String serverId) {
        connectedServersInfo.remove(serverId);
        BluetoothConnectionServer server = connectedServers.remove(serverId);
        if (null != server) {
            server.disconnect();
        } else {
            Receiver.notifyEvent(aggregator, new ConnectionServerOffEvent(serverId,
                    LinkException.build(Reason.NULL_POINTER, "No server active " + serverId)));
        }
    }

    @Override
    public boolean isServerOn(@NonNull String serverId) {
        return connectedServers.containsKey(serverId);
    }

    @NonNull
    @Override
    public Collection<String> getActiveServers() {
        return Collections.unmodifiableCollection(new ArrayList<>(connectedServers.keySet()));
    }

    /**
     * Adds a listener in order to listen for new clients - for internal use.
     *
     * @param listener listener object
     */
    void listenNewClients(@NonNull Observer<BluetoothConnectionClient> listener) {
        newClientsObservable.addListener(listener);
    }

    /**
     * Returns information about a specific server
     *
     * @param serverId server identifier
     * @return {@link ServerInfo} object
     */
    @Nullable
    public ServerInfo getActiveServerInfo(@NonNull String serverId) {
        return connectedServersInfo.get(serverId);
    }

    @Override
    public void detach() {
        aggregator.removeListener(serverListener);
        aggregator.removeListener(serverInfoListener);

        newClientsObservable.removeAll();
        for (BluetoothConnectionServer server : connectedServers.values())
            server.disconnect();

        connectedServers.clear();
        connectedServersInfo.clear();
    }
}
