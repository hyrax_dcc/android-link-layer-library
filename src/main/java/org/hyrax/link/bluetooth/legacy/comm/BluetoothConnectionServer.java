/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy.comm;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.support.annotation.NonNull;

import org.hyrax.link.bluetooth.legacy.BluetoothNode;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.EventObservable;
import org.hyrax.link.misc.Observer;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.ConnectionServerInfoEvent;
import org.hyrax.link.misc.event.ConnectionServerNewClient;
import org.hyrax.link.misc.event.ConnectionServerOffEvent;
import org.hyrax.link.misc.event.ConnectionServerOnEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.LinkLayerRuntimeException;
import org.hyrax.link.misc.exception.Reason;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class {@link BluetoothConnectionServer} represents a server that listen for client connections.
 */
public class BluetoothConnectionServer implements Runnable {
    @NonNull
    private final BluetoothAdapter bluetoothAdapter;
    @NonNull
    private final Aggregator aggregator;
    @NonNull
    private final UUID serverUUID;
    @NonNull
    private final String serverName;
    private final AtomicBoolean isRunning;
    //for new clients events
    @NonNull
    private final EventObservable<Observer<BluetoothConnectionClient>> newClientsObservable;

    private BluetoothServerSocket serverSocket;


    /**
     * Constructor of class {@link BluetoothConnectionServer}.
     * <br/>
     * This constructor specifies which uuid wants to listen and also specifies a name.
     *
     * @param bluetoothAdapter android bluetooth manager object
     * @param aggregator       listener listener {@link Aggregator}
     * @param uuid             server uuid
     * @param name             server name
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html" target="_blank">BluetoothAdapter</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/util/UUID.html" target="_blank">UUID</a>
     */
    public BluetoothConnectionServer(@NonNull BluetoothAdapter bluetoothAdapter, @NonNull Aggregator aggregator,
                                     @NonNull UUID uuid, @NonNull String name) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.aggregator = aggregator;
        this.serverUUID = uuid;
        this.serverName = name;
        this.isRunning = new AtomicBoolean(false);
        this.newClientsObservable = new EventObservable<>();
    }

    /**
     * Adds a listener in order to listen for new clients.
     *
     * @param listener listener object
     */
    public void listenNewClients(@NonNull Observer<BluetoothConnectionClient> listener) {
        newClientsObservable.addListener(listener);
    }

    @Override
    public void run() {
        boolean hasInitiated = false;
        if (isRunning.compareAndSet(false, true)) {
            try {
                try {
                    serverSocket = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(serverName, serverUUID);
                    //notifies the server information first
                    Receiver.notifyEvent(aggregator, new ConnectionServerInfoEvent(
                            new BluetoothServerInfo(serverUUID.toString())));
                    Receiver.notifyEvent(aggregator, new ConnectionServerOnEvent(serverUUID.toString()));
                    hasInitiated = true;
                } catch (IOException e) {
                    Receiver.notifyEvent(aggregator,
                            new ConnectionServerOnEvent(serverUUID.toString(), LinkException.build(e)));
                    return;
                }

                while (isRunning.get()) {
                    try {
                        BluetoothSocket bluetoothSocket = serverSocket.accept();
                        BluetoothNode node = new BluetoothNode(bluetoothSocket.getRemoteDevice(), Constants.DEFAULT_RSSI);
                        BluetoothConnectionClient connectionClient = new BluetoothConnectionClient(
                                node, serverUUID, aggregator, bluetoothSocket);
                        //notifies new client object - just for internal use
                        newClientsObservable.notifyListeners(listener -> {
                            listener.onEvent(connectionClient);
                            return false;
                        });
                        //notifies new client device - to everyone
                        Receiver.notifyEvent(aggregator,
                                new ConnectionServerNewClient(node));
                        //start client
                        new Thread(connectionClient).start();

                    } catch (IOException e) {
                        if (isRunning.get()) e.printStackTrace();
                        break;
                    }
                }
            } finally {
                close();
                isRunning.set(false);
                if (hasInitiated)
                    Receiver.notifyEvent(aggregator, new ConnectionServerOffEvent(serverUUID.toString()));
            }
        } else
            throw new LinkLayerRuntimeException("Bluetooth server already started");
    }

    /**
     * Disconnects the connection server
     */
    public void disconnect() {
        if (isRunning.compareAndSet(true, false))
            close();
        else
            Receiver.notifyEvent(aggregator, new ConnectionServerOffEvent(
                    serverUUID.toString(),
                    LinkException.build(Reason.NULL_POINTER, "Server already destroyed")
            ));
    }

    /**
     * Closes the server socket.
     */
    private void close() {
        try {
            newClientsObservable.removeAll();
            serverSocket.close();
        } catch (IOException | NullPointerException e) {
            //do nothing
        }
    }

    @Override
    public boolean equals(Object o) {
        return (o == this) ||
                (o instanceof BluetoothConnectionServer &&
                        serverUUID.equals(((BluetoothConnectionServer) o).serverUUID)
                );
    }

    @Override
    public int hashCode() {
        return serverUUID.hashCode();
    }
}
