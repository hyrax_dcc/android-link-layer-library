/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.ScanData;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Class {@link BluetoothNode} represents a bluetooth remote device.
 */
public class BluetoothNode implements Device {
    @NonNull
    private final BluetoothDevice device;
    private final String name;
    private final String uniqueAddress;
    private final int rssi;

    /**
     * Constructor of {@link BluetoothNode} class
     *
     * @param device remote bluetooth device object
     * @param rssi   signal strength
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothDevice.html" target="_blank">BluetoothDevice</a>
     */
    public BluetoothNode(@NonNull BluetoothDevice device, int rssi) {
        this.device = device;
        this.name = device.getName();
        this.uniqueAddress = device.getAddress();
        this.rssi = rssi;
    }

    @NonNull
    @Override
    public String getName() {
        if (null == name)
            return Constants.UNDEFINED;
        return name;
    }

    @NonNull
    @Override
    public String getUniqueAddress() {
        return uniqueAddress;
    }

    @Override
    public int getRssi() {
        return rssi;
    }

    @NonNull
    @Override
    public BluetoothDevice getOriginalObject() {
        return device;
    }

    @NonNull
    @Override
    public Collection<ScanData> getScanData() {
        return new ArrayList<>();
    }

    @Override
    public void mergeScanData(Collection<ScanData> scanData) {
        //do_nothing
    }

    @NonNull
    @Override
    public Technology getTechnology() {
        return Technology.BLUETOOTH;
    }

    @Override
    public String toString() {
        return String.format("%s: %s (%s)", getTechnology().name(), uniqueAddress, name);
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof BluetoothNode) && ((BluetoothNode) o).device.equals(device));
    }

    @Override
    public int hashCode() {
        return device.hashCode();
    }
}
