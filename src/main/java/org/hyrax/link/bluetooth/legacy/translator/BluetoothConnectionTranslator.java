/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy.translator;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;

import org.hyrax.link.Device;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.bluetooth.legacy.BluetoothNode;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothComm;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothConnectionClient;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.ConnectionLostEvent;
import org.hyrax.link.misc.event.ConnectionNewEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.ConnectionLogicTranslator;
import org.hyrax.link.misc.properties.ConnectionProperties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link BluetoothConnectionTranslator} manages the bluetooth remote connections.
 * CONNECT/DISCONNECT
 */
public class BluetoothConnectionTranslator implements ConnectionLogicTranslator<Void> {
    private static final int FROM_CLIENT = 0;
    private static final int FROM_SERVER = 1;

    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<Device, Pair<BluetoothConnectionClient, Integer>> connectedDevices;
    /**
     * Listens for connection events client side.
     */
    private final LinkListener clientListener = new LinkListener<Device>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<Device> outcome) {
            switch (eventType) {
                case LINK_CONNECTION_NEW:
                    if (!outcome.isSuccessful())
                        connectedDevices.remove(outcome.getOutcome());
                    break;

                case LINK_CONNECTION_LOST:
                    connectedDevices.remove(outcome.getOutcome());
                    break;
            }
        }
    };

    /**
     * Constructor of {@link BluetoothConnectionTranslator} class.
     *
     * @param aggregator listeners aggregator
     */
    public BluetoothConnectionTranslator(@NonNull Aggregator aggregator,
                                         @NonNull BluetoothConnectionServerTranslator serverTranslator) {
        this.aggregator = aggregator;

        connectedDevices = new ConcurrentHashMap<>();

        aggregator.listen(clientListener, EventCategory.LINK_CONNECTION, true);

        //listens for new clients - Source server
        serverTranslator.listenNewClients(client -> {
            Device device = client.getDevice();
            Object prop = connectedDevices.putIfAbsent(device, new Pair<>(client, FROM_SERVER));
            if (null != prop) {
                Receiver.notifyEvent(aggregator, new ConnectionNewEvent(device,
                        LinkException.build(Reason.ALREADY_STARTED,
                                "Server side client connection already exists: " + device.getUniqueAddress())));
                client.disconnect();
            }
        });
    }

    @Override
    public void turnConnectionOn(@NonNull Device remoteDevice, @NonNull ConnectionProperties<Void> properties) {
        LinkException exception = null;
        try {
            BluetoothConnectionClient connectionSocket = new BluetoothConnectionClient(
                    (BluetoothNode) remoteDevice,
                    UUID.fromString(properties.getIdentifier()),
                    aggregator);

            Object prop = connectedDevices.putIfAbsent(remoteDevice, new Pair<>(connectionSocket, FROM_CLIENT));

            if (null == prop) {
                new Thread(connectionSocket).start();

            } else {
                exception = LinkException.build(Reason.ALREADY_STARTED, "Device already connected: "
                        + remoteDevice.getUniqueAddress());
            }

        } catch (ClassCastException | IllegalArgumentException e) {
            connectedDevices.remove(remoteDevice);
            exception = LinkException.build(e);
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new ConnectionNewEvent(remoteDevice, exception));
    }

    @Override
    public void turnConnectionOff(@NonNull Device remoteDevice) {
        Pair<BluetoothConnectionClient, Integer> client = connectedDevices.remove(remoteDevice);
        if (null != client) {
            client.first.disconnect();
        } else {
            Receiver.notifyEvent(aggregator, new ConnectionLostEvent(remoteDevice,
                    LinkException.build(Reason.NULL_POINTER, "No connection active " + remoteDevice)
            ));
        }
    }

    @Override
    public boolean isConnectionOn(@NonNull Device remoteDevice) {
        return connectedDevices.containsKey(remoteDevice);
    }

    @NonNull
    @Override
    public Collection<Device> getActiveConnections() {
        return Collections.unmodifiableCollection(new ArrayList<>(connectedDevices.keySet()));
    }

    /**
     * Returns the device {@link BluetoothComm} object, in order to be able to exchange messages.
     *
     * @param remoteDevice a device object
     * @return {@link BluetoothComm} object
     */
    @Nullable
    public BluetoothComm getDeviceComm(@NonNull Device remoteDevice) {
        Pair<BluetoothConnectionClient, ?> pair = connectedDevices.get(remoteDevice);
        if (null != pair)
            return pair.first.getBluetoothComm();
        return null;
    }

    @Override
    public void detach() {
        aggregator.removeListener(clientListener);

        for (Pair<BluetoothConnectionClient, Integer> client : connectedDevices.values())
            client.first.disconnect();

        connectedDevices.clear();
    }
}
