/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy.translator;

import android.bluetooth.BluetoothAdapter;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.HardwareOffEvent;
import org.hyrax.link.misc.event.HardwareOnEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.HardwareLogicTranslator;

/**
 * Class {@link BluetoothHardwareTranslator} manages the Bluetooth state. ENABLE/DISABLE
 */
public class BluetoothHardwareTranslator implements HardwareLogicTranslator {
    @NonNull
    private final BluetoothAdapter bluetoothAdapter;
    @NonNull
    private final Aggregator aggregator;

    /**
     * Constructor of {@link BluetoothHardwareTranslator} class.
     *
     * @param bluetoothAdapter android bluetooth manager object
     * @param aggregator       listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html" target="_blank">BluetoothAdapter</a>
     */
    public BluetoothHardwareTranslator(@NonNull BluetoothAdapter bluetoothAdapter, @NonNull Aggregator aggregator) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.aggregator = aggregator;
    }

    @Override
    public void turnHardwareOn() {
        if (!bluetoothAdapter.enable()) {
            Receiver.notifyEvent(aggregator, new HardwareOnEvent(
                    LinkException.build(Reason.ERROR_START, "Error turning hardware on")
            ));
        }
    }

    @Override
    public void turnHardwareOff() {
        if (!bluetoothAdapter.disable()) {
            Receiver.notifyEvent(aggregator, new HardwareOffEvent(
                    LinkException.build(Reason.ERROR_STOP, "Error turning hardware off")
            ));
        }
    }

    @Override
    public boolean isHardwareOn() {
        return bluetoothAdapter.isEnabled();
    }

    @Override
    public void detach() {

    }
}
