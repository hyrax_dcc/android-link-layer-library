/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.legacy.comm;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.exception.LinkLayerRuntimeException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Wrapper class to allows send and receive messages on bluetooth sockets.
 */
public final class BluetoothComm {
    //private static final int WAIT_FOR_LISTENER = 10000; //10 seconds
    private final Lock lock = new ReentrantLock();
    //private final Condition listenerCond = lock.newCondition();

    @NonNull
    private final BluetoothConnectionClient client;
    @Nullable
    private CommListener commListener;

    /**
     * Constructor.
     *
     * @param client bluetooth socket client
     */
    BluetoothComm(@NonNull BluetoothConnectionClient client) {
        this.client = client;
    }

    /**
     * Returns the bluetooth socket output stream object.
     *
     * @return output stream object
     */
    @Nullable
    public OutputStream getOutputStream() {
        return client.getOutputStream();
    }

    /**
     * This method is triggered when the bluetooth socket input stream is ready to receive bytes.
     * <br/>
     * Loop through the input stream to read the bytes
     *
     * @param inputStream input stream object
     * @throws IOException exception triggered by the input stream
     */
    void onInputStreamReady(@NonNull final InputStream inputStream) throws IOException {
        lock.lock();
        try {
            if (commListener == null)
                throw new LinkLayerRuntimeException("CommListener cannot be NULL");
            commListener.onInputStreamReady(inputStream);
            /*if (commListener == null) {
                listenerCond.await(WAIT_FOR_LISTENER, TimeUnit.MILLISECONDS);
            }

            if (commListener != null)
                commListener.onInputStreamReady(inputStream);*/

        } finally {
            lock.unlock();
        }
    }

    /**
     * Verifies if the communication listener is defined.
     *
     * @return <tt>true</tt> if defined,<tt>false</tt> otherwise
     */
    public boolean isCommListenerDefined() {
        lock.lock();
        try {
            return this.commListener != null;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Changes the bluetooth communication listener.
     *
     * @param commListener listener object
     */
    public void setCommListener(@NonNull CommListener commListener) {
        lock.lock();
        try {
            if (this.commListener != null)
                throw new LinkLayerRuntimeException("CommListener already set");

            this.commListener = commListener;
            if (!client.isRunning()) {
                onDestroy();
                return;
            }
            client.registerNotify();
            //listenerCond.signal();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Cleans whatever needs to be cleansed.
     */
    void onDestroy() {
        lock.lock();
        try {
            if (commListener != null) {
                commListener.onStop();
                commListener = null;
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Forces bluetooth socket stop.
     */
    public void forceStop() {
        lock.lock();
        try {
            client.disconnect();
            //listenerCond.signal();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Returns a boolean meaning to query the bluetooth socket either if it's running or not.l
     *
     * @return boolean value. <tt>true</tt> the thread socket is running, <tt>false</tt> is not running
     */
    public boolean isRunning() {
        return client.isRunning();
    }

    /**
     * Bluetooth socket listener interface.
     */
    public interface CommListener {
        /**
         * Method triggered when the input stream is ready to be processed.
         * <br/>
         * This method is just triggered ONCE
         * <br/>
         * The user must loop through the input stream in order to get all bytes.
         *
         * @param inputStream input stream object
         * @throws IOException throws an error if something happens
         */
        void onInputStreamReady(@NonNull InputStream inputStream) throws IOException;

        /**
         * Method triggered when the bluetooth socket has been stopped.
         */
        void onStop();
    }
}
