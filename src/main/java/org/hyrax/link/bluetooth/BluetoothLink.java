/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.misc.Filter;
import org.hyrax.link.misc.link.Translators;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.Device;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothComm;
import org.hyrax.link.bluetooth.legacy.BluetoothLinkReceiver;
import org.hyrax.link.bluetooth.legacy.translator.BluetoothConnectionServerTranslator;
import org.hyrax.link.bluetooth.legacy.translator.BluetoothConnectionTranslator;
import org.hyrax.link.bluetooth.legacy.translator.BluetoothDiscoveryTranslator;
import org.hyrax.link.bluetooth.legacy.translator.BluetoothHardwareTranslator;
import org.hyrax.link.bluetooth.legacy.translator.BluetoothVisibilityTranslator;
import org.hyrax.link.misc.link.AbstractLink;
import org.hyrax.link.misc.link.LinkFeatures;

/**
 * Singleton class responsible for managing the bluetooth operations.
 */
public class BluetoothLink extends AbstractLink<BluetoothLink> implements LinkFeatures {
    private static final String SERVER_NAME = "Bluetooth Server";
    private static volatile BluetoothLink instance;

    /**
     * Constructor.
     */
    private BluetoothLink() {
        super(Technology.BLUETOOTH);
    }

    /**
     * Returns the bluetooth link instance.
     *
     * @return bluetooth link instance
     */
    @NonNull
    public static BluetoothLink getInstance() {
        if (instance == null)
            instance = new BluetoothLink();
        return instance;
    }

    @NonNull
    @Override
    protected Translators initTranslators() {
        BluetoothAdapter adapter = linkContext.getBluetoothAdapter();
        BluetoothConnectionServerTranslator serverTranslator =
                new BluetoothConnectionServerTranslator(adapter, aggregator);
        return new Translators(
                new BluetoothLinkReceiver(aggregator),
                new BluetoothHardwareTranslator(adapter, aggregator),
                new BluetoothDiscoveryTranslator(adapter, aggregator),
                new BluetoothVisibilityTranslator(adapter, aggregator),
                serverTranslator,
                new BluetoothConnectionTranslator(aggregator, serverTranslator)
        );
    }

    @Override
    protected void onAttach() {

    }

    @Override
    protected void onDetach() {
        instance = null;
    }

    @Override
    protected BluetoothLink getFeatures() {
        return this;
    }

    @Override
    public DiscoveryProperties.Builder<Void, Filter<Device>> newDiscoveryBuilder() {
        return DiscoveryProperties.<Void, Filter<Device>>newBuilder()
                .setScannerFilter(Filter.NO_FILTER);
    }

    @Override
    public VisibilityProperties.Builder<Void, Void> newVisibilityBuilder() {
        return VisibilityProperties.newBuilder();
    }

    @Override
    public ConnectionProperties.Builder<Void> newConnectionBuilder(@NonNull Device device) {
        return ConnectionProperties.newBuilder(Constants.DEFAULT_SERVER_UUID);
    }

    @Override
    public ServerProperties.Builder<Void> newServerBuilder() {
        return ServerProperties.<Void>newBuilder()
                .setName(SERVER_NAME);
    }

    /**
     * Changes the Bluetooth device name
     *
     * @param name new_def name
     * @return <tt>true</tt> if successfully changed, <tt>false</tt> otherwise
     */
    public boolean setName(String name) {
        return linkContext.getBluetoothAdapter().setName(name);
    }

    /**
     * Return the current bluetooth name
     *
     * @return string name or {@value Constants#UNDEFINED}
     */
    @NonNull
    public String getName() {
        String name = linkContext.getBluetoothAdapter().getName();
        return (null == name) ? Constants.UNDEFINED : name;
    }

    /**
     * Returns the device {@link BluetoothComm} object, in order to be able to exchange messages.
     *
     * @param remoteDevice a device object
     * @return {@link BluetoothComm} object
     */
    @Nullable
    public BluetoothComm getCommLink(@NonNull Device remoteDevice) {
        return ((BluetoothConnectionTranslator) translators.connectionLogicTranslator)
                .getDeviceComm(remoteDevice);
    }
}
