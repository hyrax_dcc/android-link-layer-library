/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.types;

import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Bluetooth le server settings.
 */
public class BluetoothLeServerSettings {
    @Nullable
    private final BluetoothGattService gattService;
    @Nullable
    private final BluetoothGattServerCallback serverCallback;


    /**
     * Constructor.
     *
     * @param builder builder object
     */
    private BluetoothLeServerSettings(@NonNull Builder builder) {
        this.gattService = builder.gattService;
        this.serverCallback = builder.serverCallback;
    }

    /**
     * Returns the gatt service object.
     *
     * @return gatt service object or NULL.
     */
    @Nullable
    public BluetoothGattService getGattService() {
        return gattService;
    }

    /**
     * Returns the service callback object.
     *
     * @return server callback or NULL
     */
    @Nullable
    public BluetoothGattServerCallback getServerCallback() {
        return serverCallback;
    }

    /**
     * Instantiates a builder object in order to defined the intended parameters.
     *
     * @return a {@link Builder} object
     */
    @NonNull
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * {@link BluetoothLeServerSettings} class builder.
     */
    public static class Builder {
        private BluetoothGattService gattService;
        private BluetoothGattServerCallback serverCallback;

        /**
         * Builder constructor.
         */
        private Builder() {
            this.gattService = null;
            this.serverCallback = null;
        }

        /**
         * Changes the gatt service.
         *
         * @param gattService bluetooth le gatt service object
         * @return builder object
         */
        @NonNull
        public Builder setGattService(@NonNull BluetoothGattService gattService) {
            this.gattService = gattService;
            return this;
        }

        /**
         * Changes the server callback
         *
         * @param serverCallback bluetooth le server callback
         * @return builder object
         */
        @NonNull
        public Builder setServerCallback(@NonNull BluetoothGattServerCallback serverCallback) {
            this.serverCallback = serverCallback;
            return this;
        }

        /**
         * Constructs and returns the {@link BluetoothLeServerSettings} object.
         *
         * @return {@link BluetoothLeServerSettings} object
         */
        @NonNull
        public BluetoothLeServerSettings build() {
            return new BluetoothLeServerSettings(this);
        }
    }
}
