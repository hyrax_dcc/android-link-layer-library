/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.bluetooth.types.BluetoothLeServerSettings;
import org.hyrax.link.misc.link.Translators;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.Device;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.bluetooth.le.BluetoothLeLinkReceiver;
import org.hyrax.link.bluetooth.le.translator.BluetoothLeConnectionServerTranslator;
import org.hyrax.link.bluetooth.le.translator.BluetoothLeConnectionTranslator;
import org.hyrax.link.bluetooth.le.translator.BluetoothLeDiscoveryTranslator;
import org.hyrax.link.bluetooth.le.translator.BluetoothLeVisibilityTranslator;
import org.hyrax.link.bluetooth.legacy.translator.BluetoothHardwareTranslator;
import org.hyrax.link.misc.link.AbstractLink;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.misc.exception.Reason;

import java.util.ArrayList;

/**
 * Singleton class responsible for managing the bluetooth le operations.
 */
@TargetApi(value = 21)
public class BluetoothLeLink extends AbstractLink<BluetoothLeLink> implements LinkFeatures {
    private static final String SERVER_NAME = "Bluetooth Le Server";
    private static BluetoothLeLink instance;

    /**
     * Constructor.
     */
    private BluetoothLeLink() {
        super(Technology.BLUETOOTH_LE);
    }

    /**
     * Returns the bluetooth le link instance.
     *
     * @return bluetooth le link instance
     */
    public static BluetoothLeLink getInstance() {
        if (instance == null)
            instance = new BluetoothLeLink();
        return instance;
    }

    /**
     * Maps the {@link AdvertiseCallback} failure integer codes to string.
     *
     * @param reason failure integer reason
     * @return string code representation
     */
    @NonNull
    public static Reason mapAdvertiseToReason(int reason) {
        switch (reason) {
            case AdvertiseCallback.ADVERTISE_FAILED_ALREADY_STARTED:
                return Reason.ALREADY_STARTED;

            case AdvertiseCallback.ADVERTISE_FAILED_DATA_TOO_LARGE:
                return Reason.DATA_TOO_LARGE;

            case AdvertiseCallback.ADVERTISE_FAILED_FEATURE_UNSUPPORTED:
                return Reason.UNSUPPORTED;

            case AdvertiseCallback.ADVERTISE_FAILED_TOO_MANY_ADVERTISERS:
                return Reason.TOO_MANY_ACTIVE;

            default:
                return Reason.INTERNAL_ERROR;
        }
    }

    /**
     * Maps an scan callback integer into enum reason
     *
     * @param reason scan callback integer reason
     * @return enum reason
     */
    @NonNull
    public static Reason mapScanToReason(int reason) {
        switch (reason) {
            case ScanCallback.SCAN_FAILED_ALREADY_STARTED:
                return Reason.ALREADY_STARTED;

            case ScanCallback.SCAN_FAILED_FEATURE_UNSUPPORTED:
                return Reason.UNSUPPORTED;

            default:
                return Reason.INTERNAL_ERROR;
        }
    }

    @NonNull
    @Override
    protected Translators initTranslators() {
        BluetoothLeConnectionServerTranslator serverTranslator =
                new BluetoothLeConnectionServerTranslator(linkContext, aggregator);

        return new Translators(new BluetoothLeLinkReceiver(aggregator),
                new BluetoothHardwareTranslator(linkContext.getBluetoothAdapter(), aggregator),
                new BluetoothLeDiscoveryTranslator(linkContext, aggregator),
                new BluetoothLeVisibilityTranslator(linkContext, aggregator),
                serverTranslator,
                new BluetoothLeConnectionTranslator(linkContext, serverTranslator, aggregator)
        );
    }

    @Override
    protected void onAttach() {

    }

    @Override
    protected void onDetach() {
        instance = null;
    }

    @Override
    protected BluetoothLeLink getFeatures() {
        return this;
    }

    @Override
    public DiscoveryProperties.Builder<ScanSettings, ArrayList<ScanFilter>> newDiscoveryBuilder() {
        return newDiscoveryBuilder(
                new ScanSettings.Builder().build(),
                new ArrayList<>());
    }

    /**
     * Returns a discovery properties builder.
     *
     * @param scanSettings scanner settings
     * @param filter       scanner filter
     * @return discovery properties builder
     */
    @NonNull
    public DiscoveryProperties.Builder<ScanSettings, ArrayList<ScanFilter>> newDiscoveryBuilder(
            @NonNull ScanSettings scanSettings,
            @NonNull ArrayList<ScanFilter> filter) {
        return DiscoveryProperties.<ScanSettings, ArrayList<ScanFilter>>newBuilder()
                .setScannerSettings(scanSettings)
                .setScannerFilter(filter);
    }


    @Override
    public VisibilityProperties.Builder<AdvertiseSettings, AdvertiseData> newVisibilityBuilder() {
        return newVisibilityBuilder(
                new AdvertiseSettings.Builder().build(),
                new AdvertiseData.Builder()
                        .setIncludeDeviceName(true)
                        .setIncludeTxPowerLevel(true)
                        .build()
        );
    }

    /**
     * Returns a visibility properties builder.
     *
     * @param settings advertiser settings
     * @param data     advertiser data
     * @return visibility properties builder
     */
    @NonNull
    public VisibilityProperties.Builder<AdvertiseSettings, AdvertiseData> newVisibilityBuilder(
            @NonNull AdvertiseSettings settings,
            @NonNull AdvertiseData data) {
        return VisibilityProperties.<AdvertiseSettings, AdvertiseData>newBuilder()
                .setAdvertiserSettings(settings)
                .setAdvertiserData(data)
                .stopAfterTimeoutExpiration(false);
    }

    @Override
    public ConnectionProperties.Builder<BluetoothGattCallback> newConnectionBuilder(@NonNull Device device) {
        return ConnectionProperties.newBuilder(device.getName());
    }

    @Override
    public ServerProperties.Builder<BluetoothLeServerSettings> newServerBuilder() {
        return ServerProperties.<BluetoothLeServerSettings>newBuilder()
                .setName(SERVER_NAME);
    }

    /**
     * Returns the bluetooth gatt object that matches a device.
     *
     * @param device bluetooth device
     * @return bluetooth gatt object or NULL if not connected
     */
    @Nullable
    public BluetoothGatt getBluetoothGatt(@NonNull Device device) {
        return ((BluetoothLeConnectionTranslator) translators.connectionLogicTranslator)
                .getBluetoothGatt(device);
    }

    /**
     * Returns the bluetooth gatt server that matches a server identifier.
     *
     * @param identifier unique server identifier
     * @return bluetooth gatt server object or NULL if not started
     */
    @Nullable
    public BluetoothGattServer getBluetoothGattServer(@NonNull String identifier) {
        return ((BluetoothLeConnectionServerTranslator) translators.connectionServerLogicTranslator)
                .getBluetoothGattServer(identifier);
    }
}
