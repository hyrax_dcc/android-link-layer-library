/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.le.translator;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Pair;

import org.hyrax.link.Device;
import org.hyrax.link.misc.Executable;
import org.hyrax.link.bluetooth.types.BluetoothLeServerSettings;
import org.hyrax.link.misc.EventObservable;
import org.hyrax.link.misc.Observer;
import org.hyrax.link.misc.event.ConnectionLostEvent;
import org.hyrax.link.misc.event.ConnectionNewEvent;
import org.hyrax.link.misc.event.ConnectionServerNewClient;
import org.hyrax.link.misc.event.ConnectionServerOffEvent;
import org.hyrax.link.misc.event.ConnectionServerOnEvent;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.bluetooth.le.BluetoothLeNode;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.link.LinkContext;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.ConnectionServerLogicTranslator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link BluetoothLeConnectionServerTranslator}  manages the bluetooth be gatt server.
 */
public class BluetoothLeConnectionServerTranslator implements ConnectionServerLogicTranslator<BluetoothLeServerSettings> {
    @NonNull
    private final LinkContext linkContext;
    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<String, GattServerManager> connectedServers;
    @NonNull
    private final BluetoothManager bluetoothManager;
    //for new clients events
    @NonNull
    private final EventObservable<Observer<Pair<Device, String>>> newClientsObservable;

    /**
     * Constructor of {@link BluetoothLeConnectionServerTranslator} class.
     * <br/>
     * The application {@link Context} is needed for devices perform a gatt connection
     * <p/>
     *
     * @param linkContext application linkContext
     * @param aggregator  listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/content/Context.html" target="_blank">Context</a>
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothManager.html" target="_blank">BluetoothManager</a>
     */
    public BluetoothLeConnectionServerTranslator(@NonNull LinkContext linkContext, @NonNull Aggregator aggregator) {
        this.linkContext = linkContext;
        this.aggregator = aggregator;

        this.connectedServers = new ConcurrentHashMap<>();
        this.bluetoothManager = linkContext.getBluetoothManager();
        this.newClientsObservable = new EventObservable<>();
    }

    @Override
    public void turnServerOn(@NonNull ServerProperties<BluetoothLeServerSettings> properties) {
        GattServerManager serverManager = new GattServerManager(
                properties.getIdentifier(),
                properties.getSettings());

        Object prop = connectedServers.putIfAbsent(properties.getIdentifier(), serverManager);
        LinkException exception = null;

        if (prop == null) {
            try {
                if (serverManager.start())
                    Receiver.notifyEvent(aggregator, new ConnectionServerOnEvent(properties.getIdentifier()));
                else
                    exception = LinkException.build(Reason.ERROR_START, "Error adding the bluetooth LE service");
            } catch (LinkException e) {
                e.printStackTrace();
                exception = e;
            }
        } else {
            exception = LinkException.build(Reason.ALREADY_STARTED,
                    "Bluetooth Le server already started: " + properties.getIdentifier());
        }
        if (exception != null)
            Receiver.notifyEvent(aggregator, new ConnectionServerOnEvent(properties.getIdentifier(),
                    exception));


            /*BluetoothGattCharacteristic elapsedCharacteristic =
                    new_def BluetoothGattCharacteristic(UUID.randomUUID(),
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                            BluetoothGattCharacteristic.PERMISSION_READ);
            BluetoothGattCharacteristic offsetCharacteristic =
                    new_def BluetoothGattCharacteristic(UUID.randomUUID(),
                            //Read+write permissions
                            BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_WRITE,
                            BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE);

            service.addCharacteristic(elapsedCharacteristic);
            service.addCharacteristic(offsetCharacteristic);*/

    }

    @Override
    public void turnServerOff(@NonNull String serverId) {
        GattServerManager serverManager = connectedServers.remove(serverId);
        if (serverManager != null) {
            serverManager.stop();
            Receiver.notifyEvent(aggregator, new ConnectionServerOffEvent(serverId));
        } else {
            Receiver.notifyEvent(aggregator, new ConnectionServerOffEvent(serverId,
                    LinkException.build(Reason.NULL_POINTER, "Server ID " + serverId)
            ));
        }
    }

    /**
     * Cancel a client connection from the server.
     *
     * @param serverId server identifier
     * @param device   device to be disconnected
     * @return <tt>true</tt> if server id exists, <tt>false</tt> otherwise
     */
    boolean cancelClientConnection(@NonNull String serverId, @NonNull BluetoothDevice device) {
        GattServerManager serverManager = connectedServers.get(serverId);
        if (serverManager != null) {
            serverManager.cancelClientConnection(device);
            return true;
        }
        return false;
    }

    @Override
    public boolean isServerOn(@NonNull String serverId) {
        return connectedServers.containsKey(serverId);
    }

    /**
     * Adds a listener in order to listen for new clients - for internal use.
     *
     * @param listener listener object
     */
    void listenNewClients(@NonNull Observer<Pair<Device, String>> listener) {
        newClientsObservable.addListener(listener);
    }

    @NonNull
    @Override
    public Collection<String> getActiveServers() {
        return Collections.unmodifiableList(new ArrayList<>(connectedServers.keySet()));
    }

    /**
     * Returns the bluetooth gatt server that matches a server identifier.
     *
     * @param identifier unique server identifier
     * @return bluetooth gatt server object or NULL if not started
     */
    @Nullable
    public BluetoothGattServer getBluetoothGattServer(@NonNull String identifier) {
        GattServerManager m = connectedServers.get(identifier);
        if (m != null)
            return m.gattServer;
        return null;
    }

    @Override
    public void detach() {
        newClientsObservable.removeAll();
        for (String serverId : connectedServers.keySet())
            turnServerOff(serverId);

        connectedServers.clear();
    }

    /**
     * BluetoothGattServerCallback that catches events regarding the gatt server states,
     * client connections, added services and other bluetooth le server domain stuff.
     */
    private class GattServerManager extends BluetoothGattServerCallback {
        private final String serverIdentifier;
        @Nullable
        private final BluetoothGattServer gattServer;
        private final BluetoothGattService gattService;
        private final ArrayList<BluetoothGattServerCallback> callbacks;


        /**
         * Constructor.
         *
         * @param serverIdentifier a server identifier
         * @param serverSettings   bluetooth le server settings
         */
        GattServerManager(@NonNull String serverIdentifier, @Nullable BluetoothLeServerSettings serverSettings) {
            this.serverIdentifier = serverIdentifier;
            this.gattServer = linkContext.getBluetoothGattServer(this);
            this.callbacks = new ArrayList<>(1);

            boolean hasService = (serverSettings != null && serverSettings.getGattService() != null);
            boolean hasCallback = (serverSettings != null && serverSettings.getServerCallback() != null);
            if (hasService)
                this.gattService = serverSettings.getGattService();
            else
                this.gattService = new BluetoothGattService(
                        UUID.fromString(serverIdentifier),
                        BluetoothGattService.SERVICE_TYPE_PRIMARY);
            if (hasCallback)
                callbacks.add(serverSettings.getServerCallback());
        }

        /**
         * Starts a bluetooth le server.
         */
        boolean start() throws LinkException {
            if (gattServer == null)
                throw LinkException.build(Reason.UNSUPPORTED, "Bluetooth gatt server not supported");
            return this.gattServer.addService(gattService);
        }

        /**
         * Cancels a connection of a specific bluetooth device.
         *
         * @param device bluetooth device object
         */
        void cancelClientConnection(@NonNull BluetoothDevice device) {
            if (gattServer != null)
                gattServer.cancelConnection(device);
        }

        /**
         * Stops the bluetooth le server.
         */
        void stop() {
            if (gattServer != null) {
                List<BluetoothDevice> deviceList =
                        bluetoothManager.getConnectedDevices(BluetoothProfile.GATT_SERVER);
                List<BluetoothDevice> deviceGatt =
                        bluetoothManager.getConnectedDevices(BluetoothProfile.GATT);

                for (BluetoothDevice device : deviceList) {
                    gattServer.cancelConnection(device);
                }

                for (BluetoothDevice device : deviceGatt) {
                    gattServer.cancelConnection(device);
                }
                gattServer.removeService(gattService);
                gattServer.clearServices();
                gattServer.close();
            }
        }

        /**
         * Executes a specific function on a list of callbacks.
         *
         * @param executable executable function
         */
        private void notifyCallbacks(@NonNull Executable<BluetoothGattServerCallback> executable) {
            for (BluetoothGattServerCallback callback : callbacks)
                executable.execute(callback);
        }

        @Override
        public void onConnectionStateChange(@NonNull final BluetoothDevice device, final int status, final int newState) {
            super.onConnectionStateChange(device, status, newState);

            BluetoothLeNode node = new BluetoothLeNode(device);
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    //notifies new server client - internal
                    newClientsObservable.notifyListeners(listener -> {
                        listener.onEvent(new Pair<>(node, serverIdentifier));
                        return false;
                    });

                    Receiver.notifyEvent(aggregator, new ConnectionServerNewClient(node));
                    Receiver.notifyEvent(aggregator, new ConnectionNewEvent(node));
                    break;

                case BluetoothProfile.STATE_DISCONNECTED:
                    //connectedServers.remove(serverIdentifier);
                    Receiver.notifyEvent(aggregator, new ConnectionLostEvent(node));
                    break;
            }
            notifyCallbacks(callback -> callback.onConnectionStateChange(device, status, newState));
        }

        @Override
        public void onServiceAdded(int status, BluetoothGattService service) {
            super.onServiceAdded(status, service);
            notifyCallbacks(callback -> callback.onServiceAdded(status, service));
        }

        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset,
                                                BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
            notifyCallbacks(callback -> callback.onCharacteristicReadRequest(
                    device, requestId, offset, characteristic));
        }

        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId,
                                                 BluetoothGattCharacteristic characteristic,
                                                 boolean preparedWrite, boolean responseNeeded,
                                                 int offset, byte[] value) {
            super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite,
                    responseNeeded, offset, value);
            notifyCallbacks(callback -> callback.onCharacteristicWriteRequest(
                    device, requestId, characteristic, preparedWrite, responseNeeded, offset, value));
        }

        @Override
        public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset,
                                            BluetoothGattDescriptor descriptor) {
            super.onDescriptorReadRequest(device, requestId, offset, descriptor);
            notifyCallbacks(callback -> callback.onDescriptorReadRequest(
                    device, requestId, offset, descriptor));
        }

        @Override
        public void onDescriptorWriteRequest(BluetoothDevice device, int requestId,
                                             BluetoothGattDescriptor descriptor,
                                             boolean preparedWrite, boolean responseNeeded,
                                             int offset, byte[] value) {
            super.onDescriptorWriteRequest(
                    device, requestId, descriptor, preparedWrite, responseNeeded, offset, value);
            notifyCallbacks(callback -> callback.onDescriptorWriteRequest(
                    device, requestId, descriptor, preparedWrite, responseNeeded, offset, value));
        }

        @Override
        public void onExecuteWrite(BluetoothDevice device, int requestId, boolean execute) {
            super.onExecuteWrite(device, requestId, execute);
            notifyCallbacks(callback -> callback.onExecuteWrite(device, requestId, execute));
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onNotificationSent(BluetoothDevice device, int status) {
            super.onNotificationSent(device, status);
            notifyCallbacks(callback -> callback.onNotificationSent(device, status));
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
        @Override
        public void onMtuChanged(BluetoothDevice device, int mtu) {
            super.onMtuChanged(device, mtu);
            notifyCallbacks(callback -> callback.onMtuChanged(device, mtu));
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onPhyUpdate(BluetoothDevice device, int txPhy, int rxPhy, int status) {
            super.onPhyUpdate(device, txPhy, rxPhy, status);
            notifyCallbacks(callback -> callback.onPhyUpdate(device, txPhy, rxPhy, status));
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onPhyRead(BluetoothDevice device, int txPhy, int rxPhy, int status) {
            super.onPhyRead(device, txPhy, rxPhy, status);
            notifyCallbacks(callback -> callback.onPhyRead(device, txPhy, rxPhy, status));
        }
    }
}
