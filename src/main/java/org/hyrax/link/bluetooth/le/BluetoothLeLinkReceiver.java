/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.le;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.event.HardwareOffEvent;
import org.hyrax.link.misc.event.HardwareOnEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Class {@link BluetoothLeLinkReceiver} receives the events of bluetooth hardware status mapping
 * into to well defined {@link Event} objects.
 * <br/>
 * With Bluetooth le most of the events are catch using callbacks.
 */
public class BluetoothLeLinkReceiver extends Receiver {
    /**
     * Creates a {@link Receiver} object.
     * <br/>
     * It receives an object {@link Aggregator}, in order to access the registered listeners.
     *
     * @param aggregator listener {@link Aggregator}
     */
    public BluetoothLeLinkReceiver(@NonNull Aggregator aggregator) {
        super(aggregator);
    }

    @NonNull
    @Override
    protected List<Event> actionToTechEvent(@NonNull String action, @NonNull Intent intent) {
        List<Event> events = new ArrayList<>();
        switch (action) {
            case BluetoothAdapter.ACTION_STATE_CHANGED:
                int hardwareState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (hardwareState) {
                    case BluetoothAdapter.STATE_ON:
                        events.add(new HardwareOnEvent());
                        break;

                    case BluetoothAdapter.STATE_OFF:
                        events.add(new HardwareOffEvent());
                        break;
                }
                break;
        }
        return events;
    }

    @NonNull
    @Override
    public IntentFilter getIntentFilters() {
        return new IntentFilter() {{
            addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        }};
    }
}
