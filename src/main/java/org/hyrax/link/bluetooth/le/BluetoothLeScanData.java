/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.le;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanRecord;
import android.os.ParcelUuid;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.ScanData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class represents the scanned data for Bluetooth.
 *
 * @see <a href="https://developer.android.com/reference/android/bluetooth/le/ScanRecord.html" target="_blank">ScanRecord</a>
 */
@TargetApi(value = 21)
public class BluetoothLeScanData extends ScanData<ScanRecord> {
    private final int hashcode;

    /**
     * Constructs a {@link BluetoothLeScanData} object given a bluetooth {@link ScanRecord}.
     *
     * @param originalObject bluetooth {@link ScanRecord} object.
     */
    BluetoothLeScanData(@NonNull ScanRecord originalObject) {
        super(originalObject);
        List<ParcelUuid> objects = new ArrayList<>();

        if (originalObject.getServiceUuids() != null)
            objects.addAll(originalObject.getServiceUuids());

        objects.addAll(originalObject.getServiceData().keySet());
        hashcode = Objects.hash(objects.toArray());
    }

    @Override
    public String toString() {
        return originalObject.toString();
    }

    @Override
    public boolean equals(Object o) {
        return o == this ||
                ((o instanceof BluetoothLeScanData) &&
                        ((BluetoothLeScanData) o).hashcode == hashcode
                );
    }

    @Override
    public int hashCode() {
        return hashcode;
    }
}
