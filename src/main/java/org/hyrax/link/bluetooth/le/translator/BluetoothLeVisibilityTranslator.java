/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.le.translator;

import android.annotation.TargetApi;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.event.VisibilityOffEvent;
import org.hyrax.link.misc.event.VisibilityOnEvent;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.bluetooth.BluetoothLeLink;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.link.LinkContext;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.VisibilityLogicTranslator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link BluetoothLeVisibilityTranslator} manages the Bluetooth le visibility state.
 * VISIBLE/INVISIBLE
 * <br/>
 * In Bluetooth le the visibility is achieved using an default advertiser that beacons it's
 * presence.
 */
@TargetApi(value = 21)
public class BluetoothLeVisibilityTranslator implements VisibilityLogicTranslator<AdvertiseSettings, AdvertiseData> {
    @NonNull
    private final LinkContext linkContext;
    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<String, AdvertiserCallback> advertisers;

    /**
     * Constructor of {@link BluetoothLeVisibilityTranslator} class.
     *
     * @param linkContext application linkContext
     * @param aggregator  listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html" target="_blank">BluetoothAdapter</a>
     */
    public BluetoothLeVisibilityTranslator(@NonNull LinkContext linkContext,
                                           @NonNull Aggregator aggregator) {
        this.linkContext = linkContext;
        this.aggregator = aggregator;

        advertisers = new ConcurrentHashMap<>();
    }


    @Override
    public void turnVisibilityOn(@NonNull VisibilityProperties<AdvertiseSettings, AdvertiseData> visibilityProperties) {
        BluetoothLeAdvertiser advertiser = linkContext.getBluetoothLeAdvertiser();
        String advertiseId = visibilityProperties.getIdentifier();

        AdvertiseSettings settings = visibilityProperties.getAdvertiserSettings();
        AdvertiseData data = visibilityProperties.getAdvertiserData();

        LinkException exception = null;
        if (null == advertiser) {
            exception = LinkException.build(Reason.NULL_POINTER,
                    "Null advertiser. Check bluetooth status.");

        } else if (settings == null) {
            exception = LinkException.build(Reason.NULL_POINTER, "Empty advertiser settings, please defined one");

        } else if (data == null) {
            exception = LinkException.build(Reason.NULL_POINTER, "Empty advertiser data, please defined one");

        } else {
            AdvertiserCallback callback = new AdvertiserCallback(visibilityProperties);

            AdvertiserCallback call = advertisers.putIfAbsent(advertiseId, callback);

            if (null == call)
                advertiser.startAdvertising(settings, data, callback);
            else
                exception = LinkException.build(Reason.ALREADY_STARTED, " Already on " + advertiseId);
        }

        if (null != exception) {
            Receiver.notifyEvent(aggregator, new VisibilityOnEvent(advertiseId, exception));
        }
    }

    @Override
    public void turnVisibilityOff(@NonNull String advertiseId) {
        BluetoothLeAdvertiser advertiser = linkContext.getBluetoothLeAdvertiser();

        String reason = null;
        if (null != advertiser) {
            AdvertiserCallback callback = advertisers.remove(advertiseId);
            if (null != callback) {
                advertiser.stopAdvertising(callback);
                Receiver.notifyEvent(aggregator, new VisibilityOffEvent(advertiseId));
            } else {
                reason = "Advertiser not active " + advertiseId;
            }
        } else {
            reason = "Null advertiser. Check bluetooth status.";
        }

        if (null != reason)
            Receiver.notifyEvent(aggregator, new VisibilityOffEvent(advertiseId,
                    LinkException.build(Reason.NULL_POINTER, reason)));
    }

    @Override
    public boolean isVisibilityOn(@NonNull String advertiseId) {
        return advertisers.containsKey(advertiseId);
    }

    @NonNull
    @Override
    public Collection<String> getAdvertisers() {
        return Collections.unmodifiableList(new ArrayList<>(advertisers.keySet()));
    }

    @Override
    public void detach() {
        for (String identifier : advertisers.keySet())
            turnVisibilityOff(identifier);

        advertisers.clear();
    }

    /**
     * Advertiser callback that catches advertisement success/error events
     */
    private class AdvertiserCallback extends AdvertiseCallback {
        @NonNull
        private final VisibilityProperties properties;

        AdvertiserCallback(@NonNull VisibilityProperties properties) {
            this.properties = properties;
        }

        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            super.onStartSuccess(settingsInEffect);
            Receiver.notifyEvent(aggregator, new VisibilityOnEvent(properties.getIdentifier()));
        }

        @Override
        public void onStartFailure(int errorCode) {
            super.onStartFailure(errorCode);
            advertisers.remove(properties.getIdentifier());
            Receiver.notifyEvent(aggregator, new VisibilityOnEvent(properties.getIdentifier(),
                    LinkException.build(BluetoothLeLink.mapAdvertiseToReason(errorCode),
                            "Error starting visibility " + properties.getIdentifier())
            ));
        }
    }
}
