/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.le.translator;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Pair;

import org.hyrax.link.misc.Executable;
import org.hyrax.link.misc.event.ConnectionLostEvent;
import org.hyrax.link.misc.event.ConnectionNewEvent;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.Device;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.Outcome;
import org.hyrax.link.bluetooth.le.BluetoothLeNode;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.link.LinkContext;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.ConnectionLogicTranslator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link BluetoothLeConnectionTranslator} manages the bluetooth be remote gatt connections.
 * CONNECT/DISCONNECT
 */
public class BluetoothLeConnectionTranslator implements ConnectionLogicTranslator<BluetoothGattCallback> {
    private static final int FROM_CLIENT = 0;
    private static final int FROM_SERVER = 1;

    @NonNull
    private final LinkContext linkContext;
    @NonNull
    private final BluetoothLeConnectionServerTranslator serverTranslator;
    @NonNull
    private final Aggregator aggregator;

    @NonNull
    private final ConcurrentHashMap<Device, Pair<Integer, String>> connectedDevices;
    @NonNull
    private final ConcurrentHashMap<Device, GattManager> clientDevices;
    /**
     * Listens for disconnection events.
     */
    private final LinkListener clientListener = new LinkListener<Device>() {
        @Override
        public void onEvent(@NonNull LinkEvent eventType, @NonNull Outcome<Device> outcome) {
            connectedDevices.remove(outcome.getOutcome());
            clientDevices.remove(outcome.getOutcome());
        }
    };

    /**
     * Constructor of {@link BluetoothLeConnectionTranslator} class.
     * <br/>
     * The application {@link Context} is needed for devices perform a gatt connection
     * <br/>
     * Some of the connection comes from the gatt server, thus in order to be able to disable
     * those connections that operation must be done on the server translator side.
     * <p/>
     *
     * @param linkContext      application linkContext
     * @param serverTranslator server translator object
     * @param aggregator       listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/content/Context.html" target="_blank">Context</a>
     */
    public BluetoothLeConnectionTranslator(@NonNull LinkContext linkContext,
                                           @NonNull BluetoothLeConnectionServerTranslator serverTranslator,
                                           @NonNull Aggregator aggregator) {
        this.linkContext = linkContext;
        this.serverTranslator = serverTranslator;
        this.aggregator = aggregator;

        this.connectedDevices = new ConcurrentHashMap<>();
        this.clientDevices = new ConcurrentHashMap<>();
        aggregator.listen(clientListener, LinkEvent.LINK_CONNECTION_LOST, Aggregator.NO_FILTER, true);

        //listen for new clients. Source Server
        serverTranslator.listenNewClients(pair -> {
            Device device = pair.first;
            String serverId = pair.second;
            Object prop = connectedDevices.putIfAbsent(device, new Pair<>(FROM_SERVER, serverId));
            if (prop != null) {
                Receiver.notifyEvent(aggregator, new ConnectionNewEvent(device,
                        LinkException.build(Reason.ALREADY_STARTED,
                                "Server side client connection already exists: " + device.getUniqueAddress())));
                serverTranslator.cancelClientConnection(serverId, (BluetoothDevice) device.getOriginalObject());
            }
        });
    }

    @Override
    public void turnConnectionOn(@NonNull Device remoteDevice,
                                 @NonNull ConnectionProperties<BluetoothGattCallback> properties) {
        LinkException exception = null;
        try {
            Object prop = connectedDevices.putIfAbsent(remoteDevice, new Pair<>(FROM_CLIENT, Constants.UNDEFINED));
            if (null == prop) {
                GattManager gattManager = new GattManager((BluetoothLeNode) remoteDevice, properties.getSettings());
                clientDevices.put(remoteDevice, gattManager);
                gattManager.connect();

            } else {
                exception = LinkException.build(Reason.ALREADY_STARTED, "Bluetooth le already connected "
                        + remoteDevice.getUniqueAddress());
            }
        } catch (ClassCastException e) {
            connectedDevices.remove(remoteDevice);
            clientDevices.remove(remoteDevice);
            exception = LinkException.build(e);
        }

        if (null != exception) {
            Receiver.notifyEvent(aggregator, new ConnectionNewEvent(remoteDevice, exception));
        }
    }

    @Override
    public void turnConnectionOff(@NonNull Device remoteDevice) {
        //disconnects client side connections
        GattManager gattManager = clientDevices.remove(remoteDevice);
        if (gattManager != null) {
            connectedDevices.remove(remoteDevice);
            gattManager.disconnect();

        } else {
            String reason = null;
            //disconnects server side connections
            Pair<Integer, String> pair = connectedDevices.remove(remoteDevice);
            if (null != pair) {
                BluetoothDevice device = (BluetoothDevice) remoteDevice.getOriginalObject();
                if (!serverTranslator.cancelClientConnection(pair.second, device))
                    reason = "Client (from server) not connected";
            } else
                reason = " Client not connected";

            if (null != reason)
                Receiver.notifyEvent(aggregator, new ConnectionLostEvent(remoteDevice,
                        LinkException.build(Reason.NULL_POINTER, reason + remoteDevice)
                ));
        }
    }

    @Override
    public boolean isConnectionOn(@NonNull Device remoteDevice) {
        return connectedDevices.containsKey(remoteDevice);
    }

    @NonNull
    @Override
    public Collection<Device> getActiveConnections() {
        return Collections.unmodifiableList(new ArrayList<>(connectedDevices.keySet()));
    }

    /**
     * Returns the bluetooth gatt object that matches a device.
     *
     * @param device bluetooth device
     * @return bluetooth gatt object or NULL if not connected
     */
    @Nullable
    public BluetoothGatt getBluetoothGatt(@NonNull Device device) {
        GattManager m = clientDevices.get(device);
        if (m != null)
            return m.bluetoothGatt;
        return null;
    }

    @Override
    public void detach() {
        aggregator.removeListener(clientListener);

        for (GattManager gatt : clientDevices.values()) {
            gatt.disconnect();
        }

        connectedDevices.clear();
        clientDevices.clear();
    }

    /**
     * BluetoothGattCallback that catches events regarding the client connection, services and
     * other bluetooth le domain stuff.
     */
    private class GattManager extends BluetoothGattCallback {
        private final BluetoothLeNode leNode;
        private final ArrayList<BluetoothGattCallback> callbacks;
        @Nullable
        private BluetoothGatt bluetoothGatt;

        /**
         * Constructor.
         *
         * @param bluetoothLeNode bluetooth le device object
         * @param callback        a bluetooth gatt connection callback
         */
        GattManager(BluetoothLeNode bluetoothLeNode, @Nullable BluetoothGattCallback callback) {
            this.leNode = bluetoothLeNode;
            this.callbacks = new ArrayList<>(1);
            if (callback != null)
                this.callbacks.add(callback);
        }

        /**
         * Connects the current device to the remote device passed on the constructor.
         */
        void connect() {
            this.bluetoothGatt = leNode.getOriginalObject()
                    .connectGatt(linkContext, false, this);
        }

        /**
         * Disconnects the current device from the remote device passed on the constructor.
         */
        void disconnect() {
            if (bluetoothGatt != null) {
                bluetoothGatt.disconnect();
            }
            this.callbacks.clear();
        }

        /**
         * Executes a specific function on a list of callbacks.
         *
         * @param executable executable function
         */
        private void notifyCallbacks(@NonNull Executable<BluetoothGattCallback> executable) {
            for (BluetoothGattCallback callback : callbacks)
                executable.execute(callback);
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onPhyUpdate(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyUpdate(gatt, txPhy, rxPhy, status);
            notifyCallbacks(callback -> callback.onPhyUpdate(gatt, txPhy, rxPhy, status));
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onPhyRead(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
            super.onPhyRead(gatt, txPhy, rxPhy, status);
            notifyCallbacks(callback -> callback.onPhyRead(gatt, txPhy, rxPhy, status));
        }

        @Override
        public void onConnectionStateChange(@NonNull BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        Receiver.notifyEvent(aggregator, new ConnectionNewEvent(leNode));
                    } else {
                        Receiver.notifyEvent(aggregator, new ConnectionNewEvent(leNode,
                                LinkException.build(Reason.ERROR_START, "Error connecting to gatt server")
                        ));
                    }
                    break;

                case BluetoothProfile.STATE_DISCONNECTED:
                    Receiver.notifyEvent(aggregator, new ConnectionLostEvent(leNode));
                    gatt.close();
                    break;
            }
            notifyCallbacks(callback -> callback.onConnectionStateChange(gatt, status, newState));
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            notifyCallbacks(callback -> callback.onServicesDiscovered(gatt, status));
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            notifyCallbacks(callback -> callback.onCharacteristicRead(gatt, characteristic, status));
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            notifyCallbacks(callback -> callback.onCharacteristicWrite(gatt, characteristic, status));
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            notifyCallbacks(callback -> callback.onCharacteristicChanged(gatt, characteristic));
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
            notifyCallbacks(callback -> callback.onDescriptorRead(gatt, descriptor, status));
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            notifyCallbacks(callback -> callback.onDescriptorWrite(gatt, descriptor, status));
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
            notifyCallbacks(callback -> callback.onReliableWriteCompleted(gatt, status));
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            notifyCallbacks(callback -> callback.onReadRemoteRssi(gatt, rssi, status));
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
            notifyCallbacks(callback -> callback.onMtuChanged(gatt, mtu, status));
        }
    }
}
