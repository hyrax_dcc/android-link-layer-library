/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.le;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanResult;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Constants;
import org.hyrax.link.Device;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.ScanData;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Class {@link BluetoothLeNode} represents a bluetooth le remote device.
 */
@TargetApi(value = 21)
public class BluetoothLeNode implements Device {
    @NonNull
    private final BluetoothDevice device;
    @NonNull
    private final Set<ScanData> scanData;
    private final int rssi;

    /**
     * Constructor of {@link BluetoothLeNode} class.
     * <br/>
     * This constructor must be used for devices that discover others.
     *
     * @param scanResult scan object of bluetooth le device
     * @see <a href="https://developer.android.com/reference/android/bluetooth/le/ScanResult.html" target="_blank">ScanResult</a>
     */
    public BluetoothLeNode(final @NonNull ScanResult scanResult) {
        this.device = scanResult.getDevice();
        this.scanData = new HashSet<>();
        if (null != scanResult.getScanRecord())
            scanData.add(new BluetoothLeScanData(scanResult.getScanRecord()));

        this.rssi = scanResult.getRssi();
    }

    /**
     * Constructor of {@link BluetoothLeNode} class.
     * <br/>
     * This constructor should be used for devices that receive connections (server).
     *
     * @param bluetoothDevice remote bluetooth device object
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothDevice.html" target="_blank">BluetoothDevice</a>
     */
    public BluetoothLeNode(@NonNull BluetoothDevice bluetoothDevice) {
        this.device = bluetoothDevice;
        this.scanData = new HashSet<>();
        this.rssi = Constants.DEFAULT_RSSI;
    }

    @NonNull
    @Override
    public String getName() {
        if (null == device.getName())
            return Constants.UNDEFINED;
        return device.getName();
    }

    @NonNull
    @Override
    public String getUniqueAddress() {
        return device.getAddress();
    }

    public int getRssi() {
        return rssi;
    }

    @NonNull
    @Override
    public BluetoothDevice getOriginalObject() {
        return device;
    }

    @NonNull
    @Override
    public Collection<ScanData> getScanData() {
        return scanData;
    }

    @Override
    public void mergeScanData(@NonNull Collection<ScanData> scanData) {
        this.scanData.addAll(scanData);
    }

    @NonNull
    @Override
    public Technology getTechnology() {
        return Technology.BLUETOOTH_LE;
    }

    @Override
    public String toString() {
        return String.format("%s: %s (%s) -> %s", getTechnology().name(), getUniqueAddress(),
                getName(), getScanData());
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof BluetoothLeNode) && ((BluetoothLeNode) o).device.equals(device));
    }

    @Override
    public int hashCode() {
        return device.hashCode();
    }
}
