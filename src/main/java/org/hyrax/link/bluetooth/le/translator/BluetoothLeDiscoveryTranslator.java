/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.bluetooth.le.translator;

import android.annotation.TargetApi;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.location.LocationManager;
import android.support.annotation.NonNull;

import org.hyrax.link.bluetooth.BluetoothLeLink;
import org.hyrax.link.misc.event.DiscoveryFoundEvent;
import org.hyrax.link.misc.event.DiscoveryOffEvent;
import org.hyrax.link.misc.event.DiscoveryOnEvent;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.bluetooth.le.BluetoothLeNode;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.link.LinkContext;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.DiscoveryLogicTranslator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class {@link BluetoothLeDiscoveryTranslator} manages the bluetooth le discovery state.
 * START_DISCOVERY/STOP_DISCOVERY
 * <br/>
 * The Bluetooth le the discovery is achieved be scanning devices that are beacon their presence
 * using a default advertiser.
 */
@TargetApi(value = 21)
public class BluetoothLeDiscoveryTranslator implements DiscoveryLogicTranslator<ScanSettings, ArrayList<ScanFilter>> {
    @NonNull
    private final LinkContext linkContext;
    @NonNull
    private final Aggregator aggregator;
    @NonNull
    private final ConcurrentHashMap<String, Callback> scanners;

    /**
     * Constructor of {@link BluetoothLeDiscoveryTranslator} class.
     *
     * @param linkContext application linkContext
     * @param aggregator  listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html" target="_blank">BluetoothAdapter</a>
     */
    public BluetoothLeDiscoveryTranslator(@NonNull LinkContext linkContext, @NonNull Aggregator aggregator) {
        this.linkContext = linkContext;
        this.aggregator = aggregator;

        scanners = new ConcurrentHashMap<>();
    }

    @Override
    public void turnDiscoveryOn(@NonNull DiscoveryProperties<ScanSettings, ArrayList<ScanFilter>> discoveryProperties) {
        String scanId = discoveryProperties.getIdentifier();
        LinkException exception = null;

        if (linkContext.getLocationManager().isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            BluetoothLeScanner sc = linkContext.getBluetoothLeScanner();
            ScanSettings scanSettings = discoveryProperties.getScannerSettings();
            ArrayList<ScanFilter> scanFilter = discoveryProperties.getScannerFilter();
            if (null == sc) {
                exception = LinkException.build(Reason.NULL_POINTER, "Null scanner. Check bluetooth status.");

            } else if (scanSettings == null) {
                exception = LinkException.build(Reason.NULL_POINTER, "Empty scanner settings, please defined one");

            } else if (scanFilter == null) {
                exception = LinkException.build(Reason.NULL_POINTER, "Empty scanner filter, please defined one");

            } else {
                Callback callback = new Callback(discoveryProperties);

                Callback call = scanners.putIfAbsent(scanId, callback);
                if (null == call) {
                    sc.startScan(scanFilter, scanSettings, callback);
                    Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(scanId));

                } else {
                    exception = LinkException.build(Reason.ALREADY_STARTED, " Already active " + scanId);
                }
            }
        } else {
            exception = LinkException.build(Reason.LOCATION_DISABLED, "Location is disabled, please enabled it");
        }

        if (null != exception)
            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(scanId, exception));
    }

    @Override
    public void turnDiscoveryOff(@NonNull String scanId) {
        BluetoothLeScanner sc = linkContext.getBluetoothLeScanner();

        String reason = null;
        if (null != sc) {
            Callback callback = scanners.remove(scanId);
            if (null != callback) {
                sc.stopScan(callback);
                Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(scanId));
            } else {
                reason = "No active scanner";
            }
        } else {
            reason = "Null scanner. Check bluetooth status.";
        }

        if (null != reason)
            Receiver.notifyEvent(aggregator, new DiscoveryOffEvent(scanId,
                    LinkException.build(Reason.NULL_POINTER, reason)));
    }

    @Override
    public boolean isDiscoveryOn(@NonNull String scanId) {
        return scanners.containsKey(scanId);
    }

    @NonNull
    @Override
    public Collection<String> getScanners() {
        return Collections.unmodifiableList(new ArrayList<>(scanners.keySet()));
    }

    @Override
    public void detach() {
        BluetoothLeScanner sc = linkContext.getBluetoothLeScanner();
        if (null != sc) {
            for (Callback callback : scanners.values()) {
                sc.stopScan(callback);
            }
            scanners.clear();
        }
    }

    /**
     * Scan callback that catches the scan error and device found events.
     */
    private class Callback extends ScanCallback {
        @NonNull
        private final DiscoveryProperties properties;

        Callback(@NonNull DiscoveryProperties properties) {
            this.properties = properties;
        }

        @Override
        public void onScanResult(int callbackType, @NonNull ScanResult result) {
            Receiver.notifyEvent(aggregator, new DiscoveryFoundEvent(
                    properties.getIdentifier(), new BluetoothLeNode(result)
            ));
        }

        @Override
        public void onScanFailed(int errorCode) {
            scanners.remove(properties.getIdentifier());
            Receiver.notifyEvent(aggregator, new DiscoveryOnEvent(properties.getIdentifier(),
                    LinkException.build(BluetoothLeLink.mapScanToReason(errorCode),
                            "Error start scanning " + properties.getIdentifier())
            ));
        }
    }
}
