/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.mobile;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.link.Translators;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.Device;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.misc.link.AbstractLink;
import org.hyrax.link.misc.link.LinkFeatures;
import org.hyrax.link.mobile.translator.MobileConnectionServerTranslator;
import org.hyrax.link.mobile.translator.MobileConnectionTranslator;
import org.hyrax.link.mobile.translator.MobileDiscoveryTranslator;
import org.hyrax.link.mobile.translator.MobileHardwareTranslator;
import org.hyrax.link.mobile.translator.MobileVisibilityTranslator;

/**
 * Singleton class responsible for managing the mobile data operations.
 */
public class MobileLink extends AbstractLink<MobileLink> implements LinkFeatures {
    private static MobileLink instance;

    /**
     * Constructor.
     */
    private MobileLink() {
        super(Technology.MOBILE);
    }

    /**
     * Returns the mobile link instance.
     *
     * @return mobile link instance
     */
    @NonNull
    public static MobileLink getInstance() {
        if (instance == null)
            instance = new MobileLink();
        return instance;
    }

    @NonNull
    @Override
    protected MobileLink getFeatures() {
        return this;
    }

    @NonNull
    @Override
    protected Translators initTranslators() {
        return new Translators(new MobileLinkReceiver(aggregator),
                new MobileHardwareTranslator(linkContext.getConnectivityManager(), aggregator),
                new MobileDiscoveryTranslator(aggregator),
                new MobileVisibilityTranslator(aggregator),
                new MobileConnectionServerTranslator(aggregator),
                new MobileConnectionTranslator(aggregator)
        );
    }

    @Override
    protected void onAttach() {

    }

    @Override
    protected void onDetach() {
        instance = null;
    }

    @Override
    public DiscoveryProperties.Builder<Void, Void> newDiscoveryBuilder() {
        return DiscoveryProperties.newBuilder();
    }

    @Override
    public VisibilityProperties.Builder<Void, Void> newVisibilityBuilder() {
        return VisibilityProperties.newBuilder();
    }

    @Override
    public ConnectionProperties.Builder<Void> newConnectionBuilder(@NonNull Device device) {
        return ConnectionProperties.newBuilder(device.getName());
    }

    @Override
    public ServerProperties.Builder<Void> newServerBuilder() {
        return ServerProperties.newBuilder();
    }
}
