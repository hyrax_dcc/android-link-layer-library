/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.mobile;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.Event;
import org.hyrax.link.misc.event.HardwareOffEvent;
import org.hyrax.link.misc.event.HardwareOnEvent;

import java.util.ArrayList;
import java.util.List;


/**
 * Class {@link MobileLinkReceiver} receives all the mobile data events.
 */

public class MobileLinkReceiver extends Receiver {

    public MobileLinkReceiver(@NonNull Aggregator aggregator) {
        super(aggregator);
    }

    @NonNull
    @Override
    protected List<Event> actionToTechEvent(@NonNull String action, @NonNull Intent intent) {
        List<Event> events = new ArrayList<>();

        switch (action) {
            case ConnectivityManager.CONNECTIVITY_ACTION:
                boolean hasConnectivity = !intent.getBooleanExtra(
                        ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE);
                int netType = intent.getIntExtra(ConnectivityManager.EXTRA_NETWORK_TYPE, -1);

                if (ConnectivityManager.TYPE_MOBILE == netType) {
                    if (hasConnectivity)
                        events.add(new HardwareOnEvent());
                    else
                        events.add(new HardwareOffEvent());
                }
                break;
        }
        return events;
    }

    @NonNull
    @Override
    public IntentFilter getIntentFilters() {
        return new IntentFilter() {{
            addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        }};
    }
}
