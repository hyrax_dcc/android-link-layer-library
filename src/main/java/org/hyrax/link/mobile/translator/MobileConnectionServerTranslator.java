/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.mobile.translator;

import android.support.annotation.NonNull;

import org.hyrax.link.misc.event.ConnectionServerOffEvent;
import org.hyrax.link.misc.event.ConnectionServerOnEvent;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.ConnectionServerLogicTranslator;

import java.util.ArrayList;
import java.util.Collection;


/**
 * The mobile does not support the connection server features.
 */
public class MobileConnectionServerTranslator implements ConnectionServerLogicTranslator {
    @NonNull
    private final Aggregator aggregator;

    /**
     * Constructor if {@link MobileConnectionServerTranslator} class.
     *
     * @param aggregator listeners aggregator
     */
    public MobileConnectionServerTranslator(@NonNull Aggregator aggregator) {
        this.aggregator = aggregator;
    }

    @Override
    public void turnServerOn(@NonNull ServerProperties properties) {
        Receiver.notifyEvent(aggregator, new ConnectionServerOnEvent(properties.getIdentifier(),
                LinkException.build(Reason.UNSUPPORTED, "Mobile does not implements connection server feature")
        ));
    }

    @Override
    public void turnServerOff(@NonNull String serverId) {
        Receiver.notifyEvent(aggregator, new ConnectionServerOffEvent(serverId,
                LinkException.build(Reason.UNSUPPORTED, "Mobile does not implements connection server feature")
        ));
    }

    @Override
    public boolean isServerOn(@NonNull String serverId) {
        return false;
    }

    @NonNull
    @Override
    public Collection<String> getActiveServers() {
        return new ArrayList<>();
    }

    @Override
    public void detach() {

    }
}
