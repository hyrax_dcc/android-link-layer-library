/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.link.mobile.translator;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import org.hyrax.link.misc.Aggregator;
import org.hyrax.link.misc.Receiver;
import org.hyrax.link.misc.event.HardwareOffEvent;
import org.hyrax.link.misc.event.HardwareOnEvent;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.exception.Reason;
import org.hyrax.link.misc.logic.translator.HardwareLogicTranslator;

/**
 * The mobile data does not supported the hardware ENABLE/DISABLE, but the developer may query
 * about the mobile hardware state.
 */
public class MobileHardwareTranslator implements HardwareLogicTranslator {
    @NonNull
    private final ConnectivityManager connectivityManager;
    @NonNull
    private final Aggregator aggregator;

    /**
     * Constructor if {@link MobileHardwareTranslator} class.
     *
     * @param connectivityManager mobile data object manager
     * @param aggregator          listeners aggregator
     * @see <a href="https://developer.android.com/reference/android/net/ConnectivityManager.html" target="_blank">ConnectivityManager</a>
     */
    public MobileHardwareTranslator(@NonNull ConnectivityManager connectivityManager, @NonNull Aggregator aggregator) {
        this.connectivityManager = connectivityManager;
        this.aggregator = aggregator;
    }

    @Override
    public void turnHardwareOn() {
        Receiver.notifyEvent(aggregator, new HardwareOnEvent(
                LinkException.build(Reason.ERROR_START, "The mobile hardware cannot be enabled by " +
                        "the api. It must be done by the user or through intent.")
        ));
    }

    @Override
    public void turnHardwareOff() {
        Receiver.notifyEvent(aggregator, new HardwareOffEvent(
                LinkException.build(Reason.ERROR_STOP, "The mobile hardware cannot be disable by " +
                        "the api. It must be done by the user or through intent.")
        ));
    }

    @Override
    public boolean isHardwareOn() {
        //if sdk version is greater or equals than 21
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            for (Network network : networks) {
                NetworkInfo info = connectivityManager.getNetworkInfo(network);
                if (info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE)
                    return true;
            }
            return false;
        } else {
            NetworkInfo[] netInfo = connectivityManager.getAllNetworkInfo();
            for (NetworkInfo info : netInfo) {
                if (info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE)
                    return true;
            }
            return false;
        }
    }

    @Override
    public void detach() {

    }
}
